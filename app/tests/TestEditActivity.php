<?php

class TestEditActivity extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
    
    public function setUp()
    {
        parent::setUp();
        Session::start();

        // Enable filters
        Route::enableFilters();
  
    }
    
    public function testPostAddActivity()
    {
        $user = User::find(3);
        
        $this->be($user);
        
        $event = array('eventID'=>'1','activity_name' => 'activityname', 'activity_description' => 'activitydescription', 'activity_location' => 'activitylocation', 'activity_number_positions' => '5', 'activity_date' => '12/11/2014', 'activity_start_time' => '06:00', 'activity_end_time' => '23:50');
        
        $value = 'Successfully Updated Activity';
        
        $this->action('GET', 'ActivityController@update', '4', $event);
        
        $this->assertRedirectedTo('event/1/showActivity');
        $this->assertSessionHas('message', $value);        
        
    }
}