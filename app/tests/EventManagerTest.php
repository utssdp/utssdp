<?php

class EventManagerTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
    
    public function setUp()
    {
        parent::setUp();
  
    }
    
    public function testAddEventManager()
    {
        $user = User::find(3);
        
        $this->be($user);
        
        $this->call('GET', 'addEventManager');
        $this->assertResponseOk();
    }
    
   public function testPostAddEventManager()
    {
        //$user = User::find(3);
        
        //$this->be($user);
        
        $eventManager = array('utsID' => '123457', 'userFirstName' => 'unitTestFirstName', 'userLastName' => 'unitTestLastName', 'userGender' => 'Male', 'email' => 'unittesting1@gmail.com', 'userPhone' => '0410009426', 'userType' => '2');
        
        $value = 'You have Successfully Registered a Event Manager.';
        
        $this->action('POST', 'UsersController@doRegister', null, $eventManager);
        
        $this->assertRedirectedTo('addEventManager');
        $this->assertSessionHas('message', $value); 
        //$this->assertSessionHasErrors('utsID');
    }
    
    public function testPostFalseAddEventManager()
    {
        //$user = User::find(3);
        
        //$this->be($user);
        
        $eventManager = array('utsID' => '1', 'userFirstName' => 'unitTestFirstName', 'userLastName' => 'unitTestLastName', 'userGender' => 'Male', 'email' => 'unittesting11@gmail.com', 'userPhone' => '11111111', 'userType' => '2');
        
        
        $this->action('POST', 'UsersController@doRegister', null, $eventManager);
        
        $this->assertRedirectedTo('addEventManager');
        $this->assertSessionHasErrors('utsID');
        
    }
    
    public function testPostNoUTSIDAddEventManager()
    {
        //$user = User::find(3);
        
        //$this->be($user);
        
        $eventManager = array('userFirstName' => 'unitTestFirstName', 'userLastName' => 'unitTestLastName', 'userGender' => 'Male', 'email' => 'unittesting11@gmail.com', 'userPhone' => '11111111', 'userType' => '2');
        
        
        $this->action('POST', 'UsersController@doRegister', null, $eventManager);
        
        $this->assertRedirectedTo('addEventManager');
        $this->assertSessionHasErrors('utsID');
        
    }
    
    public function testPostNonAlphaNameAddEventManager()
    {
        //$user = User::find(3);
        
        //$this->be($user);
        
        $eventManager = array('utsID' => '1', 'userFirstName' => 'unitTes$tFirstName', 'userLastName' => 'unitTestLastName', 'userGender' => 'Male', 'email' => 'unittesting11@gmail.com', 'userPhone' => '11111111', 'userType' => '2');
        
        
        $this->action('POST', 'UsersController@doRegister', null, $eventManager);
        
        $this->assertRedirectedTo('addEventManager');
        $this->assertSessionHasErrors('userFirstName');
        
    }
    
    public function testPostInvalidEmailAddEventManager()
    {
        //$user = User::find(3);
        
        //$this->be($user);
        
        $eventManager = array('utsID' => '1', 'userFirstName' => 'unitTestFirstName', 'userLastName' => 'unitTestLastName', 'userGender' => 'Male', 'email' => 'unittesting11mail.com', 'userPhone' => '11111111', 'userType' => '2');
        
        
        $this->action('POST', 'UsersController@doRegister', null, $eventManager);
        
        $this->assertRedirectedTo('addEventManager');
        $this->assertSessionHasErrors('email');
        
    }
    
    public function testPostInvalidPhoneAddEventManager()
    {
        //$user = User::find(3);
        
        //$this->be($user);
        
        $eventManager = array('utsID' => '1', 'userFirstName' => 'unitTestFirstName', 'userLastName' => 'unitTestLastName', 'userGender' => 'Male', 'email' => 'unittesting11@mail.com', 'userPhone' => '1111@1111', 'userType' => '2');
        
        
        $this->action('POST', 'UsersController@doRegister', null, $eventManager);
        
        $this->assertRedirectedTo('addEventManager');
        $this->assertSessionHasErrors('userPhone');
        
    }
    

    
}