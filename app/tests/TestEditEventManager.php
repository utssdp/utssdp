<?php

class TestEditEventManager extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
    
    public function setUp()
    {
        parent::setUp();
        //Session::start();

        // Enable filters
        Route::enableFilters();
  
    }
    
    public function testEditPage()
    {
        $user = User::find(3);
        $this->be($user);
        
        $this->call('GET', 'users/15/edit');  
        $this->assertResponseOk();
    }
        
    public function testPostEditVolunteer()
    {
        $user = User::find(3);
        
        $this->be($user);
        
        $value = "Successfully Edited the User";
        
        $volunteer = array('userFirstName' => 'Testing', 'userLastName' => 'SPROUT', 'userGender' => 'Male', 'userPhone' => '0410009426', 'userType' => '2');
        
        $this->action('PUT', 'UsersController@doUpdate', '15', $volunteer);
        
        $this->assertRedirectedTo('showUsers');
        $this->assertSessionHas('message', $value);        
        
    }
    
    
    
}