<?php

class TestVolunteerActivity extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
    
    public function setUp()
    {
        parent::setUp();
        Session::start();

        // Enable filters
        Route::enableFilters();
  
    }

   public function testActivityRegistration()
    {
        $user = User::find(3);
        $this->be($user);
        $_SERVER['HTTP_REFERER'] = 'event/profile/1';
        $volunteer = array('activityID' => '1', '$volunteerID' => '3', 'availableFrom' => '09:55', 'availableTo' => '23:55', 'notes' => 'asdadsfaf');
        
        $this->action('POST', 'RegistrationController@register', $volunteer);
        
        
        //$this->assertResponseOk();
        
        $value = "Successfully Applied for Activity";
        
        $this->assertSessionHas('message', $value);
        
    }


    
    public function testEditPage()
    {
        $user = User::find(3);
        $this->be($user);
        
        $this->call('GET', 'volunteer/registrations/1');  
        $this->assertResponseOk();
    }
    
    public function testEdit(){
     
        $this->action('GET', 'RegistrationController@editRegistration', '1');
    }
    
    public function testEditRegistration()
    {
        $user = User::find(3);
        $this->be($user);
        
        $volunteer = array('registrationID' => '1', 'activityID' => '1', '$volunteerID' => '3', 'availableFrom' => '09:55', 'availableTo' => '23:55', 'notes' => 'asdadsfa1111f');
        
        $this->action('POST', 'RegistrationController@updateRegistration', $volunteer);
        
        
        //$this->assertResponseOk();
        
        $value = "Successfully Updated Registration";
        
        $this->assertSessionHas('message', $value);
        
    }
    
    
    
    
    
    
}
