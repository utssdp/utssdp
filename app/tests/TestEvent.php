<?php

class TestEvent extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
    
    public function setUp()
    {
        parent::setUp();
        Session::start();

        // Enable filters
        Route::enableFilters();
  
    }
    
    public function testShowEvent()
    {
        $user = User::find(3);
        $this->be($user);
        
        $this->call('GET', 'event');
        $this->assertResponseOk();
    }
    
    public function testPostAddEvent()
    {
        $user = User::find(3);
        
        $this->be($user);
        
        $event = array('event_name' => 'TestName', 'event_description' => 'TestDesciription', 'event_location' => 'TestLocation', 'event_manager' => NULL, 'eventStartEndDate' => '26/10/2014 - 28/10/2014', 'activity_name' => 'activityname', 'activity_description' => 'activitydescription', 'activity_location' => 'activitylocation', 'activity_isPaid' => 'False', 'activity_number_positions' => '5', 'activity_date' => '26/10/2014', 'activity_start_time' => '06:00', 'activity_end_time' => '23:50');
        
        $value = 'Successfully Created Event';
        
        $this->action('GET', 'EventController@create', null, $event);
        
       // $this->assertRedirectedTo('showEvents');
        //$this->assertSessionHas('message', $value);        
        $this->assertResponseStatus(302);
        
    }
    
    public function testPostOnlyNameAddEvent()
    {
        $user = User::find(3);
        
        $this->be($user);
        
        $event = array('event_name' => 'TestName', 'activity_name' => 'activityname', 'activity_description' => 'activitydescription', 'activity_location' => 'activitylocation', 'activity_isPaid' => 'False', 'activity_number_positions' => '5', 'activity_date' => '26/10/2014', 'activity_start_time' => '06:00', 'activity_end_time' => '23:50');
        
        $value = 'Successfully Created Event';
        
        $this->action('GET', 'EventController@create', null, $event);
        
       // $this->assertRedirectedTo('showEvents');
        //$this->assertSessionHas('message', $value);        
        $this->assertResponseStatus(302);
        
    }
    
    
    
    
}