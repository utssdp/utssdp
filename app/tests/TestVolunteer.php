<?php

class TestVolunteer extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
    
    public function setUp()
    {
        parent::setUp();
        Session::start();

        // Enable filters
        Route::enableFilters();
  
    }
    
    public function testLoginPage()
    {
        
        $this->call('GET', 'login');
        $this->assertResponseOk();
    }
    
    public function testAddVolunteer()
    {
        $user = User::find(3);
        
        $this->be($user);
        
        $this->call('GET', 'addVolunteer');
        $this->assertResponseOk();
    }
    
    public function testPostAddVolunteer()
    {
        $user = User::find(3);
        
        $this->be($user);
        
        $volunteer = array('utsID' => '12345678', 'userFirstName' => 'unitTestFirstName', 'userLastName' => 'unitTestLastName', 'userGender' => 'Male', 'email' => 'unittesting@gmail.com', 'userPhone' => '0410009426', 'userType' => '3');
        
        $value = 'You have Successfully Registered a User.';
        
        $this->action('POST', 'UsersController@doRegister', null, $volunteer);
        
        $this->assertRedirectedTo('addVolunteer');
        $this->assertSessionHas('message', $value);        
        
    }
    
    public function testPostFalseAddVolunteer()
    {
        $user = User::find(3);
        
        $this->be($user);
        
        $eventManager = array('utsID' => '1', 'userFirstName' => 'unitTestFirstName', 'userLastName' => 'unitTestLastName', 'userGender' => 'Male', 'email' => 'unittesting11@gmail.com', 'userPhone' => '0410009426', 'userType' => '3');
        
        
        $this->action('POST', 'UsersController@doRegister', null, $eventManager);
        
        $this->assertRedirectedTo('addVolunteer');
        $this->assertSessionHasErrors('utsID');
        
    }
    
    /*public function testPostNoUTSIDVolunteer()
    {
        $user = User::find(3);
        
        $this->be($user);
        
        $value = 'You have Successfully Registered a User.';
        
        $eventManager = array('userFirstName' => 'unitTestFirstName', 'userLastName' => 'unitTestLastName', 'userGender' => 'Male', 'email' => 'unittesting12@gmail.com', 'userPhone' => '0410009426', 'userType' => '3');
        
        
        $this->action('POST', 'UsersController@doRegister', null, $eventManager);
        
        $this->assertRedirectedTo('addVolunteer');
        $this->assertSessionHas('message', $value); 
        
    }*/
    
    
    public function testPostInvalidNameAddVolunteer()
    {
        $user = User::find(3);
        
        $this->be($user);
        
        $eventManager = array('utsID' => '112345', 'userFirstName' => 'unitTest%FirstName', 'userLastName' => 'unitTestLas%tName', 'userGender' => 'Male', 'email' => 'unittesting11@gmail.com', 'userPhone' => '0410009426', 'userType' => '3');
        
        
        $this->action('POST', 'UsersController@doRegister', null, $eventManager);
        
        $this->assertRedirectedTo('addVolunteer');
        $this->assertSessionHasErrors('userFirstName');
        $this->assertSessionHasErrors('userLastName');
        
    }
    
    public function testPostInvalidEmailAddVolunteer()
    {
        $user = User::find(3);
        
        $this->be($user);
        
        $eventManager = array('utsID' => '112345', 'userFirstName' => 'unitTest%FirstName', 'userLastName' => 'unitTestLastName', 'userGender' => 'Male', 'email' => 'unittesting11gmail.com', 'userPhone' => '0410009426', 'userType' => '3');
        
        
        $this->action('POST', 'UsersController@doRegister', null, $eventManager);
        
        $this->assertRedirectedTo('addVolunteer');
        $this->assertSessionHasErrors('email');
        
    }
    
    public function testPostInvalidPhoneAddVolunteer()
    {
        $user = User::find(3);
        
        $this->be($user);
        
        $eventManager = array('utsID' => '112345', 'userFirstName' => 'unitTest%FirstName', 'userLastName' => 'unitTestLastName', 'userGender' => 'Male', 'email' => 'unittesting11gmail.com', 'userPhone' => '041000$9426', 'userType' => '3');
        
        
        $this->action('POST', 'UsersController@doRegister', null, $eventManager);
        
        $this->assertRedirectedTo('addVolunteer');
        $this->assertSessionHasErrors('email');
        
    }

}
