<?php

class TestAllocateVolunteer extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
    
    public function setUp()
    {
        parent::setUp();
        Session::start();

        // Enable filters
        Route::enableFilters();
  
    }
    
    public function testLoginPage()
    {
        $user = User::find(3);
        $this->be($user);
        
        $this->call('GET', 'volunteer/findRoles/R3');  
        $this->assertResponseOk();
    }


 
    public function testPostEditVolunteer()
    {
        $user = User::find(3);
        
        $this->be($user);
        
        $value = "Successfully Edited the User!!";
        
        $volunteer = array('activityRoleID' => '3', 'registrationID' => '1', 'activityID' => '3');
        
        $this->action('POST', 'RegistrationController@allocate', '15', $volunteer);
        
        //$this->assertRedirectedTo('showUsers');
        //$this->assertSessionHas('message', $value);        
        
    }
    

    
    
    
    
    
}