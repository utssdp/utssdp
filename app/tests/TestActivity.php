<?php

class TestActivity extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
    
    public function setUp()
    {
        parent::setUp();
        Session::start();

        // Enable filters
        Route::enableFilters();
  
    }
    
    
    public function testAddActivity()
    {
        $user = User::find(3);
        $this->be($user);
        
        $this->call('GET', 'activity/add/1');
        $this->assertResponseOk();
        
    }
    
   /* public function testPostAddActivity()
    {
        $user = User::find(3);
        
        $this->be($user);
        
        $event = array('eventID'=>'1','activity_name' => 'activityname', 'activity_description' => 'activitydescription', 'activity_location' => 'activitylocation', 'activity_isPaid' => 'False', 'activity_number_positions' => '5', 'activity_date' => '12/11/2014', 'activity_start_time' => '06:00', 'activity_end_time' => '23:50');
        
        $value = 'Successfully Created Event';
        
        $this->action('GET', 'ActivityController@create', null, $event);
        
       // $this->assertRedirectedTo('showEvents');
        //$this->assertSessionHas('message', $value);        
        $this->assertResponseStatus(302);
        
    }*/
    
}