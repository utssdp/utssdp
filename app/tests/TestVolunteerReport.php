<?php

class TestVolunteerReport extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
    
    public function setUp()
    {
        parent::setUp();
        Session::start();

        // Enable filters
        Route::enableFilters();
  
    }
    
    public function testShowEvent()
    {
        $user = User::find(3);
        $this->be($user);
        
        $this->call('GET', 'volunteer/registrations/3');
        $this->assertResponseOk();
    }
    
    
}