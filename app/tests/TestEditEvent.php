<?php

class TestEditEvent extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
    
    public function setUp()
    {
        parent::setUp();
        Session::start();

        // Enable filters
        Route::enableFilters();
  
    }
    
    public function testShowEvent()
    {
        $user = User::find(3);
        $this->be($user);
        
        $this->call('GET', 'event/edit/2');
        $this->assertResponseOk();
    }
    
    public function testPostAddEvent()
    {
        $user = User::find(3);
        
        $this->be($user);
        
        $event = array('event_name' => 'TestName', 'event_description' => 'TestDesciription', 'event_location' => 'TestLocation', 'event_manager' => NULL, 'eventStartEndDate' => '26/10/2014 - 28/10/2014');
        
        $value = 'Successfully Updated Event';
        
        $this->action('PUT', 'EventController@update', '2', $event);
        
       $this->assertRedirectedTo('showEvents');
       $this->assertSessionHas('message', $value);        
        
    }
    
}