<?php
/**
 * Handling of all activity related functions.
 *
 * @package controllers
 */
class ActivityController extends \BaseController {

	/**
	 * Show a list of Activity
	 */
    public function showActivity($eventID)
	{
		$activities = DB::table('Activity')->where('eventID', '=', $eventID)->where('isArchived', '!=', 1)->get();
        
        return View::make('SEM.showActivity', compact('activities'));
	}
    
    /**
     * Show the view to add an activity to an event based on an eventID
     *
     * @param int $eventID
     */
    
    public function addActivity($eventID){
    
        $event = SemEvent::find($eventID);
        
        $eventDate = $event->eventStartDate;
        
        $eventDate = date('d/m/Y', strtotime($eventDate));
        
        return View::make('SEM.addActivity', compact('eventID'), compact('eventDate'));
    }
    
    /**
     * Show the view to edit an activity
     *
     * @param int $activityID
     */
    
    public function editActivity($activityID){
        
        $activity = DB::table('Activity')->where('activityID', '=', $activityID)->first();
        
        $eventName = DB::table('Event')->select('eventName')->where('eventID', '=', $activity->eventID)->first(); 
        
        return View::make('SEM.editActivity')->with('activity', $activity)->with('eventName',$eventName);
    }
    
    /**
	 * Retrieve the activity based on Activity ID
	 *
	 * @param $activityID
	 */
    
    public function getActivity($activityID)
    {
        $activity = SemActivity::find($activityID);
        $event = SemEvent::find($activity->eventID);
        $roles = DB::table('ActivityRole')
            ->join('Role', 'ActivityRole.roleID', '=', 'Role.roleID')
            ->join('Activity', 'ActivityRole.activityID', '=', 'Activity.activityID')
            ->where('ActivityRole.activityID', '=', $activityID)
            ->get();
        
        $activityRoleIDs = array();
        foreach($roles as $role)
        {
            array_push($activityRoleIDs, $role->activityRoleID);
        }
        
        //should this be moved to another controller
        $rolesStatuses = DB::table('Application')
            ->whereIN('activityRoleID', $activityRoleIDs)
            ->get();
        
        $rolesStatus = array();
        foreach($rolesStatuses as $r)
        {
            $rolesStatus[$r->activityRoleID] = array('selected' => $r->selected, 'confirmed' => $r->confirmed);
        }
        
        $applications = DB::table('Application')
            ->join('Users','Application.VolunteerID','=','Users.id')
            ->whereIN('activityRoleID', $activityRoleIDs)
            ->where('selected','=',1)
            ->get();
        
        $positionsFilled = array();
        $pending = array();
        foreach($applications as $a)
        {
            if(!array_key_exists($a->activityRoleID,$positionsFilled))
                $positionsFilled[$a->activityRoleID] = 0;            
            
            if(!array_key_exists($a->activityRoleID,$pending))
                $pending[$a->activityRoleID] = 0;
            
            if($a->confirmed == 1)
                $positionsFilled[$a->activityRoleID] += 1;    
            else                
                $pending[$a->activityRoleID] += 1;
            
        }       
        
        if(substr(BaseController::getReturnURL(),-14) == substr(URL::route('findVolunteers'),-14))
            Session::flash('find', 'Please select an Activity and Role to continue'); 
        
        return View::make('SEM.activityProfile')
            ->with('activity', $activity)
            ->with('event',$event)
            ->with('roles',$roles)
            ->with('volunteers',$applications)
            ->with('rolesStatus',$rolesStatus)
            ->with('positionsFilled',$positionsFilled)
            ->with('pending', $pending);
    }
    


	/**
     * Create new activity in storage
     */
	public function create()
	{   
        
        $activityDateGet = Input::get('activity_date');
        
        $activityStartSQLDate = date_create_from_format('d/m/Y', $activityDateGet);
        $activityStartSQLTime = date_create_from_format('H:i', Input::get('activity_start_time'));
        $activityEndSQLTime = date_create_from_format('H:i', Input::get('activity_end_time'));
        
        $SQL_Activity_Date = null;
        $SQL_Activity_Start_Time = null;
        $SQL_Activity_End_Time = null;
        
        $event = SemEvent::find(Input::get('eventID')); 
        $eventStartDate = date_create_from_format('Y-m-d H:i:s', $event->eventStartDate);
        $eventEndDate = date_create_from_format('Y-m-d H:i:s', $event->eventEndDate);
        $errors = 0;
                
        if($activityStartSQLDate)
        {
            if($eventStartDate >= $activityStartSQLDate) 
            {
                $eventStartDate = date("d/m/Y", strtotime($event->eventStartDate));
                $create_errors['startDate'] = 'Your date Must be after ' . $eventStartDate;
                $errors = 1;
            }
            else if (date_add($eventEndDate, date_interval_create_from_date_string('1 days')) < $activityStartSQLDate) {
                $eventEndDate = date("d/m/Y", strtotime($event->eventEndDate));
                $create_errors['endDate'] = 'Your Date must on or be before ' . $eventEndDate;
                $errors = 1;
            }
            else
            {
                $SQL_Activity_Date = date_format($activityStartSQLDate, 'Y-m-d');
            }
        }
        
        if($activityStartSQLTime && $activityEndSQLTime)
        {
            if($activityStartSQLTime > $activityEndSQLTime){
                $create_errors['time'] = 'Start Time Cannot be before End Time';
                $errors = 1;
            }
            else
            {
                $SQL_Activity_Start_Time = date_format($activityStartSQLTime, 'H:i:s');   
                $SQL_Activity_End_Time = date_format($activityStartSQLTime, 'H:i:s');   
            }     
        }           
        
        if($errors != 0)
        {
         return Redirect::to(BaseController::getReturnURL())
				->withErrors($create_errors)
                ->withInput(Input::except('password'));// send back all errors to the login form
        }
        
        $userType = Auth::user()->userType;
        if($userType==2 || $userType == 1)//system admin or manager
        {
            $activityStartSQLDate = date_create_from_format('d/m/Y', Input::get('activity_date'));
            $activityStartSQLTime = date_create_from_format('H:i', Input::get('activity_start_time'));
            $activityEndSQLTime = date_create_from_format('H:i', Input::get('activity_end_time'));
        
            $SemActivity = new SemActivity;
            $SemActivity->activityName = Input::get('activity_name');
            $SemActivity->activityDescription = Input::get('activity_description');
            $SemActivity->activityLocation = Input::get('activity_location');
            $SemActivity->positionsAvailable = Input::get('activity_number_positions');
            $SemActivity->isPaid = Input::get('activity_isPaid')[0];
            $SemActivity->activityDate = $SQL_Activity_Date;
            $SemActivity->activityStartDateTime = $SQL_Activity_Start_Time;
            $SemActivity->activityEndDateTime = $SQL_Activity_End_Time;
            $SemActivity->eventID = Input::get('eventID');
            $SemActivity->save();
        
            Session::flash('message', 'Activity has been Added!');
            return Redirect::to('event/' . Input::get('eventID') . '/showActivity');

        }
        else
        {   
            Session::flash('message', 'You are not authorised to create an activity');
            return Redirect::to('event/' . Input::get('eventID'));
        }
	}


	/**
     * Update the specified activity in storage.
     *
     * @param  int  $activityID
     */
	public function update($activityID)
	{
		$rules = array(
			'activity_name'    => 'min:5', // make sure the email is an actual email
            'activity_location'    => 'min:5', // make sure the email is an actual email
            'activity_description'    => 'min:5' // make sure the email is an actual email
		);
        
        $validator = Validator::make(Input::all(), $rules);
        
        $activity = SemActivity::find($activityID);
        
        $registrationPosition = DB::table('Registrations')
            ->where('activityID', $activityID)
            ->where('allocated', '=', '1')
            ->count();    
        
        if ($validator->fails()) 
        {
			return Redirect::to(BaseController::getReturnURL())
				->withErrors($validator)
                ->withInput(Input::except('password'));// send back all errors to the login form				
		} 
        
        else if($registrationPosition > Input::get('activity_number_positions'))
        {
            $create_errors['activity_number_positions'] = 'Your number of positions mus be greater than ' . $registrationPosition;
            $error = 1;
            return Redirect::to(BaseController::getReturnURL())
				->withErrors($create_errors)
                ->withInput(Input::except('password'));// send back all errors to the login form
        }
        
        else
        {
            $activityDateGet = Input::get('activity_date');
            
            $activityStartSQLDate = date_create_from_format('d/m/Y', $activityDateGet);
            $activityStartSQLTime = date_create_from_format('H:i', Input::get('activity_start_time'));
            $activityEndSQLTime = date_create_from_format('H:i', Input::get('activity_end_time'));
            
            $event = SemEvent::find($activity->eventID);
            
            $SQL_Activity_Date = null;
            $SQL_Activity_Start_Time = null;
            $SQL_Activity_End_Time = null;
            
            $eventStartDate = date_create_from_format('Y-m-d H:i:s', $event->eventStartDate);
            $eventEndDate = date_create_from_format('Y-m-d H:i:s', $event->eventEndDate);
            $errors = 0;
            
            if($activityStartSQLDate)
            {
                if($eventStartDate >= $activityStartSQLDate) 
                {
                    $eventStartDate = date("d/m/Y", strtotime($event->eventStartDate));
                    $create_errors['startDate'] = 'Your date Must be after ' . $eventStartDate;
                    $errors = 1;
                }
                else if (date_add($eventEndDate, date_interval_create_from_date_string('1 days')) < $activityStartSQLDate) {
                    $eventEndDate = date("d/m/Y", strtotime($event->eventEndDate));
                    $create_errors['endDate'] = 'Your Date must on or be before ' . $eventEndDate;
                    $errors = 1;
                }
                else
                {
                    $SQL_Activity_Date = date_format($activityStartSQLDate, 'Y-m-d');
                }
            }
            
            if($activityStartSQLTime && $activityEndSQLTime)
            {
                if($activityStartSQLTime > $activityEndSQLTime){
                    $create_errors['time'] = 'Start Time Cannot be before End Time';
                    $errors = 1;
                }
                else
                {
                    $SQL_Activity_Start_Time = date_format($activityStartSQLTime, 'H:i:s');   
                    $SQL_Activity_End_Time = date_format($activityStartSQLTime, 'H:i:s');   
                }     
            }     
            
            if($errors != 0)
            {
                return Redirect::to(BaseController::getReturnURL())
                       ->withErrors($create_errors)
                       ->withInput(Input::except('password'));// send back all errors to the login form
            }
        
            $activityStartSQLDate = date_create_from_format('d/m/Y', Input::get('activity_date'));
            $activityStartSQLTime = date_create_from_format('H:i', Input::get('activity_start_time'));
            $activityEndSQLTime = date_create_from_format('H:i', Input::get('activity_end_time'));

            $activity = SemActivity::find($activityID);

            $activity->activityName = Input::get('activity_name');
            $activity->activityDescription = Input::get('activity_description');
            $activity->activityLocation = Input::get('activity_location');
            $activity->positionsAvailable = Input::get('activity_number_positions');
            $activity->isPaid = Input::get('activity_isPaid')[0];
            $activity->activityDate = $SQL_Activity_Date;
            $activity->activityStartDateTime = $SQL_Activity_Start_Time;
            $activity->activityEndDateTime = $SQL_Activity_End_Time;
            $activity->push();

            Session::flash('message', 'Successfully Updated Activity');
            return Redirect::to('event/' . $activity->eventID . '/showActivity');
        }
	}

	/**
     * Remove the specified activity and archive it.
     *
     * @param  int  $id
     * @param  int  $eventID
     */
	public function destroy($id, $eventID)
	{
		DB::table('Activity')
            ->where('activityID', $id)
            ->update(array('isArchived' => 1, 'updated_at' => DB::raw('NOW()')));
        
        DB::table('Registrations')
            ->where('activityID', $id)
            ->delete();
        
        DB::table('ActivityRole')
            ->where('activityID', $id)
            ->delete();
        
        //Email Notification
        $activityRoles = DB::table('ActivityRole')
            ->where('activityID', '=', $id)
            ->get();
        
        $activityName = DB::table('Activity')
            ->where('activityID', '=', $id)
            ->first();
                
        $activityDate = date("d-m-Y", strtotime($activityName->activityDate));
        
        $eventFirstName = "G.21";
        $eventLastName = "SEM";        

               
        $event = DB::table('Event')
          ->where('eventID', '=', $activityName->eventID)
          ->first();
        
        $eventName = $event->eventName;
        
        if(isset($event->userID)){
              
            $eventManager = DB::table('Users')
            ->where('id', '=', $event->userID)
            ->first();

           $eventFirstName = $eventManager->userFirstName;
           $eventLastName = $eventManager->userLastName;
        }
        
        foreach($activityRoles as $activityRole){

            $applications = DB::table('Application')
                ->where('activityRoleID', '=', $activityRole->activityRoleID)
                ->get();

            $roles = DB::table('Role')
                ->where('roleID', '=', $activityRole->roleID)
                ->first();

            $roleName = $roles->roleName;

            foreach($applications as $application){
                $users = DB::table('Users')
                ->where('id', '=', $application->volunteerID)
                ->get();

                foreach($users as $user){


                    $data = array('userFirstName' => $user->userFirstName, 'email' => $user->email, 'userLastName' => $user->userLastName, 'roleName' => $roleName, 'activityName' => $activityName->activityName, 'eventFirstName' => $eventFirstName, 'eventLastName' => $eventLastName, 'eventName' =>$eventName, 'activityDate' => $activityDate, 'eventLocation' => $event->eventLocation);

                    Mail::send('emails.activityCancellation', $data, function($message) use($data)
                    {
                        $userFirstName = $data['userFirstName'];
                        $userLastName = $data['userLastName'];
                        $email = $data['email'];
                        $name = $userFirstName . " " . $userLastName;
                        $message->to($email, $name)->subject('eVol Events - Activity Cancelled');
                    });
                }


            }
        }
        
        Session::flash('archived', 'Activity has been deleted!');
		return Redirect::to('event/' . $eventID . '/showActivity');

         
	}
    
    /**
	 * Check Activity not duplicated or if it exisits
	 *
	 * @param  int  $id
     * @param  int  $eventID
	 */
    
    public function checkActivity($id, $eventID){
        
        DB::table('Activity')
            ->where('activityID', $id);
        
        $registrations = DB::table('Registrations')
            ->where('activityID', $id);
        
     if($registrations){
            Session::flash('registrationExists', $id);
		    return Redirect::to('event/' . $eventID . '/showActivity');
        }
        else{   
            $this->destroy($id, $eventID);
            Session::flash('archived', 'Activity has been deleted!');
		    return Redirect::to('event/' . $eventID . '/showActivity');
        }
        
        
    }
}
