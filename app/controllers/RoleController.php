<?php
/**
 * Handling of all role related functions.
 *
 * @package controllers
 */
class RoleController extends \BaseController {

	/**
     * Show Create Role View. Parse activity ID & skills
     *
     * @param int $activityID
     */
	public function index($activityID)
	{
        $skills = DB::table('Skill')->get();
        
		return View::make('SEM.addActivityRole')
            ->with('activityID',$activityID)
            ->with('skills',$skills);
	}
    
    /**
     * Show Roles for a particular registration or activity
     *
     * @param int $searchID
     */
    public function showRoles($searchID)
	{
        $volunteerID = null;
        //check if parameter is a registration ID
        if(substr($searchID,0,1)=='R')
        {
            $registrationID = substr($searchID,1);
            
            $roles = DB::table('ActivityRole')
             ->select('*', 'ActivityRole.activityRoleID')
             ->join('Role', 'ActivityRole.roleID', '=', 'Role.roleID')
             ->join('Registrations', 'Registrations.activityID', '=', 'ActivityRole.activityID')
             ->where('Registrations.registrationID', '=', $registrationID)
             ->where('isArchived', '!=', 1)
             ->get();         
        }
        else
        {
            if(substr($searchID,0,1)=='A')
            {
                $activityID = substr($searchID,1);  
                $volunteerID = Input::get('volunteerID');
            }
            else
            {
                $activityID = $searchID;
            }
            
            $roles = DB::table('ActivityRole')
             ->join('Role', 'ActivityRole.roleID', '=', 'Role.roleID')
             ->where('activityID', '=', $activityID)
             ->where('isArchived', '!=', 1)
             ->get(); 
        }
        
        return View::make('SEM.showRoles')
            ->with('roles',$roles)
            ->with('volunteerID', $volunteerID)
            ->with('type',substr($searchID,0,1));
	}  
    
    /**
     * Show Edit Role Form for specified Activity Role
     *
     * @param int $activityRoleID
     */
    public function editRole($activityRoleID)
    {
        $role = DB::table('ActivityRole')
            ->join('Role', 'ActivityRole.roleID', '=', 'Role.roleID')
            ->where('ActivityRole.activityRoleID', '=', $activityRoleID)->get();
        
        $activity = DB::table('Activity')->where('activityID','=',$role[0]->activityID)->get();       
        
        $skills = DB::table('Skill')->get();
        
        $roleSkillsQuery = DB::table('Roleskill')->where('roleID','=',$role[0]->roleID)->get();
        
        $roleSkills = array();
        
        foreach($roleSkillsQuery as $key => $value)
        {
            $roleSkills[] = $value->skillID;     
        }
        
        return View::make('SEM.editRole')
            ->with('role',$role[0])
            ->with('activity',$activity[0])
            ->with('skills',$skills)
            ->with('roleSkills',$roleSkills);
    }
    /**
     * Create a new role in storage.
     */
    public function createActivityRole()
	{
        $userType = Auth::user()->userType;
        if($userType <= 2)//system admin or manager
        {
            $SemRole = new SemRole;
            $SemRole->roleName = Input::get('role_name');
            $SemRole->roleDescription = Input::get('role_description');
            $SemRole->save();
            $LastRoleInsertID = $SemRole->roleID;
        
            $SemActivityRole = new SemActivityRole;
            $SemActivityRole->roleID = $LastRoleInsertID;
            $SemActivityRole->activityID = Input::get('activityID');
            $SemActivityRole->save(); 
        
            Session::flash('message', 'You have Successfully Added a Role.');
            return Redirect::to(BaseController::getReturnURL());
        }
        else
        {        
            Session::flash('error', 'You are not authorised to create add Roles');        
            return Redirect::to(BaseController::getReturnURL());
        }
	}    

	/**
     * Update the specified role in storage.
     *
     * @param  int  $id
     */
	public function updateRole()
	{               
        $input = Input::all();
        
        $ActivityRole = SemActivityRole::find(Input::get('activityRoleID'));
        $roleID = $ActivityRole->roleID;
        $Role = SemRole::find($roleID);
        
        $skills = Input::get('skills');
        
        $this->deleteRoleSkills($roleID);
        
        if(isset($skills))
        {
            foreach($skills as $key => $value)
            {
                $this->addRoleSkill($value, $roleID);
            }     
        }
        
        //check if user created this event or the user is a system admin
        //check security on the Auth::user(), server session variable or browser session?
        if((Auth::user()->id == $this->userCheck(Input::get('activityRoleID'))) || Auth::user()->userType == 1)
        {   
            $Role->roleName = Input::get('role_name');
            $Role->roleDescription = Input::get('role_description');
            
            //need to change this later
            $Role->push();
            
            $ActivityRole->push();
            
            Session::flash('message', 'Successfully Updated Role');        
            return Redirect::to('activity/'.$ActivityRole->activityID.'/showRoles');
        }
        else
        {        
            Session::flash('error', 'You are not authorised to edit this role');      
            return Redirect::to('activity/'.$ActivityRole->activityID.'/showRoles');
        }
	}    
    
    
    /**
     * Update Role Skills in storage
     */
    public function updateSkills()
    {
        $skills = Input::get('skills');
        
        if(isset($skills))
        {
            foreach($skills as $key => $value)
            {
                $skill = new Skill;
                $skill->skillName = $value;
                $skill->save();
            }     
            
        }
        
    }
    
	/**
     * Remove the specified role from storage.
     *
     * @param  int  $id
     * @param  int $activitiyID
     */
	public function destroyRole($id, $activityID)
	{
		DB::table('ActivityRole')
            ->where('activityID', $activityID)
            ->where('roleID', $id)
            ->update(array('isArchived' => 1, 'updated_at' => DB::raw('NOW()')));
        
        //Email Notification
        $activityRoles = DB::table('ActivityRole')
           ->where('roleID', '=', $id)
           ->get();
        
        
        $activityName = DB::table('Activity')
            ->where('activityID', '=', $activityID)
            ->first();
        
        $activityDate = date("d-m-Y", strtotime($activityName->activityDate));
        $activityStartDateTime = date("H:i", strtotime($activityName->activityStartDateTime));
        $activityEndDateTime = date("H:i", strtotime($activityName->activityEndDateTime));
        
        
        $eventFirstName = "G.21";
        $eventLastName = "SEM";        

        
        $event = DB::table('Event')
          ->where('eventID', '=', $activityName->eventID)
          ->first();
        
        $eventName = $event->eventName;
        
        if(isset($event->userID)){
            
            $eventManager = DB::table('Users')
            ->where('id', '=', $event->userID)
            ->first();

            $eventFirstName = $eventManager->userFirstName;
            $eventLastName = $eventManager->userLastName;
        }
        
        
        
        foreach($activityRoles as $activityRole){

            $applications = DB::table('Application')
                ->where('activityRoleID', '=', $activityRole->activityRoleID)
                ->get();

            $roles = DB::table('Role')
                ->where('roleID', '=', $activityRole->roleID)
                ->first();

            $roleName = $roles->roleName;

            foreach($applications as $application){
                $users = DB::table('Users')
                ->where('id', '=', $application->volunteerID)
                ->get();

                foreach($users as $user){


                    $data = array('userFirstName' => $user->userFirstName, 'email' => $user->email, 'userLastName' => $user->userLastName, 'roleName' => $roleName, 'eventFirstName' => $eventFirstName, 'eventLastName' => $eventLastName, 'eventName' =>$eventName, 'activityName' => $activityName->activityName, 'activityDate' => $activityName->activityDate, 'activityStartDateTime' =>$activityStartDateTime, 'activityEndDateTime' =>$activityEndDateTime, 'eventLocation' => $event->eventLocation);

                    Mail::send('emails.roleCancellation', $data, function($message) use($data)
                    {
                        $userFirstName = $data['userFirstName'];
                        $userLastName = $data['userLastName'];
                        $email = $data['email'];
                        $name = $userFirstName . " " . $userLastName;
                        $message->to($email, $name)->subject('eVol Events - Role Cancelled');
                    });
                }


            }
        }
        
        
        Session::flash('archived', 'Role has been deleted!');
		return Redirect::to('activity/' . $activityID . '/showRoles');
	}

    
    
    /**
     * Remove the specified role skills from storage.
     *
     * @param  int $roleID
     */
    private function deleteRoleSkills($roleID)
    {
        DB::table('RoleSkill')
            ->where('roleID',$roleID)
            ->delete();
    }
    
    /**
     * Add role skill assoication to storage.
     *
     * @param  int $key
     * @param  int $roleID
     */
    private function addRoleSkill($key, $roleID)
    {
        $skill = new RoleSkill;
        $skill->skillID = $key;
        $skill->roleID = $roleID;
        $skill->save();        
    }
    
    /**
     * Recursive security check to identify the original creator of the event that this particular role belongs to 
     * 
     */
    private function userCheck($activityRoleID)
    {
        $creator = DB::table('Event')
                        ->join('Activity', 'Activity.eventID', '=', 'Event.eventID')
                        ->join('ActivityRole', 'ActivityRole.activityID', '=', 'Activity.activityID')
                        ->where('ActivityRole.activityRoleID', '=', $activityRoleID)->get();

        return $creator[0]->created_by;
    }

}
