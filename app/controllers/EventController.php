<?php
/**
 * Handling of all event related functions.
 *
 * @package controllers
 */
class EventController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 */
	public function index()
	{
        $eventManagersQuery = $this->getEventManagers();
        
        $eventManagers = array();
            
        $eventManagers[0] = 'No Event Manager Selected';
        
        foreach($eventManagersQuery as $em)
        {
            $eventManagers[$em->id] = $em->userFirstName . ' ' . $em->userLastName;
        }
        
		return View::make('SEM.createEvent')->with('eventManagers', $eventManagers);
	}
    
    
    /**
     * Show the form for creating a new Event
     */
    
    public function showEvents()
	{
        $date = Date('Y-m-d 00:00:00');
        
        $events = DB::table('Event')
                    ->leftjoin('Users','Users.id','=', 'Event.eventManager')
                    ->where('Event.isArchived', '!=', 1)
                    ->whereRAW("(eventEndDate >= '$date' OR eventEndDate IS NULL)")
                    ->get();
        
        $pastEvents = DB::table('Event')
                        ->leftjoin('Users','Users.id','=', 'Event.eventManager')
                        ->where('Event.isArchived', '!=', 1)
                        ->where('eventEndDate', '<', $date)
                        ->get();
        
        if(isset($event->eventStartDate))
            $dateString = date('d/m/Y', strtotime($event->eventStartDate)) .  ' - ' . date('d/m/Y', strtotime($event->eventEndDate));
        else
            $dateString = '';
        
        return View::make('SEM.showEvents', compact('events', 'pastEvents'));
	}
    
    /**
     * Get the Event of an Event ID
     *
     * @param int $eventID
     */
    public function getEvent($eventID)
    {
        //Get the event
        $event = DB::table('Event')
            ->leftjoin('Users','Users.id','=','Event.eventManager')
            ->where('eventID','=',$eventID)
            ->first();
        
        //Get all activities for the event
        $activities = SemActivity::where('eventID', '=', $eventID)->get();
        
        //get all roles for the event
        $roles = DB::table('ActivityRole')
            ->join('Role', 'ActivityRole.roleID', '=', 'Role.roleID')
            ->join('Activity', 'ActivityRole.activityID', '=', 'Activity.activityID')
            ->where('eventID', '=', $eventID)
            ->get();
        
        //extract all activity IDs into an array
        $activityIDs = array();
        foreach($activities as $activity)
        {
            array_push($activityIDs, $activity->activityID);
        }
        
        //get the statuses of all the registrations for a particular activity
        $registrationStatuses = DB::table('Registrations')
            ->whereIN('activityID', $activityIDs)
            ->where('volunteerID', '=', Auth::user()->id)
            ->get();
        
        $registrationStatus = array();
        foreach($registrationStatuses as $r)
        {
            $registrationStatus[$r->activityID] = array('allocated' => $r->allocated, 'confirmed' => $r->confirmed, 'rejected' => $r->rejected, 'declined' => $r->declined,);
        }
        
        //Get all registrations for all the activities that have not been rejected or declined
        $registrations = DB::table('Registrations')
            ->join('Activity','Activity.activityID','=','Registrations.activityID')
            ->whereIN('Registrations.activityID', $activityIDs)
            ->where('allocated','=',1)
            ->where('rejected','=',0)
            ->where('declined','=',0)
            ->distinct()
            ->get();
        
        
        $positionsFilled = array();
        $pending = array();
        
        //sum the positions filled and pending for each activity by looping through each registration
        foreach($registrations as $a)
        {
            //initialise array element for the activity (activityID)      
            if(!array_key_exists($a->activityID,$positionsFilled))
                $positionsFilled[$a->activityID] = 0;       
            if(!array_key_exists($a->activityID,$pending))
                $pending[$a->activityID] = 0;
            
            //if confirmed status increment particular activityID value in positionsFilled array
            if($a->confirmed == 1)
            {    
                $positionsFilled[$a->activityID] += 1;
            }
            //if not confirmed increment particular activityID value in pending array
            else                
            {  
                $pending[$a->activityID] += 1;            
            }
        }       
        
        //if this is a find volunteers process flow, display guide message
        if(substr(BaseController::getReturnURL(),-14) == substr(URL::route('findVolunteers'),-14))
            Session::flash('find', 'Please select an Activity and Role to continue'); 
        
        return View::make('SEM.eventProfile')
            ->with('event',$event)
            ->with('activities',$activities)
            ->with('roles',$roles)
            ->with('registrationStatus',$registrationStatus)
            ->with('pending',$pending)
            ->with('positionsFilled',$positionsFilled);
    }
    
    
    /**
     * Show the custom show events page for the find events process
     */
    
    public function showVolunteerEvents()
	{
		
        $date = Date('Y-m-d 00:00:00');
        
        $events = DB::table('Event')
                    ->where('isArchived', '!=', 1)
                    ->where('eventEndDate', '>=', $date)
                    ->get();        
                  
        if(substr($_SERVER['REQUEST_URI'],-14) == substr(URL::route('findVolunteers'),-14))
            Session::flash('find', 'Please select an Event to continue');              
        
        return View::make('SEM.findEvents', compact('events'));
	}    


	/**
	 * Show the form for editing the specified event.
	 *
	 * @param  int  $id
	 */    
    public function editEvent($eventID)
	{
		$event = DB::table('Event')        
                    ->where('eventID', '=', $eventID)
                    ->first();    
           
        $eventManagersQuery = $this->getEventManagers();
        
        $eventManagers = array();
        
        if(!$event->eventManager)
            $eventManagers[null] = 'No Event Manager Selected';
        
        foreach($eventManagersQuery as $em)
        {
            $eventManagers[$em->id] = $em->userFirstName . ' ' . $em->userLastName;
        }
        
        if(isset($event->eventStartDate))
            $dateString = date('d/m/Y', strtotime($event->eventStartDate)) .  ' - ' . date('d/m/Y', strtotime($event->eventEndDate));
        else
            $dateString = '';
        
        return View::make('SEM.editEvent')
            ->with('event',$event)
            ->with('dateString', $dateString)
            ->with('eventManagers', $eventManagers);
	}
    
    /**
     * Check if specified event not duplicated or if it exisits
     *
     * @param  int  $id
     */    
    public function checkEvent($id)
    {
        $event = DB::table('Event')
            ->where('eventID', $id)
            ->first();
        
        $activities = DB::table('Activity')
            ->where('eventID', '=', $id)
            ->get();
        
        $eventStartDate = date_create_from_format('Y-m-d H:i:s', $event->eventStartDate);
        $currentDate = date('Y-m-d H:i:s');
        
        if($activities)
        {
            Session::flash('activityExist', $id);
		    return Redirect::to('showEvents');
        }
        else if($eventStartDate > $currentDate)
        {
            Session::flash('eventNotPassed', $id);
		    return Redirect::to('showEvents');
        }
        else
        {
            destroy($id);
        }
        
    }

	/**
	 * Create new event in storage
	 */
	public function create()
	{
		$input = Input::all();
        
        $startDate = Input::get('eventStartEndDate');
        
        if(isset($startDate) && $startDate != '') 
        {        
            $startEndDate = explode(" - ", $startDate);
        
            $startSQLDateTime = $startEndDate[0];
            $endSQLDateTime = $startEndDate[1];
        
            $startSQLDateTime = date_create_from_format('d/m/Y', $startSQLDateTime);
            $endSQLDateTime = date_create_from_format('d/m/Y', $endSQLDateTime);
        
            $SQL_Start_Time = date_format($startSQLDateTime, 'Y-m-d');
            $SQL_End_Time = date_format($endSQLDateTime, 'Y-m-d');
        }
        else
        {
            $SQL_Start_Time = null;
            $SQL_End_Time = null;
        }
        
        $id = Auth::user()->id;
        
        //ADD EVENT
        $userType = Auth::user()->userType;
        if($userType==2 || $userType == 1)//system admin or manager
        {
            $event = new SemEvent;
            $event->eventName = Input::get('event_name');
            
            if(Input::get('event_manager') != null && Input::get('event_manager') != 0 )
                $event->eventManager = Input::get('event_manager');
            
            $event->eventLocation = Input::get('event_location');
            $event->eventDescription = Input::get('event_description');
            $event->eventStartTime = Input::get('event_start_time');
            $event->eventStartDate = $SQL_Start_Time;
            $event->eventEndDate = $SQL_End_Time;
            $event->created_by = $id;
            $event->save();
            
            //EVENT INSERTED ID
            $LastInsertID = $event->eventID;
        
            $activityStartSQLDate = date_create_from_format('d/m/Y', Input::get('activity_date'));
            $activityStartSQLTime = date_create_from_format('H:i', Input::get('activity_start_time'));
            $activityEndSQLTime = date_create_from_format('H:i', Input::get('activity_end_time'));
            
            $SQL_Activity_Start = null;
            $SQL_Activity_Start_Time = null;
            $SQL_Activity_End_Time = null;
            
            if($activityStartSQLDate)
                $SQL_Activity_Start = date_format($activityStartSQLDate, 'Y-m-d');
            
            if($activityStartSQLTime)
                $SQL_Activity_Start_Time = date_format($activityStartSQLTime, 'H:i:s');
            
            if($activityEndSQLTime)
                $SQL_Activity_End_Time = date_format($activityEndSQLTime, 'H:i:s');            

        
            //INSERT ACTIVITY
            $SemActivity = new SemActivity;
            $SemActivity->activityName = Input::get('activity_name');
            $SemActivity->activityDescription = Input::get('activity_description');
            $SemActivity->activityLocation = Input::get('activity_location');
            $SemActivity->positionsAvailable = Input::get('activity_number_positions');
            $SemActivity->isPaid = Input::get('activity_isPaid')[0];
            $SemActivity->activityDate = $SQL_Activity_Start;
            $SemActivity->activityStartDateTime = $SQL_Activity_Start_Time;
            $SemActivity->activityEndDateTime = $SQL_Activity_End_Time;
            $SemActivity->eventID = $LastInsertID;
            $SemActivity->save();
            
            $LastInsertID = $SemActivity->activityID;        
            
            Session::flash('message', 'Successfully Created Event');        
            return Redirect::to('showEvents');
        }
        else
        {        
            Session::flash('error', 'You are not authorised to create events');        
            return Redirect::to('showEvents');
        }
	}

	/**
	 * Update the specified event in storage.
	 *
	 * @param  int  $eventID
	 */
	public function update($eventID)
	{
		 $rules = array(
			'event_name'    => 'min:5', // make sure the email is an actual email
            'event_location'    => 'min:5', // make sure the email is an actual email
            'event_description'    => 'min:5' // make sure the email is an actual email
		);
        
        $validator = Validator::make(Input::all(), $rules);
        
        
        if ($validator->fails()) {
			return Redirect::to(BaseController::getReturnURL())
				->withErrors($validator)
                ->withInput(Input::except('password'));// send back all errors to the login form				
		} 
        else{
            $input = Input::all();
            
            $startDate = Input::get('eventStartEndDate');
            
            if(isset($startDate) && $startDate != '') {
                $startEndDate = explode(" - ", $startDate);
                
                $startSQLDateTime = $startEndDate[0];
                $endSQLDateTime = $startEndDate[1];
                
                $startSQLDateTime = date_create_from_format('d/m/Y', $startSQLDateTime);
                $endSQLDateTime = date_create_from_format('d/m/Y', $endSQLDateTime);
                
                $SQL_Start_Time = date_format($startSQLDateTime, 'Y-m-d');
                $SQL_End_Time = date_format($endSQLDateTime, 'Y-m-d');
            }
            else{
                $SQL_Start_Time = null;
                $SQL_End_Time = null;
            }
            

            $userID = Auth::user()->id;
            $userType = Auth::user()->userType;
            $event = SemEvent::find($eventID);

            //check if user created this event or the user is a system admin
            //check security on the Auth::user(), server session variable or browser session
            if(($userID == $event->eventManager) || $userType == 1)
            {            
                $event->eventName = Input::get('event_name');
                
                if(Input::get('event_manager') != null)
                    $event->eventManager = Input::get('event_manager');
                
                $event->eventLocation = Input::get('event_location');
                $event->eventDescription = Input::get('event_description');
                $event->eventStartTime = Input::get('event_start_time');
                $event->eventStartDate = $SQL_Start_Time;
                $event->eventEndDate = $SQL_End_Time;
                $event->push();

                Session::flash('message', 'Successfully Updated Event');        
                return Redirect::to('showEvents');
            }
            else
            {        
                Session::flash('error', 'You are not authorised to edit this event');        
                return Redirect::to('showEvents');
            }
        }
	}
    
    /**
     * Archive the specified event in storage.
     *
     * @param  int  $id
     */
	public function destroy($id)
	{
		// delete
        DB::table('Event')
            ->where('eventID', $id)
            ->update(array('isArchived' => 1, 'updated_at' => DB::raw('NOW()')));
	
        DB::table('Activity')
            ->where('eventID', $id)
            ->update(array('isArchived' => 1, 'updated_at' => DB::raw('NOW()')));
        
        DB::table('Registrations')
            ->join('Activity','Activity.activityID','=','Registrations.activityID')
            ->where('eventID', $id)
            ->delete();
        
        DB::table('ActivityRole')
            ->join('Activity','Activity.activityID','=','ActivityRole.activityID')
            ->where('eventID', $id)
            ->delete();
        
        
        $event = DB::table('Event')
            ->where('eventID', $id)
            ->first();
        
        $eventFirstName = "G.21";
        $eventLastName = "SEM";
        $eventName = $event->eventName;
        $eventStartDate = date("d-m-Y", strtotime($event->eventStartDate));
        $eventEndDate = date("d-m-Y", strtotime($event->eventEndDate));
        
 
          if(isset($event->userID))
           {
               $eventManager = DB::table('Users')
                ->where('id', '=', $event->userID)
                ->first();

               $eventFirstName = $eventManager->userFirstName;
               $eventLastName = $eventManager->userLastName;
           }
        
        //Email Notice
        $activities = DB::table('Activity')
            ->where('eventID', '=', $id)
            ->get();
            
        
        foreach ($activities as $activity)
        {
             $activityRoles = DB::table('ActivityRole')
                    ->where('activityID', '=', $activity->activityID)
                    ->get();
                
                foreach($activityRoles as $activityRole)
                {
                    $registrations = DB::table('Registrations')
                        ->where('activityRoleID', '=', $activityRole->activityRoleID)
                        ->get();
                    
                    $roles = DB::table('Role')
                        ->where('roleID', '=', $activityRole->roleID)
                        ->first();
                    
                    $roleName = $roles->roleName;
                    
                    foreach($registrations as $registration)
                    {
                        $users = DB::table('Users')
                        ->where('id', '=', $registration->volunteerID)
                        ->get();
                        
                        foreach($users as $user)
                        {
                            $data = array('userFirstName' => $user->userFirstName, 'email' => $user->email, 'userLastName' => $user->userLastName, 'roleName' => $roleName, 'eventFirstName' => $eventFirstName, 'eventLastName' => $eventLastName, 'eventName' =>$eventName, 'eventLocation' => $event->eventLocation, 'eventStartDate' => $eventStartDate, 'eventEndDate' => $eventEndDate);
                            
                            Mail::send('emails.eventCancellation', $data, function($message) use($data)
                            {
                                $userFirstName = $data['userFirstName'];
                                $userLastName = $data['userLastName'];
                                $email = $data['email'];
                                $name = $userFirstName . " " . $userLastName;
                                $message->to($email, $name)->subject('eVol Events - Event Cancelled');
                            });
                        }

                                            
                    }
                }
        }
        
        
        
		// redirect
		Session::flash('archived', 'Event has been deleted!');
		return Redirect::to('showEvents');
	}   

    /**
     * Get all event managers from storage
     *
     */ 
    private function getEventManagers()
    {
        $eventManagersQuery = DB::table('Users')->where('userType', '<=', 2)->get();    
        
        return $eventManagersQuery;
    }
}
