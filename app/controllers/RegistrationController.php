<?php
/**
 * Handling of all registration related functions.
 *
 * @package controllers
 */
class RegistrationController extends \BaseController {
    
    /**
     * Display all registrations. Overloaded to serve both admin and volunteer
     * 
     * @param int $volunteerID
     */
    public function showRegistrations($volunteerID = null)
    {
        $registrationsCheck = null;
        
        if($volunteerID == null && (Auth::user()->userType == 1))
        {
            $registrations = DB::table('Registrations')
                ->select('*', 'Registrations.created_at','Registrations.updated_at')
                ->join('Activity', 'Activity.activityID', '=', 'Registrations.activityID')
                ->join('Event', 'Event.eventID', '=', 'Activity.eventID')
                ->join('Users', 'Users.id', '=', 'Registrations.volunteerID')
                ->where('Registrations.allocated','=',0)
                ->where('Registrations.rejected','=',0)
                ->get();     
            
            //run a query returning the number registered and allocated to each activity + positionsAvailable for each activity
            $registeredQuery = DB::table('Registrations')
                ->selectRaw('Registrations.activityID, COUNT(*) AS Registered, SUM(Registrations.allocated) AS Allocated, Activity.positionsAvailable')
                ->join('Activity', 'Activity.activityID', '=', 'Registrations.activityID')
                ->groupBy('Registrations.activityID')
                ->get();
            
            $registrationsCheck = array();
            
            foreach($registeredQuery as $r)
            {
                if($r->Allocated >= $r->positionsAvailable)
                {
                    $registrationsCheck[$r->activityID] = 'A'; //set over allocated status code      
                }
                else if($r->Registered >= $r->positionsAvailable)
                {
                    $registrationsCheck[$r->activityID] = 'R'; //set over registered status code                    
                }
            } 
        }
        else if($volunteerID == null && (Auth::user()->userType == 2))
        {
            //this needs to be completed
            $registrations = DB::table('Registrations')
                ->select('*', 'Registrations.created_at','Registrations.updated_at')
                ->join('Activity', 'Activity.activityID', '=', 'Registrations.activityID')
                ->join('Event', 'Event.eventID', '=', 'Activity.eventID')
                ->join('Users', 'Users.id', '=', 'Registrations.volunteerID')
                ->where('Registrations.allocated','=',0)
                ->where('Registrations.rejected','=',0)
                ->get();      
        }
        else if(isset($volunteerID))
        {            
            $registrations = DB::table('Registrations')
                ->select('*', 'Registrations.created_at','Registrations.updated_at')
                ->join('Activity', 'Activity.activityID', '=', 'Registrations.activityID')
                ->join('Event', 'Event.eventID', '=', 'Activity.eventID')
                ->join('Users', 'Users.id', '=', 'Registrations.volunteerID')
                ->where('Registrations.volunteerID','=',$volunteerID)
                ->where('Registrations.allocated','=',0)
                ->get();   
        }
        
        return View::make('SEM.showRegistrations')
            ->with('registrations',$registrations)
            ->with('volunteerID',$volunteerID)
            ->with('registrationsCheck',$registrationsCheck);
    }
    
    /**
     * Display all allocations. Overloaded to serve both admin and volunteer
     * 
     * @param int $volunteerID
     */
    public function showAllocations($volunteerID = null)
    {
        if($volunteerID == null && (Auth::user()->userType <= 2))
        {
            $allocations = DB::table('Registrations')
                ->join('ActivityRole','ActivityRole.activityRoleID', '=', 'Registrations.activityRoleID')
                ->join('Role', 'ActivityRole.roleID', '=', 'Role.roleID')
                ->join('Activity', 'Activity.activityID', '=', 'ActivityRole.activityID')
                ->join('Event', 'Event.eventID', '=', 'Activity.eventID')
                ->join('Users', 'Users.id', '=', 'Registrations.volunteerID')
                ->where('Registrations.allocated','=',1)
                ->where('Registrations.declined','=',0)
                ->where('Registrations.rejected','=',0)
                ->get();
        }
        else if(isset($volunteerID))
        {        
            $allocations = DB::table('Registrations')
                ->join('ActivityRole','ActivityRole.activityRoleID', '=', 'Registrations.activityRoleID')
                ->join('Role', 'ActivityRole.roleID', '=', 'Role.roleID')
                ->join('Activity', 'Activity.activityID', '=', 'ActivityRole.activityID')
                ->join('Event', 'Event.eventID', '=', 'Activity.eventID')
                ->join('Users', 'Users.id', '=', 'Registrations.volunteerID')
                ->where('Registrations.allocated','=',1)
                ->where('Registrations.rejected','=',0)
                ->where('Registrations.declined','=',0)
                ->where('Registrations.volunteerID', '=', Auth::user()->id)
                ->get();        
        }
        
        return View::make('SEM.showAllocations')
            ->with('allocations',$allocations)
            ->with('volunteerID',$volunteerID); 
    }  
    
    /**
     * Register volunteer for activity. Overloaded to server both event managers and volunteers
     * 
     * @param boolean $directAllocate
     */
    public function register($directAllocate = false)
    {           
        //get activityID for registration
        $activityID = Input::get('activityID');
        
        //check if event manager is selecting volunteers directly
        if($directAllocate)
        {
            $volunteerID = Input::get('volunteerID');
        }
        else
        {
            $volunteerID = Auth::user()->id;
        
            //check if this is a duplicate registration
        
            $check_activity = SemActivity::find(Input::get('activityID'));
            $activityStartSQLTime = date('H:i', strtotime($check_activity->activityStartDateTime)) + (60*2);
            $activityEndSQLTime = date('H:i', strtotime($check_activity->activityEndDateTime)) - (60*2);
        
            $dateFrom = date('H:i', strtotime(Input::get('availableFrom')));
            $dateTo = date('H:i', strtotime(Input::get('availableTo')));
        
            $errors = 0;
    
            if($dateFrom >= $dateTo){
                $create_errors['time'] = $check_activity->activityName . ' Start Time Cannot be before End Time ';
                $errors = 1;            
            }
            if($dateFrom >= $activityStartSQLTime){
                $create_errors['startTime'] = $check_activity->activityName . ' Start Time Cannot be after than ' . $activityStartSQLTime . ' ';
                $errors = 1;          
            }
        
            if($dateTo <= $activityEndSQLTime){
                $create_errors['endTime'] = $check_activity->activityName . ' End Time Cannot be before than ' . $activityEndSQLTime . ' ';
                $errors = 1;          
            }
        
        
            if($errors != 0)
            {
             return Redirect::to(BaseController::getReturnURL())
				    ->withErrors($create_errors)
                    ->withInput(Input::except('password'));// send back all errors to the login form
            }
        }
        if(!$this->duplicateRegistration($activityID, $volunteerID))
        {
            $Registration = new SemRegistration;
            $Registration->activityID = $activityID;
            $Registration->volunteerID = $volunteerID;
            $Registration->notes = Input::get('notes');
            $Registration->availableFrom = date_create_from_format('H:i', Input::get('availableFrom'));
            $Registration->availableTo = date_create_from_format('H:i', Input::get('availableTo'));
            $Registration->confirmed = 0;
            $Registration->allocated = 0;
            
            if($Registration->push())
            {                
                $currentuser = User::find($volunteerID);
                
                $mailDetails = array('userFirstName' => $currentuser->userFirstName,'userLastName' =>  $currentuser->userLastName);
                
                //pull all data from Registration, activity, event and users
                $fullDetails = DB::table('Registrations')
                 ->join('Activity', 'Activity.activityID', '=', 'Registrations.activityID')
                 ->join('Event', 'Event.eventID', '=', 'Activity.eventID')
                 ->join('Users', 'Users.id', '=', 'Registrations.volunteerID')
                 ->where('Registrations.volunteerID', '=', $currentuser->id)
                 ->first();
                
                $mailDetails['activityName'] = $fullDetails->activityName;
                $mailDetails['activityDate'] = $fullDetails->activityDate;
                $mailDetails['activityStartDateTime'] = $fullDetails->activityStartDateTime;
                $mailDetails['activityEndDateTime'] = $fullDetails->activityEndDateTime;
                $mailDetails['eventName'] = $fullDetails->eventName;
                $mailDetails['eventLocation'] = $fullDetails->eventLocation;
                
                try 
                {      
                    
                    if(!$directAllocate) {                        
                        Mail::send('emails.registrationConfirmation', $mailDetails, function($message)
                        {       
                            $currentuser = User::find(Auth::user()->id);
                            $userFirstName = $currentuser->userFirstName;
                            $userLastName = $currentuser->userLastName;
                            $email = $currentuser->email;
                            $name = $userFirstName . " " . $userLastName;
                            $message->to($email, $name)->subject('eVol Events - Registration Confirmation');
                        });                    
                    } 
                    else{
                        
                        Mail::send('emails.registrationConfirmation', $mailDetails, function($message)
                        {       
                            $currentuser = User::find(Input::get('volunteerID'));
                            $userFirstName = $currentuser->userFirstName;
                            $userLastName = $currentuser->userLastName;
                            $email = $currentuser->email;
                            $name = $userFirstName . " " . $userLastName;
                            $message->to($email, $name)->subject('eVol Events - Registration Confirmation');
                        }); 
                        
                    }
                    
                    $numberRegistration = DB::table('registrations')
                                    ->where('activityID', $activityID)
                                    ->count();
                    
                    if($numberRegistration >= $check_activity->positionsAvailable){
                        Session::flash('messageFull', 'Successfully Applied for Activity - There are more registrations than people requried!');        
                    }
                    else{
                        Session::flash('message', 'Successfully Applied for Activity');        
                    }
                    
                    if($directAllocate)
                       $this->allocate($Registration->registrationID);
                    else
                       return Redirect::to(BaseController::getReturnURL());
                }
                catch ( Exception $e )
                {
                    Session::flash('warning', 'Email Confirmation Failed to Send');        
                    
                    if($directAllocate)
                        $this->allocate($Registration->registrationID);
                    else
                       return Redirect::to(BaseController::getReturnURL());
                }
            }
            else
            {
                Session::flash('error', 'Error in applying for this role');          
                return Redirect::to(BaseController::getReturnURL());                
            }
        }  
        else
        {
            Session::flash('error', 'You have already applied for this particular role');          
            return Redirect::to(BaseController::getReturnURL());   
        }          
    }
    
    /**
     * Show edit form for registration.
     * 
     * @param int $registrationID
     */
    public function editRegistration($registrationID)
    {             
        $registration = DB::table('Registrations')
            ->select('*', 'Registrations.created_at','Registrations.updated_at')
            ->join('Activity', 'Activity.activityID', '=', 'Registrations.activityID')
            ->join('Event', 'Event.eventID', '=', 'Activity.eventID')
            ->join('Users', 'Users.id', '=', 'Registrations.volunteerID')
            ->where('Registrations.registrationID','=',$registrationID)
            ->first();
        
        return View::make('SEM.editRegistration')
			->with('registration', $registration);
    }
    
    /**
     * Update registration in storage
     */
    public function updateRegistration()
    {  
	    $registration = SemRegistration::find(Input::get('registrationID'));
        
        $registration->availableFrom = date_create_from_format('H:i', Input::get('availableFrom'));
        $registration->availableTo = date_create_from_format('H:i', Input::get('availableTo'));
        $registration->notes = Input::get('notes');
        
        if($registration->push())
        {
            Session::flash('message', 'Successfully Updated Registration');        
            return Redirect::to('volunteer/registrations/' . Auth::user()->id );
        }
        else
        {
            Session::flash('error', 'Error updating registration');         
            return Redirect::to('volunteer/registrations/' . Auth::user()->id );     
        }
    }
    
    /**
     * Renove registration from storage
     */
    public function deleteRegistration()
    {                
        $registrationID = Input::get('registrationID');
        
		$registration = SemRegistration::find($registrationID);
        
        $registration->delete();
		
		// redirect
		Session::flash('delete', 'Registration has been deleted!');  
        return Redirect::to('volunteer/registrations/' . Auth::user()->id );
    }
    
    /**
     * Allocate volunteer to activity role
     * 
     * @param int $registrationID
     */
    public function allocate($registrationID = null)
    {      
        if(Input::get('registrationID'))
            $registrationID = Input::get('registrationID');
        
        if(Input::get('activityID'))
            $activityID = Input::get('activityID');
        
            $numberRegistration = DB::table('registrations')
                                    ->where('activityID', $activityID)
                                    ->where('allocated', '1')
                                    ->count();
        
            $activityDetails = SemActivity::find($activityID);
        
        
            if($numberRegistration <= $activityDetails->positionsAvailable){
                
                $registration = SemRegistration::find($registrationID);
                $registration->activityRoleID = Input::get('activityRoleID');
                $registration->allocated = 1;

                //user validation needed here
                if($registration->push())
                {
                    $fullDetails = DB::table('Registrations')
                     ->join('ActivityRole','ActivityRole.activityRoleID', '=', 'Registrations.activityRoleID')
                     ->join('Role', 'ActivityRole.roleID', '=', 'Role.roleID')
                     ->join('Activity', 'Activity.activityID', '=', 'ActivityRole.activityID')
                     ->join('Event', 'Event.eventID', '=', 'Activity.eventID')
                     ->join('Users', 'Users.id', '=', 'Registrations.volunteerID')
                     ->where('Registrations.registrationID', '=', $registration->registrationID)
                     ->first();

                    $mailDetails = array('userFirstName' => $fullDetails->userFirstName,'userLastName' =>  $fullDetails->userLastName);

                    $mailDetails['roleName'] = $fullDetails->roleName;
                    $mailDetails['activityName'] = $fullDetails->activityName;
                    $mailDetails['activityDate'] = $fullDetails->activityDate;
                    $mailDetails['activityStartDateTime'] = $fullDetails->activityStartDateTime;
                    $mailDetails['activityEndDateTime'] = $fullDetails->activityEndDateTime;
                    $mailDetails['eventName'] = $fullDetails->eventName;
                    $mailDetails['eventLocation'] = $fullDetails->eventLocation;
                    $mailDetails['email'] = $fullDetails->email;

                    try {   

                        Mail::send('emails.roleConfirmation', $mailDetails, function($message) use($mailDetails)
                        {

                            $name = $mailDetails['userFirstName'] . " " . $mailDetails['userLastName'];

                            $message->to($mailDetails['email'], $name)->subject('eVol Events - Role Allocated');
                        });

                        Session::flash('message', 'Successfully Allocated Volunteer for Role.');          
                        return Redirect::to('volunteer/registrations/');
                    }
                    catch ( Exception $e )
                    {
                        Session::flash('warning', 'Successfully Allocated Volunteer. Email Confirmation Failed to Send'); 
                        return Redirect::to('volunteer/registrations/');
                    }            
                } 
                
            }
            else{
                Session::flash('noMorePositions', 'No More Postions Avaliable!'); 
                return Redirect::to('volunteer/registrations/');
            }
        
                    
    }
    
    /**
     * Reject volunteer registration
     */
    public function registerAllocate()
    {
        //need to add controller for event managers
        Session::flash('noMorePositions', 'No More Postions Avaliable!');   
        return Redirect::to('volunteer/findRoles/A'. Input::get('activityID') . '?volunteerID=' . Input::get('volunteerID'));
        if(Auth::user()->userType <= 2)
        {            
            if(Input::get('activityID'))
                $activityID = Input::get('activityID');
            
            $numberRegistration = DB::table('registrations')
                                    ->where('activityID', $activityID)
                                    ->where('allocated', '1')
                                    ->count();
            $activityDetails = SemActivity::find($activityID);
            
            
            if($numberRegistration <= $activityDetails->positionsAvailable){
                $this->register(true);
                Session::flash('message', 'Successfully Allocated Volunteer.');    
                return $this->findVolunteers(Input::get('activityID'));
            }
            else{
                Session::flash('noMorePositions', 'No More Postions Avaliable!');   
                return Redirect::to('volunteer/findRoles/A'. Input::get('activityID') . '?volunteerID=' . Input::get('volunteerID'));
            }
        }           
    }
    
    /**
     * Reject volunteer registration
     */
    public function reject()
    {                          
        $registrationID = Input::get('registrationID');
        
		$registration = SemRegistration::find($registrationID);
        
        $registration->rejected = 1;      
        
        if($registration->push())
        {
            if(Input::get('remove'))
            {
                Session::flash('reject', 'Sucessfully Removed Volunteer from Role');        
                return Redirect::to('volunteer/allocations/');            
            }
            else
            {
                Session::flash('reject', 'Sucessfully Rejected Registration');        
                return Redirect::to('volunteer/registrations/');
            }
        }
        else
        {
            if(Input::get('remove'))
            {
                Session::flash('error', 'Unable to Remove Volunteer from Event');        
                return Redirect::to('volunteer/allocations/');            
            }
            else
            {
                Session::flash('error', 'Unable to Reject Registration');        
                return Redirect::to('volunteer/registrations/');
            }     
        }       
    }
    
    /**
     * Switch method to confirm, decline or withdraw allocation
     */
    public function select()
    {
        if(Input::get('confirm')) 
        {
            return $this->confirm(); //if confirm then use this method
        } 
        elseif(Input::get('decline')) 
        {
            return $this->decline(); //if decline then use this method
        }
        elseif(Input::get('withdraw'))
        {            
            return $this->decline(true); //if withdraw then use this method
        }
    }
    
    /**
     * Confirm role allocation
     */
    private function confirm()
    {
        $registrationID = Input::get('registrationID');
        
        $registration = SemRegistration::find($registrationID);
        
        $registration->confirmed = 1;
        
        //user validation needed here
        if($registration->push())
        {
            $volunteerID = Auth::user()->id;
            $currentuser = User::find($volunteerID);
            
            $mailDetails = array('userFirstName' => $currentuser->userFirstName,'userLastName' =>  $currentuser->userLastName);

            $fullDetails = DB::table('Registrations')
             ->join('ActivityRole','ActivityRole.activityRoleID', '=', 'Registrations.activityRoleID')
             ->join('Role', 'ActivityRole.roleID', '=', 'Role.roleID')
             ->join('Activity', 'Activity.activityID', '=', 'ActivityRole.activityID')
             ->join('Event', 'Event.eventID', '=', 'Activity.eventID')
             ->join('Users', 'Users.id', '=', 'Registrations.volunteerID')
             ->where('Registrations.volunteerID', '=', $currentuser->id)
             ->first();

            $mailDetails['roleName'] = $fullDetails->roleName;
            $mailDetails['activityName'] = $fullDetails->activityName;
            $mailDetails['activityDate'] = $fullDetails->activityDate;
            $mailDetails['activityStartDateTime'] = $fullDetails->activityStartDateTime;
            $mailDetails['activityEndDateTime'] = $fullDetails->activityEndDateTime;
            $mailDetails['eventName'] = $fullDetails->eventName;
            $mailDetails['eventLocation'] = $fullDetails->eventLocation;
            
            try {   
                Mail::send('emails.assignmentConfirmation', $mailDetails, function($message)
                {
                    $currentuser = User::find(Auth::user()->id);
                    $userFirstName = $currentuser->userFirstName;
                    $userLastName = $currentuser->userLastName;
                    $email = $currentuser->email;
                    $name = $userFirstName . " " . $userLastName;
                    $message->to($email, $name)->subject('eVol Events - Role Confirmed');
                });
                

                Session::flash('message', 'Successfully Confirmed Role.');          
                return Redirect::to(BaseController::getReturnURL());   
            }
            catch ( Exception $e )
            {
                Session::flash('warning', 'Successfully Confirmed Role. Email Confirmation Failed to Send');             
                return Redirect::to(BaseController::getReturnURL());   
            }          
        }            
    }    
    
    /**
     * Decline role allocation. Overloaded for volunteers to withdraw previously confirmed role
     * 
     * @param boolean $withdraw
     */
    private function decline($withdraw = false)
    {                          
        $registrationID = Input::get('registrationID');
        
		$registration = SemRegistration::find($registrationID);
        
        $registration->declined = 1;      
        
        if($withdraw)
        {
            $registration->confirmed = 0;
        }
        
        if($registration->push())
        {
            Session::flash('declined', 'Successfully Declined Role Allocation');         
            return Redirect::to(BaseController::getReturnURL()); 
        }
        else
        {
            Session::flash('error', 'An error occured when trying to decline your allocation');             
            return Redirect::to(BaseController::getReturnURL()); 
        }       
    }
     
    /**
     * Check if registration for activity has already been submitted
     * 
     * @param int $activityID
     * @param int $volunteerID
     */
    private function duplicateRegistration($activityID, $volunteerID)
    {
        $result = DB::table('Registrations')
            ->where('volunteerID','=',$volunteerID)
            ->where('activityID','=',$activityID)
            ->get();
        
        $count = count($result);
        
        return (($count > 0) ? true : false);
    }
}
?>