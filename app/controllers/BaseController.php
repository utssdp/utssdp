<?php
/**
 * BaseController inherit by all controllers
 *
 * @package controllers
 */
class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
    
    /**
     * Return the previous URL
     */
    public function getReturnURL()
    {
        return $_SERVER['HTTP_REFERER'];
    }

}
