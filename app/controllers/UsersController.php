<?PHP
/**
 * Handling of all user related functions.
 *
 * @package controllers
 */
class UsersController extends BaseController{
    
    public function getIndex(){
        return View::make('SEM.index');
    }
    
    /**
     * Display the Login page
     */
    public function showLogin(){
        return View::make('SEM.login');
    }
    
    /**
     * Return all user details. Include reporting details
     * 
     * @param int $volunteerID
     */
    public function getUser($volunteerID)
    {        
        $userRecords = DB::table('Users')
            ->select('*', 'Registrations.created_at AS created')
            ->leftjoin('Registrations','Registrations.volunteerID','=','Users.id')
            ->leftjoin('ActivityRole','ActivityRole.activityRoleID','=','Registrations.activityRoleID')
            ->leftjoin('Activity','Activity.activityID','=','Registrations.activityID')
            ->leftjoin('Event','Event.eventID','=','Activity.eventID')
            ->leftjoin('Role','Role.roleID','=','ActivityRole.roleID')
            ->where('id', '=', $volunteerID)
            ->get();
        
        $userSkills = DB::table('UserSkill')
            ->join('Skill','Skill.skillID', '=', 'UserSkill.skillID')
            ->where('userID','=', $volunteerID)
            ->get();
        
        $userTrainings = DB::table('UserTraining')
            ->join('Training','Training.trainingID', '=', 'UserTraining.trainingID')
            ->where('userID','=', $volunteerID)
            ->get();
        
        $participations = 0;
        $registrations = 0;
      
        foreach($userRecords as $userRecord)
        {
            if(($userRecord->confirmed) && (!$userRecord->rejected) && (!$userRecord->declined))
                $participations++;
            else
                $registrations++;
        }   
        
        $requestString = explode('/',$_SERVER['REQUEST_URI']);
        
        //Check if this is a report request by examining uri string for "report" keyword
        if(in_array('registrations',$requestString))
        {   
            $view = View::make('reports.volunteerRegistrationReport')
                    ->with('userRecords',$userRecords)
                    ->with('userSkills', $userSkills)
                    ->with('userTrainings', $userTrainings)
                    ->with('registrations', $registrations)
                    ->with('participations', $participations)
                    ->render();  
            
            $pdf = App::make('dompdf');
            $pdf->loadHTML($view);
            $pdf->setPaper('a4', 'landscape');
            return $pdf->stream('volunteer_registrations.pdf');
        }
        elseif(in_array('participation',$requestString))
        {   
            $view = View::make('reports.volunteerParticipationReport')
                    ->with('userRecords',$userRecords)
                    ->with('userSkills', $userSkills)
                    ->with('userTrainings', $userTrainings)
                    ->with('registrations', $registrations)
                    ->with('participations', $participations)
                    ->render();  
            
            $pdf = App::make('dompdf');
            $pdf->loadHTML($view);
            $pdf->setPaper('a4', 'landscape');
            return $pdf->stream('volunteer_participation.pdf');
        }
        else
        {
            return View::make('SEM.volunteerProfile')
                ->with('userRecords',$userRecords)
                ->with('userSkills', $userSkills)
                ->with('userTrainings', $userTrainings)
                ->with('registrations', $registrations)
                ->with('participations', $participations);    
        }
    }
    
    /**
     * Find Volunteers. Overloaded to serve both admin and volunteer
     * 
     * @param int $volunteerID
     */
    public function findVolunteers($activityID)
    {   
        $activity = SemActivity::find($activityID);
        
        $day = date('l',strtotime($activity->activityDate));
        
        /*
        $roleSkillCheck = DB::table('RoleSkill')
        ->where('roleID','=',$role->roleID)
        ->get();
        
        if(count($roleSkillCheck)>0)
        {
        $users = DB::table('Users')
        ->join('UserSkill', 'UserSkill.userID', '=', 'Users.id')
        ->join('Skill', 'Skill.skillID', '=', 'UserSkill.skillID')
        ->join('RoleSkill', 'RoleSkill.skillID', '=', 'Skill.skillID')
        ->join('Role', 'RoleSkill.roleID', '=', 'Role.roleID')
        ->where('availability'.substr($day,0,3),'=',1)
        ->where('isArchived', '!=', 1)
        ->where('Role.roleID','=', $role->roleID)
        ->whereRaw('id NOT IN (select volunteerID from Registrations where activityRoleID = '.$activityRoleID.')')
        ->groupBy('id')
        ->havingRaw('count(distinct `UserSkill`.`skillID`) = (select COUNT(*) from `RoleSkill` where `roleID` = '. $role->roleID .')')
        ->get();
        }       
        else            
        {*/
        $users = DB::table('Users')
                //->where('availability'.substr($day,0,3),'=',1)
                ->where('isArchived', '!=', 1)
                ->whereRaw('id NOT IN (select volunteerID from Registrations where activityID = '.$activityID.' AND allocated = 0 AND rejected = 0 AND declined = 0)')
                ->get();
        
        return View::make('SEM.findVolunteers')
            ->with('users', $users)
            ->with('activityID', $activityID);   
    }
    
    /**
     * Display event manager report. Optional parameter to display event report for a specific event.
     *
     * @param  int  $eventManagerID
     * @param  int  $eventID
     */   
    public function displayEventManagerReport($eventManagerID, $eventID = null)
    {            
        //declare $records array 
        $records = array();
        
        if($eventID != null)
        {
            //Get the event
            $event = DB::table('Event')
                ->leftjoin('Users','Users.id','=','Event.eventManager')
                ->where('eventID','=',$eventID)
                ->first();
            
            //Get all activities for the event
            $activities = SemActivity::where('eventID', '=', $eventID)->get();
            
            //extract all activity IDs into an array
            $activityIDs = array();
            foreach($activities as $activity)
            {
                array_push($activityIDs, $activity->activityID);
            }
            
            //get the statuses of all the registrations for a particular activity
            $registrationStatuses = DB::table('Registrations')
                ->whereIN('activityID', $activityIDs)
                ->where('volunteerID', '=', Auth::user()->id)
                ->get();
            
            $registrationStatus = array();
            foreach($registrationStatuses as $r)
            {
                $registrationStatus[$r->activityID] = array('allocated' => $r->allocated, 'confirmed' => $r->confirmed, 'rejected' => $r->rejected, 'declined' => $r->declined,);
            }
            
            //Get all volunteers for all the activities that have not been rejected or declined
            $volunteers = DB::table('Users')
                ->select('*','Registrations.activityID')
                ->leftjoin('Registrations','Registrations.volunteerID','=','Users.id')
                ->join('Activity','Activity.activityID','=','Registrations.activityID')
                ->leftjoin('ActivityRole','ActivityRole.activityRoleID','=','Registrations.activityRoleID')
                ->leftjoin('Role','Role.roleID','=','ActivityRole.roleID')
                ->whereIN('Registrations.activityID', $activityIDs)
                ->where('rejected','=',0)
                ->where('declined','=',0)
                ->get();
            
            
            $positionsFilled = array();
            $pending = array();
            
            //sum the positions filled and pending for each activity by looping through each registration
            foreach($volunteers as $a)
            {
                //initialise array element for the activity (activityID)      
                if(!array_key_exists($a->activityID,$positionsFilled))
                    $positionsFilled[$a->activityID] = 0;       
                if(!array_key_exists($a->activityID,$pending))
                    $pending[$a->activityID] = 0;
                
                //if confirmed status increment particular activityID value in positionsFilled array
                if($a->confirmed == 1)
                {    
                    $positionsFilled[$a->activityID] += 1;
                }
                //if not confirmed increment particular activityID value in pending array
                else                
                {  
                    $pending[$a->activityID] += 1;            
                }
            }       
            
            $records[$eventID]['event'] = $event;
            $records[$eventID]['activities'] = $activities;
            $records[$eventID]['volunteers'] = $volunteers;
            $records[$eventID]['pending'] = $pending;
            $records[$eventID]['positionsFilled'] = $positionsFilled;
        }
        else
        {     
            //Get all events management by the event manager
            $managerEvents = DB::table('Event')
                ->leftjoin('Users','Users.id','=','Event.eventManager')
                ->where('eventManager','=',$eventManagerID)
                ->get();
            
            foreach($managerEvents as $event)
            {
                
                //Get all activities for the event
                $activities = SemActivity::where('eventID', '=', $event->eventID)->get();
                
                //extract all activity IDs into an array
                $activityIDs = array();
                foreach($activities as $activity)
                {
                    array_push($activityIDs, $activity->activityID);
                }
                
                //get the statuses of all the registrations for a particular activity
                $registrationStatuses = DB::table('Registrations')
                    ->whereIN('activityID', $activityIDs)
                    ->where('volunteerID', '=', Auth::user()->id)
                    ->get();
                
                $registrationStatus = array();
                foreach($registrationStatuses as $r)
                {
                    $registrationStatus[$r->activityID] = array('allocated' => $r->allocated, 'confirmed' => $r->confirmed, 'rejected' => $r->rejected, 'declined' => $r->declined,);
                }
                
                //Get all volunteers for all the activities that have not been rejected or declined
                $volunteers = DB::table('Users')
                    ->select('*','Registrations.activityID')
                    ->leftjoin('Registrations','Registrations.volunteerID','=','Users.id')
                    ->join('Activity','Activity.activityID','=','Registrations.activityID')
                    ->leftjoin('ActivityRole','ActivityRole.activityRoleID','=','Registrations.activityRoleID')
                    ->leftjoin('Role','Role.roleID','=','ActivityRole.roleID')
                    ->whereIN('Registrations.activityID', $activityIDs)
                    ->where('rejected','=',0)
                    ->where('declined','=',0)
                    ->get();
                
                
                $positionsFilled = array();
                $pending = array();
                
                //sum the positions filled and pending for each activity by looping through each registration
                foreach($volunteers as $a)
                {
                    //initialise array element for the activity (activityID)      
                    if(!array_key_exists($a->activityID,$positionsFilled))
                        $positionsFilled[$a->activityID] = 0;       
                    if(!array_key_exists($a->activityID,$pending))
                        $pending[$a->activityID] = 0;
                    
                    //if confirmed status increment particular activityID value in positionsFilled array
                    if($a->confirmed == 1)
                    {    
                        $positionsFilled[$a->activityID] += 1;
                    }
                    //if not confirmed increment particular activityID value in pending array
                    else                
                    {  
                        $pending[$a->activityID] += 1;            
                    }
                }
                
                $records[$event->eventID]['event'] = $event;
                $records[$event->eventID]['activities'] = $activities;
                $records[$event->eventID]['volunteers'] = $volunteers;
                $records[$event->eventID]['pending'] = $pending;
                $records[$event->eventID]['positionsFilled'] = $positionsFilled;
            }
        }   
        
        $view = View::make('reports.eventManagerReport')
                    ->with('records',$records)
                    ->render();  
        
        $pdf = App::make('dompdf');
        $pdf->loadHTML($view);
        return $pdf->stream('event_report.pdf');
    }    
    
    /**
     * Show all users in storage who are not archived
     */
    public function showAllUsers()
    {
        
        $users = DB::table('Users')->where('isArchived', '!=', 1)->get();
        
        return View::make('SEM.showUsers', compact('users'));
    
    }   
    
    /**
     * Show the form for editing the specified user.
     *
     * @param  int  $id
     */    
    public function edit($id)
	{
		$user = User::find($id);
        $skills = DB::table('Skill')->get();
        $trainings = DB::table('Training')->get();
        
        $userSkillsQuery = DB::table('Userskill')->where('userID','=',$id)->get();
        
        $userSkills = array();
        foreach($userSkillsQuery as $key => $value)
        {
            $userSkills[] = $value->skillID;     
        }
        
        $userTrainingQuery = DB::table('UserTraining')->where('userID','=',$id)->get();
        
        $userTraining = array();
        foreach($userTrainingQuery as $key => $value)
        {
            $userTraining[] = $value->trainingID;     
        }
        
        return View::make('SEM.editUser')
			->with('user', $user)
            ->with('skills', $skills)
            ->with('trainings', $trainings)
            ->with('userSkills', $userSkills)
            ->with('userTraining', $userTraining);
	}
    
    /**
     * Perform user login
     */
    public function doLogin()
	{
		// validate the info, create rules for the inputs
		$rules = array(
			'email'    => 'required|email', // make sure the email is an actual email
			'password' => 'required|min:3' // password can only be alphanumeric and has to be greater than 3 characters
		);

		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
			return Redirect::to('login')
				->withErrors($validator) // send back all errors to the login form
				->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
		} else {

			// create our user data for the authentication
			$userdata = array(
				'email' 	=> Input::get('email'),
				'password' 	=> Input::get('password')
			);
            

			// attempt to do the login
			if (Auth::attempt($userdata)) {
                

				// validation successful!
				// redirect them to the secure section or whatever
				// return Redirect::to('secure');
				// for now we'll just echo success (even though echoing in a controller is bad)
                $id = Auth::id(); // Get Authenticated ID
                
                $system_access = DB::table('Users')->where('id', $id)->pluck('userType');
                return Redirect::to('home');
                //if ($system_access == 1){
                //    return Redirect::to('home');
               //}
                //else if ($system_access == 2){
                //    echo 'Event Manager Access Type';
                //}
                //else if ($system_access == 3){
                //    echo 'System Admin Access Type';
                //}
                //else{
                //    return Redirect::to('home');
                //}

			} else {	 

				// validation not successful, send back to form	
                Session::flash('LoginFailed', 'Username or Password is Incorrect');
				return Redirect::to('login');

			}

		}
	}
    
    /**
     * Perform user logout
     */
    public function doLogout()
	{
		Auth::logout(); // log the user out of our application
		return Redirect::to('login'); // redirect the user to the login screen
	}
    
    /**
     * Register User
     */
    public function doRegister()
    {
        
        if(Input::get('userType') == '5'){
            $rules = array(
                'email'    => 'required|email|unique:Users', // make sure the email is an actual email
                'userFirstName' =>'alpha|required',
                'userLastName' =>'alpha|required',
                'userPhone' => array('regex:/^(?:\()?(?:\+?61|0)[0-478]\)?(?:[ -]?[0-9]){5,}[0-9]$/','required')
            );
        }
        else if(Input::get('userType') == '2'){
            $rules = array(
                'email'    => 'required|email|unique:Users', // make sure the email is an actual email
                'userFirstName' =>'alpha|required',
                'userLastName' =>'alpha|required',
                'utsID'    => 'numeric|required|digits_between :6,6|unique:Users,utsID', // make sure the email is an actual email
                'userPhone' => array('regex:/^(?:\()?(?:\+?61|0)[0-478]\)?(?:[ -]?[0-9]){5,}[0-9]$/','required')
            );
        }
        else {
            $rules = array(
                'email'    => 'required|email|unique:Users', // make sure the email is an actual email
                'userFirstName' =>'required|alpha|required',
                'userLastName' =>'required|alpha|required',
                'utsID'    => 'numeric|required|digits_between :8,8|unique:Users,utsID',
                'userPhone' => array('regex:/^(?:\()?(?:\+?61|0)[0-478]\)?(?:[ -]?[0-9]){5,}[0-9]$/','required')
            );
        }
        
        $messages = array(
            'userPhone.regex' => 'Please enter a Valid Phone Number!'
        );
        
        
        // $messages = array(
        //    'utsID.required' => 'Your UTS ID is Required!',
        //);

		// run the validation rules on the inputs from the form $messages
		$validator = Validator::make(Input::all(), $rules, $messages);
        
        $length = 8;
        
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-=`';
        
        $count = mb_strlen($chars);

        for ($i = 0, $password = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $password .= mb_substr($chars, $index, 1);
        }


		// if the validator fails, redirect back to the form $validator->fails()
		if ($validator->fails() && Input::get('userType') == '2' ) {
			return Redirect::to('addEventManager')
				->withErrors($validator) // send back all errors to the login form
				->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
		} 
        else if ($validator->fails()) {
            return Redirect::to('addVolunteer')
				->withErrors($validator) // send back all errors to the login form
				->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
        }
        else {
            $date = new DateTime();
			// create our user data for the authentication
            
            $User = new User;
            $User->email = Input::get('email');
            $User->username = Input::get('email');
            $User->userFirstName = Input::get('userFirstName');
            $User->userLastName = Input::get('userLastName');
            $User->utsID = Input::get('utsID');
            $User->userGender = Input::get('userGender');
            $User->userPhone = Input::get('userPhone');
            $User->password = Hash::make($password);
            $User->userType = Input::get('userType');
            $User->save();
            
            $name = Input::get('userFirstName') . ' ' . Input::get('userLastName');
            $email = Input::get('email');
            
            if(isset($email) && $email != ''){
                Mail::send('emails.welcomeEmail', array('key' => $password, 'name' => $name, 'email' => $email), function($message)
                {

                    $name = Input::get('userFirstName') . ' ' . Input::get('userLastName');
                    $email = Input::get('email');
                    $message->to($email, $name)->subject('Welcome to eVol!');
                });
            }
            
            if(Input::get('userType') == '2'){
                Session::flash('message', 'You have Successfully Registered a Event Manager.');
                return Redirect::to('addEventManager');
            }
            else {
                Session::flash('message', 'You have Successfully Registered a User.');
                return Redirect::to('addVolunteer');
            }
        }
        
    }
    
    /**
     * Update specified user details
     * 
     * @param int $id
     */
    public function doUpdate($id)
	{               
        
        $user = User::find($id);
        
        $availabilityMon;
        $availabilityTue;
        $availabilityWed;
        $availabilityThu;
        $availabilityFri;
        $availabilitySat;
        $availabilitySun;
        
        $skills = Input::get('skills');
        $training = Input::get('training');
        
        $this->deleteUserSkills($user->id);
        $this->deleteUserTraining($user->id);
        
        foreach(Input::all() as $key => $value)
        {            
            if(substr($key,0,12) == 'availability')
            {
                $$key = $value;
            }
        }
        
        if(isset($skills))
        {
            foreach($skills as $key => $value)
            {
                $this->addUserSkill($user->id, $value);
            }     
        }
        
        if(isset($training))
        {
            foreach($training as $key => $value)
            {
                $this->addUserTraining($user->id, $value);
            }    
        }
        
        if(Input::get('userType') == '2' || Input::get('userType') == '1'){
            $rules = array(
                'userFirstName' =>'alpha|required',
                'userLastName' =>'alpha|required',
                'userPhone' => array('regex:/^(?:\()?(?:\+?61|0)[0-478]\)?(?:[ -]?[0-9]){5,}[0-9]$/')
                
            );
        }
        else {
            $rules = array(
                'userFirstName' =>'required|alpha',
                'userLastName' =>'required|alpha',
                'userPhone' => array('regex:/^(?:\()?(?:\+?61|0)[0-478]\)?(?:[ -]?[0-9]){5,}[0-9]$/')
            );
        }
        
        $messages = array(
            'userPhone.regex' => 'Please enter a Valid Phone Number!'
        );
        
        $validator = Validator::make(Input::all(), $rules, $messages);
        
        if ($validator->fails()) {
			return Redirect::to(BaseController::getReturnURL())
				->withErrors($validator) // send back all errors to the login form
				->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
		} 
        else {
            $userType = Input::get('userType');
            
            if(!isset($userType))
                $userType = $user->userType;      
            
            DB::table('Users')
                ->where('id', $id)
                ->update(
                    array(
                        'userFirstName' => Input::get('userFirstName'), 
                        'userLastName' => Input::get('userLastName'), 
                        'userGender' => Input::get('userGender'), 
                        'userPhone' => Input::get('userPhone'),
                        'userType' => $userType,
                        'userPhone' => Input::get('userPhone'),
                        'availabilityMon' => Input::get('availabilityMon'),
                        'availabilityTue' => Input::get('availabilityTue'),
                        'availabilityWed' => Input::get('availabilityWed'),
                        'availabilityThu' => Input::get('availabilityThu'),
                        'availabilityFri' => Input::get('availabilityFri'),
                        'availabilitySat' => Input::get('availabilitySat'),
                        'availabilitySun' => Input::get('availabilitySun'),
                        'availabilityUpdated' => DB::raw('NOW()'),
                        'updated_at' => DB::raw('NOW()')
                    ));      
            
            Session::flash('message', 'Successfully Edited the User');
            return Redirect::to('showUsers');
        }
        
	}
    
    /**
     * Archive specified user in stroage
     * 
     * @param int $id
     */
    public function destroy($id)
	{
		// delete
		$user = User::find($id);
        
        $date = date(time());
        
        //volunteering check query
        $volunteerCheckQuery = DB::table('Registrations')
                                ->join('Users','id','=','volunteerID')
                                ->join('Activity','Activity.activityID','=','Registrations.activityID')
                                ->where('volunteerID','=',$id)
                                ->where('rejected','=',0)
                                ->where('Activity.activityDate','>=',$date)
                                ->get();
        
        //if the user is not listed to volunteer against any active events 
        if(count($volunteerCheckQuery) == 0)
        {
            //if event manager, check that they're not managing any events
            if($user->userType == 2)
            {
                $eventManagerCheckQuery = DB::table('Event')
                                            ->join('Users','id','=','eventManager')
                                            ->where('id','=',$id)
                                            ->where('Event.isArchived','=',0)
                                            ->get();
                
                if(count($eventManagerCheckQuery) == 0 )
                {
                    DB::table('Users')
                        ->where('id', $id)
                        ->update(array('isArchived' => 1, 'updated_at' => DB::raw('NOW()')));
                    
                    // redirect
                    Session::flash('archived', 'User has been deleted!');
                    return Redirect::to('showUsers');
                }
                else
                {
                    // redirect
                    Session::flash('error', 'This event manager is managing an active event. Please wait for this event to pass or remove this user as the event manager before deleting');
                    return Redirect::to('showUsers');
                }
            }
            else
            {
                DB::table('Users')
                    ->where('id', $id)
                    ->update(array('isArchived' => 1, 'updated_at' => DB::raw('NOW()')));                
                
                // redirect
                Session::flash('archived', 'User has been deleted!');
                return Redirect::to('showUsers');
            }
        }
        else
        {
            // redirect
	        Session::flash('error', 'This user is listed in an active event. You must de-allocate this volunteer or wait for this event to pass.');
	        return Redirect::to('showUsers');
        }
	}
    
    /**
     * Add User Skills to storage
     * 
     * @param int $userID
     * @param int $value
     */
    private function addUserSkill($userID, $value)
    {
        $skill = new UserSkill;
        $skill->skillID = $value;
        $skill->userID = $userID;
        $skill->save();        
    }
    
    /**
     * Add User Training to storage
     * 
     * @param int $userID
     * @param int $value
     */
    private function addUserTraining($userID, $value)
    {
        $training = new UserTraining;
        $training->trainingID = $value;
        $training->userID = $userID;
        $training->save();        
    }
    
    /**
     * Remove User Skills from storage based on user
     * 
     * @param int $userID
     */
    private function deleteUserSkills($userID)
    {
        DB::table('Userskill')
            ->where('userID',$userID)
            ->delete();
    }
    
    /**
     * Remove User Training from storage based on user
     * 
     * @param int $userID
     */
    private function deleteUserTraining($userID)
    {
        DB::table('UserTraining')
            ->where('userID',$userID)
            ->delete();
    }
}