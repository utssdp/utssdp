<?php
/**
 * Handling of all reminder related functions.
 *
 * @package controllers
 */
class RemindersController extends Controller {

	/**
	 * Display the password reminder view.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('SEM.reminder');
	}

	/**
	 * Handle a POST request to remind a user of their password.
	 *
	 * @return Response
	 */
	public function store()
	{
		switch ($response = Password::remind(Input::only('email')))
		{
			case Password::INVALID_USER:
                Session::flash('message', 'Email you have entered is incorrect.');
				return Redirect::back();

			case Password::REMINDER_SENT:
				Session::flash('message', 'Your password reset has been sent to your email.');
                return Redirect::back();
		}
	}

	/**
	 * Display the password reset view for the given token.
	 *
	 * @param  string  $token
	 * @return Response
	 */
	public function show($token)
	{
		//if (is_null($token)) App::abort(404);

		return View::make('SEM.reset')->with('token', $token);
	}

	/**
	 * Handle a POST request to reset a user's password.
	 *
	 * @return Response
	 */
	public function update()
  {
    $credentials = Input::only(
      'email', 'password', 'password_confirmation', 'token'
    );

    $response = Password::reset($credentials, function($user, $password)
    {
      $user->password = Hash::make($password);

      $user->save();
    });

    switch ($response)
    {
      case Password::INVALID_PASSWORD:
      case Password::INVALID_TOKEN:
      case Password::INVALID_USER:
        return Redirect::back()->with('error', Lang::get($response));

      case Password::PASSWORD_RESET:
        return Redirect::to('/');
    }
  }

}
