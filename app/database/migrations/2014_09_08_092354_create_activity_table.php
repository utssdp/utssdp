<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Activity', function($table){
            $table->increments('activityID');
            
            $table->unsignedInteger('eventID');
            $table->foreign('eventID')
                ->references('eventID')->on('Event');
            
            $table->string('activityName', 255);
            $table->text('activityDescription');
            $table->string('activityLocation', 255)->nullable();
            $table->date('activityDate')->nullable();
            $table->time('activityStartDateTime')->nullable();
            $table->time('activityEndDateTime')->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Activity');
	}

}
