<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnsUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('Users', function($table)
        {
            $table->dropColumn('availabilityMon');
            $table->dropColumn('availabilityTue');
            $table->dropColumn('availabilityWed');
            $table->dropColumn('availabilityThu');
            $table->dropColumn('availabilityFri');
            $table->dropColumn('availabilitySat');
            $table->dropColumn('availabilitySun');
            $table->dropColumn('availabilityUpdated');
        });
        
		Schema::table('Users', function($table)
        {
            $table->integer('availabilityMon')->nullable();
            $table->integer('availabilityTue')->nullable();
            $table->integer('availabilityWed')->nullable();
            $table->integer('availabilityThu')->nullable();
            $table->integer('availabilityFri')->nullable();
            $table->integer('availabilitySat')->nullable();
            $table->integer('availabilitySun')->nullable();
            $table->date('availabilityUpdated')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{        
        Schema::table('Users', function($table)
        {
            $table->dropColumn('availabilityMon');
            $table->dropColumn('availabilityTue');
            $table->dropColumn('availabilityWed');
            $table->dropColumn('availabilityThu');
            $table->dropColumn('availabilityFri');
            $table->dropColumn('availabilitySat');
            $table->dropColumn('availabilitySun');
            $table->dropColumn('availabilityUpdated');
        });
        
		Schema::table('Users', function($table)
        {            
            $table->integer('availabilityMon');
            $table->integer('availabilityTue');
            $table->integer('availabilityWed');
            $table->integer('availabilityThu');
            $table->integer('availabilityFri');
            $table->integer('availabilitySat');
            $table->integer('availabilitySun');
            $table->date('availabilityUpdated');
        });
	}

}
