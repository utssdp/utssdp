<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSinglePrimaryKeyActivityRole extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('ActivityRole', function($table)
        {
            $table->increments('activityRoleID')->first;
        });
        
        DB::update(DB::raw('ALTER TABLE `ActivityRole` CHANGE COLUMN `activityRoleID` `activityRoleID` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT FIRST'));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ActivityRole', function($table)
        {
            $table->dropColumn('activityRoleID');
        });
	}

}
