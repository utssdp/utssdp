<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveActivityrolevolunteer extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::drop('ActivityRoleVolunteer');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::create('ActivityRoleVolunteer', function($table){
            
            $table->unsignedInteger('activityID');
            $table->foreign('activityID')
                ->references('activityID')->on('Activity');
            
            $table->unsignedInteger('roleID');
            $table->foreign('roleID')
                ->references('roleID')->on('Role');
            
            $table->unsignedInteger('volunteerID');
            $table->foreign('volunteerID')
                ->references('id')->on('Users');
            
            $table->boolean('confirmed');
            $table->boolean('selected');

            $table->timestamps();
        });
	}

}
