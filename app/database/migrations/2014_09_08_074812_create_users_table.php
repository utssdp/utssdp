<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Users', function($table){
            $table->increments('id');
            $table->integer('utsID')->nullable();
            $table->string('userFirstName', 255);
            $table->string('userLastName', 255);
            $table->string('username', 255);
            $table->string('userGender', 6)->nullable();
            $table->string('email', 255);
            $table->string('password', 255);
            $table->string('userPhone', 13);
            $table->integer('availabilityMon');
            $table->integer('availabilityTue');
            $table->integer('availabilityWed');
            $table->integer('availabilityThu');
            $table->integer('availabilityFri');
            $table->integer('availabilitySat');
            $table->integer('availabilitySun');
            $table->date('availabilityUpdated');
            $table->integer('isSPROUT');
            $table->integer('isExternal');
            $table->integer('isArchived');
            $table->integer('userType');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Users');
	}

}
