<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeActivitiestable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{        
		Schema::table('Activity', function($table)
        {
            $table->integer('positionsAvailable')->nullable()->after('activityLocation');
            $table->boolean('isPaid')->after('positionsAvailable');
            $table->boolean('isDefault')->after('updated_at');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{        
		Schema::table('Activity', function($table)
        {
            $table->dropColumn('positionsAvailable')->nullable();
            $table->dropColumn('isPaid');
            $table->dropColumn('isDefault');
        });
	}

}
