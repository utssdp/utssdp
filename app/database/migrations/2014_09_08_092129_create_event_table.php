<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Event', function($table){
            $table->increments('eventID');
            
            $table->unsignedInteger('eventManager')->nullable();
            $table->foreign('eventmanager')
                ->references('id')->on('Users');
            
            $table->string('eventName', 255);
            $table->date('eventStartDate')->nullable();
            $table->date('eventEndDate')->nullable();
            $table->text('eventDescription')->nullable();
            $table->string('eventLocation', 255)->nullable();
            $table->time('eventStartTime', 255)->nullable();
            $table->string('pickupLocation', 255)->nullable();
            $table->integer('isArchived');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Event');
	}

}
