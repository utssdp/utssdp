<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Application', function($table){
            
            $table->increments('applicationID');
            
            $table->unsignedInteger('volunteerID');
            $table->foreign('volunteerID')
                ->references('id')->on('Users');
            
            $table->unsignedInteger('activityRoleID')->nullable();
            $table->foreign('activityRoleID')
                ->references('activityRoleID')->on('ActivityRole');
            
            $table->boolean('selected');
            $table->boolean('confirmed');

            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Application');
	}

}
