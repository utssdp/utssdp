<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyEventDateToDatetime extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('Event', function($table)
        {
            $table->dropColumn('eventStartDate');
            $table->dropColumn('eventEndDate');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('Event', function($table)
        {
            $table->date('eventStartDate');
            $table->date('eventEndDate');
        });
	}

}
