<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeRegistrationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('registrations', function($table)
        {
            $table->renameColumn('applicationID', 'registrationID');
            $table->renameColumn('selected', 'allocated');
        });        
        
        Schema::table('registrations', function($table)
        {
            $table->unsignedInteger('activityID')->after('registrationID');
            $table->foreign('activityID')
                ->references('activityID')->on('Activity');      
            
            $table->string('notes', 255)->nullable()->after('activityRoleID');
            $table->boolean('declined')->after('confirmed');
              
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('registrations', function($table)
        {
            $table->renameColumn('registrationID', 'applicationID');
            $table->renameColumn('allocated', 'selected');
        });
        
        Schema::table('registrations', function($table)
        {               
            $table->dropForeign('registrations_activityid_foreign');
            
            $table->dropColumn('activityID');
            $table->dropColumn('notes');
            $table->dropColumn('declined');
        });
        
	}

}
