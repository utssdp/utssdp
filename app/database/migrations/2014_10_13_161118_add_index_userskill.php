<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexUserskill extends Migration {
    
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
        Schema::table('Userskill', function($table)
        {
            $table->increments('userSkillID');
        });
	}

	/**
     * Reverse the migrations.
     *
     * @return void
     */
	public function down()
	{
		Schema::table('Userskill', function($table)
        {
            $table->dropColumn('userSkillID');
        });
	}


}
