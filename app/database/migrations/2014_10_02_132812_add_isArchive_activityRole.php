<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsArchiveActivityRole extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		
        Schema::table('ActivityRole', function($table)
        {
            $table->integer('isArchived');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ActivityRole', function($table)
        {
            $table->dropColumn('isArchived');
        });
	}

}
