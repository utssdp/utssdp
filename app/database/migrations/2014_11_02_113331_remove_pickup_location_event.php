<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemovePickupLocationEvent extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		 Schema::table('Event', function($table)
        {
            $table->dropColumn('pickupLocation');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        
        Schema::table('Event', function($table)
        {
            $table->string('pickupLocation', 255)->nullable();
        });
	}

}
