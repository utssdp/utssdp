<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeActivityRoleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ActivityRole', function($table)
        {
            $table->dropColumn('positionsAvailable');
            $table->dropColumn('activityRolePayRate');
        });        
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ActivityRole', function($table)
        {          
            $table->integer('positionsAvailable');
            $table->decimal('activityRolePayRate', 5,2);
        });   
	}

}
