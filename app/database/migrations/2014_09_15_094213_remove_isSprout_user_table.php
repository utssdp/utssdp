<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveIsSproutUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		
        Schema::table('Users', function($table)
        {
            $table->dropColumn('isSPROUT');
            $table->dropColumn('isExternal');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('Users', function($table)
        {
            $table->integer('isSPROUT');
            $table->integer('isExternal');
        });
	}

}
