<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSkillTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('UserSkill', function($table){
            
            $table->unsignedInteger('userID');
            $table->foreign('userID')
                ->references('id')->on('Users');
            
            $table->unsignedInteger('skillID');
            $table->foreign('skillID')
                ->references('skillID')->on('Skill');
            $table->timestamps();
            
            
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('UserSkill');
	}

}
