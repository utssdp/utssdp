<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityRoleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ActivityRole', function($table){
            
            $table->unsignedInteger('activityID');
            $table->foreign('activityID')
                ->references('activityID')->on('Activity');
            
            $table->unsignedInteger('roleID');
            $table->foreign('roleID')
                ->references('roleID')->on('Role');
            
            $table->unsignedInteger('reportingRoleID');
            $table->foreign('reportingRoleID')
                ->references('id')->on('Users');
            
            
            $table->integer('positionsAvailable');
            $table->decimal('activityRolePayRate', 5,2);

            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ActivityRole');
	}

}
