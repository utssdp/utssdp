<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeApplicationTableName extends Migration {

	/**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
        Schema::rename('application', 'registrations');
    }

	/**
     * Reverse the migrations.
     *
     * @return void
     */
	public function down()
	{
        Schema::rename('registrations','application');        
	}

}
