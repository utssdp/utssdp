<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Change2RegistrationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('registrations', function($table)
        {
            $table->boolean('rejected')->after('confirmed');
            
            $table->time('availableFrom')->after('activityRoleID');
            $table->time('availableTo')->after('availableFrom');
        });   
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('registrations', function($table)
        {
            $table->dropColumn('rejected');
            
            $table->dropColumn('availableFrom');
            $table->dropColumn('availableTo');
        });   
	}

}
