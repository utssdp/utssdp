<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEventDateTime extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('Event', function($table)
        {
            $table->dateTime('eventStartDate')->nullable()->after('eventLocation');
            $table->dateTime('eventEndDate')->nullable()->after('eventStartDate');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('Event', function($table)
        {
            $table->dropColumn('eventStartDate');
            $table->dropColumn('eventEndDate');

        });
	}

}
