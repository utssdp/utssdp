<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCertificateTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Certificate', function($table){
            $table->increments('certificateID');
            
            $table->unsignedInteger('courseID');
            $table->foreign('courseID')
                ->references('courseID')->on('Course');
            
            $table->date('completionDate');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Certificate');
	}

}
