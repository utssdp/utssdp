<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTrainingRecords extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::drop('Certificate');
        Schema::drop('Course');
        
        Schema::create('Training', function($table)
        {
            $table->increments('trainingID');            
            
            $table->string('trainingName', 255);
            $table->timestamps();
        });   
        
        Schema::create('UserTraining', function($table)
        {
            $table->increments('userTrainingID');
            
            $table->unsignedInteger('trainingID');
            $table->foreign('trainingid')
                ->references('trainingid')->on('Training');
            
            $table->unsignedInteger('userID');
            $table->foreign('userid')
                ->references('id')->on('Users');
            
            $table->timestamps();
        });   
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('UserTraining');
		Schema::drop('Training');
        
        Schema::create('Course', function($table){
            $table->increments('courseID');
            $table->string('courseName', 255);
            $table->string('courseProvider', 255);
            $table->timestamps();
        });
		Schema::create('Certificate', function($table){
            $table->increments('certificateID');
            
            $table->unsignedInteger('courseID');
            $table->foreign('courseID')
                ->references('courseID')->on('Course');
            
            $table->date('completionDate');
            $table->timestamps();
        });
        
	}

}
