<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleSkillTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('RoleSkill', function($table){
            
            $table->unsignedInteger('roleID');
            $table->foreign('roleID')
                ->references('roleID')->on('Role');
            
            $table->unsignedInteger('skillID');
            $table->foreign('skillID')
                ->references('skillID')->on('Skill');
            $table->timestamps();
            
            
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('RoleSkill');
	}

}
