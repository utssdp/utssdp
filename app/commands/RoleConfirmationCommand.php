<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class RoleConfirmationCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'RoleConfirmation:CheckSend';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$date = date("Y-m-d");
        
        $date_7 = date("Y-m-d", strtotime('+7 Days'));
        

        
        $events = DB::table('event')
            ->where('eventStartDate', '<=', $date_7)
            ->where('eventStartDate', '>=', $date_7)
            ->get();
            
        
        foreach ($events as $event)
        {
            
            $activities = DB::table('activity')
                ->where('eventID', '=', $event->eventID)
                ->get();
            
            $eventStartDate = date("Y-m-d", strtotime($event->eventStartDate)); 
            
            $eventFirstName = "G.21";
            $eventLastName = "SEM";
                
              if(isset($event->userID))
               {
                   $eventManager = DB::table('Users')
                    ->where('id', '=', $event->userID)
                    ->first();

                   $eventFirstName = $eventManager->userFirstName;
                   $eventLastName = $eventManager->userLastName;
               }
            
            
            
            $diff = date_diff(date_create($eventStartDate), date_create($date));
            
            foreach ($activities as $activity){
                
                $activityRoles = DB::table('ActivityRole')
                    ->where('activityID', '=', $activity->activityID)
                    ->get();
                
                foreach($activityRoles as $activityRole){
                    
                    $applications = DB::table('Application')
                        ->where('activityRoleID', '=', $activityRole->activityRoleID)
                        ->get();
                    
                    $roles = DB::table('Role')
                        ->where('roleID', '=', $activityRole->roleID)
                        ->first();
                    
                    $roleName = $roles->roleName;
                    
                    foreach($applications as $application){
                        $users = DB::table('Users')
                        ->where('id', '=', $application->volunteerID)
                        ->get();
                        
                        foreach($users as $user){
                            
                            
                            $data = array('userFirstName' => $user->userFirstName, 'email' => $user->email, 'userLastName' => $user->userLastName, 'roleName' => $roleName, 'dateStart' => $diff, 'eventFirstName' => $eventFirstName, 'eventLastName' => $eventLastName);
                            
                            Mail::send('emails.roleReminder', $data, function($message) use($data)
                            {
                                $userFirstName = $data['userFirstName'];
                                $userLastName = $data['userLastName'];
                                $email = $data['email'];
                                $name = $userFirstName . " " . $userLastName;
                                $message->to($email, $name)->subject('SEM Event - Role Reminder');
                            });
                        }

                                            
                    }
                }
            }
            
        }
        
	}
	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	//protected function getArguments()
	//{
		//return array(
		//	array('example', InputArgument::REQUIRED, 'An example argument.'),
		//);
	//}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	//protected function getOptions()
	//{
		//return array(
		//	array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		//);
	//}

}
