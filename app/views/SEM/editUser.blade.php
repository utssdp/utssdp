@include('SEM.header')
@include('SEM.header-nav')

<!--Need to be moved? -->
<style>
    .table td,
    .table th {
        text-align: center;   
    }

</style>

<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <ol class="breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li>Account</li>
                <li class="active">Edit</li>
            </ol>

            <h1>Edit Account</h1>
        </div>


        <div class="container">


            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4>{{ $user->userFirstName, ' ', $user->userLastName }}</h4>
                        </div>
                        <div class="panel-body collapse in">
                            @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                            @else  
                     {{ Form::open(array('url' => 'users/'. $user->id . '/edit', 'class' => 'form-horizontal', 'style' => 'margin-bottom:0px !important', 'method' => 'PUT')) }}
                        <div class="form-group">
                            <label class="col-sm-3 control-label">UTS ID</label>
                            <div class="col-sm-6 control-label" style="text-align: left;">
                                {{ $user->utsID }}
                            </div>
                        </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">First Name</label>
                                <div class="col-sm-6">
                                    {{ Form::text('userFirstName', $user->userFirstName, array('class' => 'form-control', 'placeholder' => 'First Name')) }}
                                </div>
                                {{ $errors->first('userFirstName') }}
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Last Name</label>
                                <div class="col-sm-6">
                                    {{ Form::text('userLastName', $user->userLastName, array('class' => 'form-control', 'placeholder' => 'Last Name')) }}
                                </div>
                                {{ $errors->first('userLastName') }}
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Gender</label>
                                <div class="col-sm-6">
                                    {{ Form::select('userGender', array('Male' => 'Male', 'Female' => 'Female'), $user->userGender, array('class' => 'form-control')) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-6 control-label" style="text-align: left;">
                                    {{ $user->email }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Phone Number</label>
                                <div class="col-sm-6">
                                    {{ Form::text('userPhone', $user->userPhone, array('class' => 'form-control', 'placeholder' => 'Enter Phone Number (XX) XXXX XXXX or +61XXX XXX XXX')) }}
                                </div>
                                {{ $errors->first('userPhone') }}
                            </div>

                            @if($user->userType <= 2)
                            <div class="form-group">
                                <label class="col-sm-3 control-label">User Type</label>
                                <div class="col-sm-6">
                                    {{ Form::select('userType', array('1' => 'System Administrator', '2' => 'Event Manager'), $user->userType, array('class' => 'form-control')) }}
                                </div>
                            </div>
                            @endif

                            @if(Auth::user()->userType == 1 || Auth::user()->id == Request::segment(2))
                            <hr />

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Availabilities &nbsp @if($GLOBALS['availabilityBadge'])<a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title="Toggle Infobar"><span class="badge badge-warning badge-sub"><i class="fa fa-warning"></i></span></a>@endif</label>
                                <div class="col-sm-4">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Monday</th>
                                                    <th>Tuesday</th>
                                                    <th>Wednesday</th>
                                                    <th>Thursday</th>
                                                    <th>Friday</th>
                                                    <th>Saturday</th>
                                                    <th>Sunday</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>{{ Form::checkbox('availabilityMon', 1, $user->availabilityMon, array('class' => 'form-control')) }}</td>
                                                    <td>{{ Form::checkbox('availabilityTue', 1, $user->availabilityTue, array('class' => 'form-control')) }}</td>
                                                    <td>{{ Form::checkbox('availabilityWed', 1, $user->availabilityWed, array('class' => 'form-control')) }}</td>
                                                    <td>{{ Form::checkbox('availabilityThu', 1, $user->availabilityThu, array('class' => 'form-control')) }}</td>
                                                    <td>{{ Form::checkbox('availabilityFri', 1, $user->availabilityFri, array('class' => 'form-control')) }}</td>
                                                    <td>{{ Form::checkbox('availabilitySat', 1, $user->availabilitySat, array('class' => 'form-control')) }}</td>
                                                    <td>{{ Form::checkbox('availabilitySun', 1, $user->availabilitySun, array('class' => 'form-control')) }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Skills &nbsp @if($GLOBALS['skillsBadge'])<a href="#" data-toggle="tooltip" title="Default tooltip"><span class="badge badge-warning badge-sub"><i class="fa fa-warning"></i></span></a>@endif</label>
                                <div class="col-sm-3 table-responsive">
                                    <select id="skills" name="skills[]" multiple style="width:100%" class="populate">
                                        @foreach($skills as $skill)
                                            <option value="{{$skill->skillID}}" {{ (in_array($skill->skillID, $userSkills)) ? "selected='selected'" : '' }}>{{$skill->skillName}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Training</label>
                                <div class="col-sm-6 table-responsive">
                                    <select multiple="multiple" id="training" name="training[]">
                                        @foreach($trainings as $training)
                                            <option value="{{$training->trainingID}}" {{ (in_array($training->trainingID, $userTraining)) ? "selected='selected'" : '' }}>{{$training->trainingName}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @endif
                            
                            <div class="panel-footer">
                                <div class="row">
                                    <div class="col-sm-6 col-sm-offset-3">
                                        <div class="btn-toolbar">
                                            {{ Form::submit('Update Account', array('class'=>'btn btn-primary')) }}
                                                {{ Form::close() }}
                                            @endif  
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel-footer">
                                <div class="row">
                                    <div class="col-sm-6 col-sm-offset-3">
                                        <div class="btn-toolbar">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>



</div>
<!-- container -->
</div>
<!--wrap -->
</div>
<!-- page-content -->

@include('SEM.footer')