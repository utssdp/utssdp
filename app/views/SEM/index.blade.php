@include('SEM.header')
@include('SEM.header-nav')

<style>
    .top-pad{
        padding-top: 25px;
    }
    
    .bottom-pad{
        padding-bottom: 15px;
    }


</style>

<div id="page-content">
    <div id='wrap'>
        <div class="container">
            <div class="panel panel-default" style="max-width: 900px; margin: 0 auto">
                <div class="panel-body">
                    <div class="clearfix">
                        <div class="jumbotron pull-left" style="background-color: #fff; padding: 0 0 0 0; margin: 0">
                            <h1>Welcome to 
                            <img src="{{ URL::asset('assets/img/Logo.svg'); }}" style="width:224px;height:77px" /></h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
							<h3 style="background: #00a0dc; padding: 5px 10px; color: #fff; border-radius: 1px; margin: 20px 0 20px; text-align:center">Event Management made simple</h3>
						</div>
                    </div>
                    <img src="https://www.uts.edu.au/sites/default/files/styles/video-postcard/public/future%20students%20UTS%20Info%20Day%202013_Anshuman%20Bose010.JPG?itok=yOc8y5aW" width="100%"/>
                    <hr />
                    <div class="row">
                        <div class="col-md-4" style="">
                            <div class="top-pad bottom-pad" style="text-align: center; background-color: #85c744; color: #fff">
                                <i class="fa fa-users fa-3x" ></i>  
                                <h3 style="color: #fff">Find Volunteers</h3>
                            </div>
                            <div style="background-color: #f6f6f6; font-size: 15px; height: 280px" class="top-pad bottom-pad">
                                <ul>
                                    <li>Let Volunteers find your Event!</li>
                                    <br />
                                    <li>Advertise to Unpaid, SPROUT and External Volunteers</li>
                                    <br />
                                    <li>Advanced skill filtering technology</li>
                                    <br />
                                    <li>Email Reminder System</li>
                                    <br />
                                    <li>Add roles for even more control!</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4" style="">
                            <div class="top-pad bottom-pad" style="text-align: center; background-color: #8d6cab; color: #fff">
                                <i class="fa fa-calendar fa-3x" ></i>  
                                <h3 style="color: #fff">Find Events</h3>
                            </div>
                            <div style="background-color: #f6f6f6; font-size: 15px; height: 280px" class="top-pad bottom-pad">
                                <ul>
                                    <li>View all events at your fingertips!</li>
                                    <br />
                                    <li>Gain hands on experience for your CV</li>
                                    <br />
                                    <li>Meet new people</li>
                                    <br />
                                    <li>Filters for finding events tailored for you</li>
                                    <br />
                                    <li>Available anywhere at anytime.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4" style="">
                            <div class="top-pad bottom-pad" style="text-align: center; background-color: #e05959; color: #fff">
                                <i class="fa fa-line-chart fa-3x" ></i>  
                                <h3 style="color: #fff">Report</h3>
                            </div>
                            <div style="background-color: #f6f6f6; font-size: 15px; height: 280px" class="top-pad bottom-pad">
                                <ul>
                                    <li>Generate Volunteer experience reports</li>
                                    <br />
                                    <li>Generate Volunteer history reports</li>
                                    <br />
                                    <li>Generate Event Manager event reports</li>
                                    <br />
                                </ul>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="row top-pad">
                        <div class="bottom-pad" style="text-align: center;">
                            <h1>Get started <span style="color: #e69017">Connecting Volunteers to Events!</span></h1>
                        </div>
                        <div class="row">
                            <div class="col-sm-5">
                                <h3 style="color: #00a0dc">Event Profiles</h3>
                                <em>Easiest way to see events at a glance</em>
                                <br /><br />
                                <p style="text-align: justify">See all the details of an event that matter. Quickly view the status of your activities and see how many positions are left in each activity. See the roles associated with each activity.</p>
                            </div>
                            <div class="col-sm-7">
                                 <img src="{{ URL::asset('assets/img/screencap1.PNG'); }}" width="100%" />
                            </div>
                        </div>
                        <div class="row  top-pad">
                            <div class="col-sm-7">
                                 <img src="{{ URL::asset('assets/img/screencap1.PNG'); }}" width="100%" />
                            </div>
                            <div class="col-sm-5">
                                <h3 style="color: #00a0dc">Volunteer Profiles</h3>
                                <em>Easiest way to see volunteers details</em>
                                <br /><br />
                                <p style="text-align: justify">See all the details of an event that matter. Apply for activities which have roles suited to your skill set. You can view the status of your application by looking at the status column in an activity you have applied for.</p>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="row top-pad">
                        <div class="bottom-pad" style="text-align: center;">
                            <h1>Frequently Asked Questions (FAQs)</h1>
                        </div>
                        <div class="row">
                            <div class="col-sm-5 col-sm-offset-1">
                                <h3>How long does it take to be notified of the result of your registration/application? </h3>
                                <p style="text-align: justify">This is dependent on the Event Manager themselves. But as a rule of thumb, we aim to respond within a week of lodging the registration/application.</p>
                            </div>
                            <div class="col-sm-5">
                                <h3>I'm unable to open the user manual, what should I do?</h3>
                                <p style="text-align: justify"> The most likely case here is that you do not have the required program to open the file. Click <a href="http://get.adobe.com/reader/" target="_blank">here</a> to download the latest version of Adobe Reader which is the program that will open the PDF file you downloaded.</p>
                            </div>
                        </div>
                        <div class="row top-pad">
                            <div class="col-sm-5 col-sm-offset-1">
                                <h3>How can I show my experience from Volunteering?</h3>
                                <p style="text-align: justify">You can request a Volunteer Experience Report which is a official document provided by eVol that highlights the events that you have participated in. This also includes the number of hours so that you can quantify your experience.</p>
                            </div>
                            <div class="col-sm-5">
                                <h3>How do I use eVol?</h3>
                                <p style="text-align: justify">There is a User Manual guide located at the "Help and Documentation" tab on the navigation bar. You will need a pdf reader in order to open the file. The User Manual will give a step by step guide on how to use eVol with screenshots. Also located in the "Help and Documentation" tab is a link to our <a href="https://www.youtube.com/channel/UCd0uFR0uD0LBMLexXgxmmOw" target="_blank">YouTube channel</a>. Here you can watch tutorial videos on using eVol.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--wrap -->
</div>
<!-- page-content -->

@include('SEM.footer')