<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>eVol - UTS Events</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="EVOL - Small Event Marketing">
    <meta name="author" content="G21">


    <!-- CSS & SCRIPTS -->

    <link rel="stylesheet" href="<?PHP echo URL::asset('assets/css/styles.min.css?=113'); ?>">
    <link rel="icon" type="image/png" href='<?PHP echo URL::asset('assets/img/favicon.png'); ?>'>
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600' rel='stylesheet' type='text/css'>
    <link rel='stylesheet' type='text/css' href='<?PHP echo URL::asset('assets/plugins/codeprettifier/prettify.css'); ?>' />
    <link rel='stylesheet' type='text/css' href='<?PHP echo URL::asset('assets/plugins/form-toggle/toggles.css'); ?>' />
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <link href='<?PHP echo URL::asset('assets/demo/variations/default.css'); ?>' rel='stylesheet' type='text/css' media='all' id='styleswitcher'>
    <link href='<?PHP echo URL::asset('assets/demo/variations/default.css'); ?>' rel='stylesheet' type='text/css' media='all' id='headerswitcher'>

    <link href="<?PHP echo URL::asset('assets/less/styles.less'); ?>" rel="stylesheet/less" media="all">

    <link rel="stylesheet" href="<?PHP echo URL::asset('assets/css/styles.css?=121'); ?>">

    <style>
        .badge-sub {
            top: 7px !important;
        }

            .badge-sub .fa-warning {
                margin: 0 auto !important;
            }

        .navbar-inverse .nav > .open > a,
        .navbar-inverse .nav > .open > a:hover,
        .navbar-inverse .nav > .open > a:focus {
            background-color: #27292d;
            color: #ffffff;
        }
    </style>

    <script type="text/javascript" src="<?PHP echo URL::asset('assets/js/less.js'); ?>"></script>
    <!-- CSS & SCRIPTS -->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries. Placeholdr.js enables the placeholder attribute -->
    <!--[if lt IE 9]>
        <link rel="stylesheet" href="assets/css/ie8.css">
		<script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>
        <script type="text/javascript" src="assets/plugins/charts-flot/excanvas.min.js"></script>
	<![endif]-->


    <link rel='stylesheet' type='text/css' href='<?PHP echo URL::asset('assets/plugins/datatables/dataTables.css'); ?>' />
    <link rel='stylesheet' type='text/css' href='<?PHP echo URL::asset('assets/plugins/form-daterangepicker/daterangepicker-bs3.css'); ?>' />

    <link rel='stylesheet' type='text/css' href='<?PHP echo URL::asset('assets/plugins/jqueryui-timepicker/jquery.ui.timepicker.css'); ?>' />
    <link rel='stylesheet' type='text/css' href='<?PHP echo URL::asset('assets/js/jqueryui.css'); ?>' />

    <link rel='stylesheet' type='text/css' href='<?PHP echo URL::asset('assets/plugins/form-select2/select2.css'); ?>' />
    <link rel='stylesheet' type='text/css' href='<?PHP echo URL::asset('assets/plugins/form-multiselect/css/multi-select.css'); ?>' />
    <link rel='stylesheet' type='text/css' href='<?PHP echo URL::asset('assets/plugins/form-tokenfield/bootstrap-tokenfield.css'); ?>' />

</head>