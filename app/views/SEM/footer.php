    <footer role="contentinfo">
        <div class="clearfix">
            <ul class="list-unstyled list-inline pull-left">
                <li>eVol - G21 &copy; 2014</li>-
                <li><a href="<?PHP echo URL::to('http://evoluts.freshdesk.com') ?>" target="_blank">User Help</a></li>-
                <li><a href="<?PHP echo URL::to('http://evoluts.freshdesk.com/support/tickets/new') ?>" target="_blank">Submit a Support Request</a></li>
            </ul>
            <button class="pull-right btn btn-inverse-alt btn-xs hidden-print" id="back-to-top"><i class="fa fa-arrow-up"></i></button>
        </div>
    </footer>

</div> <!-- page-container -->

<!--
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<script>!window.jQuery && document.write(unescape('%3Cscript src="assets/js/jquery-1.10.2.min.js"%3E%3C/script%3E'))</script>
<script type="text/javascript">!window.jQuery.ui && document.write(unescape('%3Cscript src="assets/js/jqueryui-1.10.3.min.js'))</script>
-->

<script type='text/javascript' src='<?PHP echo URL::asset('assets/js/jquery-1.10.2.min.js'); ?>'></script> 
<script type='text/javascript' src='<?PHP echo URL::asset('assets/js/jqueryui-1.10.3.min.js'); ?>'></script> 
<script type='text/javascript' src='<?PHP echo URL::asset('assets/js/bootstrap.min.js'); ?>'></script> 
<script type='text/javascript' src='<?PHP echo URL::asset('assets/js/enquire.js'); ?>'></script> 
<script type='text/javascript' src='<?PHP echo URL::asset('assets/js/jquery.cookie.js'); ?>'></script> 
<script type='text/javascript' src='<?PHP echo URL::asset('assets/js/jquery.nicescroll.min.js'); ?>'></script> 
<script type='text/javascript' src='<?PHP echo URL::asset('assets/plugins/codeprettifier/prettify.js'); ?>'></script> 
<script type='text/javascript' src='<?PHP echo URL::asset('assets/plugins/easypiechart/jquery.easypiechart.min.js'); ?>'></script> 
<script type='text/javascript' src='<?PHP echo URL::asset('assets/plugins/sparklines/jquery.sparklines.min.js'); ?>'></script> 
<script type='text/javascript' src='<?PHP echo URL::asset('assets/plugins/form-toggle/toggle.min.js'); ?>'></script>
<script type='text/javascript' src='<?PHP echo URL::asset('assets/plugins/datatables/jquery.dataTables.min.js'); ?>'></script> 
<script type='text/javascript' src='<?PHP echo URL::asset('assets/plugins/datatables/dataTables.bootstrap.js'); ?>'></script> 
<script type='text/javascript' src='<?PHP echo URL::asset('assets/js/placeholdr.js'); ?>'></script> 
<script type='text/javascript' src='<?PHP echo URL::asset('assets/js/application.js'); ?>'></script> 

<script type='text/javascript' src='<?PHP echo URL::asset('assets/plugins/form-validation/jquery.validate.min.js'); ?>'></script> 
<script type='text/javascript' src='<?PHP echo URL::asset('assets/plugins/form-stepy/jquery.stepy.js'); ?>'></script> 
<script type='text/javascript' src='<?PHP echo URL::asset('assets/js/SEM-formWizard.js'); ?>'></script> 

<script type='text/javascript' src='<?PHP echo URL::asset('assets/plugins/form-daterangepicker/daterangepicker.min.js'); ?>'></script> 
<script type='text/javascript' src='<?PHP echo URL::asset('assets/plugins/form-datepicker/js/bootstrap-datepicker.js'); ?>'></script> 
<script type='text/javascript' src='<?PHP echo URL::asset('assets/plugins/form-daterangepicker/moment.min.js'); ?>'></script> 
<script type='text/javascript' src='<?PHP echo URL::asset('assets/plugins/jqueryui-timepicker/jquery.ui.timepicker.min.js'); ?>'></script> 

<script type='text/javascript' src='<?PHP echo URL::asset('assets/plugins/form-multiselect/js/jquery.multi-select.min.js'); ?>'></script> 
<script type='text/javascript' src='<?PHP echo URL::asset('assets/plugins/quicksearch/jquery.quicksearch.min.js'); ?>'></script> 
<script type='text/javascript' src='<?PHP echo URL::asset('assets/plugins/form-typeahead/typeahead.min.js'); ?>'></script> 
<script type='text/javascript' src='<?PHP echo URL::asset('assets/plugins/form-select2/select2.min.js'); ?>'></script> 
<script type='text/javascript' src='<?PHP echo URL::asset('assets/plugins/form-autosize/jquery.autosize-min.js'); ?>'></script>
<script type='text/javascript' src='<?PHP echo URL::asset('assets/plugins/bootbox/bootbox.min.js'); ?>'></script>
<script type='text/javascript' src='<?PHP echo URL::asset('assets/plugins/form-tokenfield/bootstrap-tokenfield.min.js'); ?>'></script>



<script>
    $(document).ready(function () {
        
        $('.datatables').dataTable({
            "sDom": "<'row'<'col-xs-6'l><'col-xs-6'f>r>t<'row'<'col-xs-6'i><'col-xs-6'p>>",
            "sPaginationType": "bootstrap",
            "oLanguage": {
                "sLengthMenu": "_MENU_ records per page",
                "sSearch": "",
            }
        });
        $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search...');
        $('.dataTables_length select').addClass('form-control');

        $('#eventStartEndDate').daterangepicker({ timePicker: false, format: 'DD/MM/YYYY', minDate: '<?PHP echo date("d/m/Y")?>' });

        //select2
        $("#event_manager, #role_activity_name, #role_reports_to").select2({ width: 'resolve' });
        $('#activity_description, #role_description').autosize();
        
        
        
        $('#activity_date').datepicker(
            {   
                startDate: '<?PHP if(isset($eventDate)) { echo $eventDate;} else{echo "today";};?>'
            }
        );
        
        $('#activity_start_time,#activity_end_time').timepicker({ minutes: { interval: 15 } });

        $('#availableFrom,#availableTo,#event_start_time').timepicker({ minutes: { interval: 15 } });
        
        $('.tooltips').tooltip(); //bootstrap's tooltip

        $("#skills").select2({ width: 'resolve' });

        $('#training').multiSelect({
            selectableHeader: "<div class='panel-heading' style='font-size: 14px'>Recognised Training</div>",
            selectionHeader: "<div class='panel-heading' style='font-size: 14px'>Your Training</div>",
        });


        $('#addTraining').tokenfield({
            autocomplete: {
                delay: 100
            },
            showAutocompleteOnFocus: true
        });


    });
    
    $(window).load(function(){
        $('#myModal').modal('show');
    });
    
</script>


</body>
</html>