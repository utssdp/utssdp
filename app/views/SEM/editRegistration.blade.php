@include('SEM.header')
@include('SEM.header-nav')

<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <ol class="breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li>Volunteer</li>
                <li>Registration</li>
                <li class="active">Edit</li>
            </ol>

            <h1>Edit Registration</h1>
            
        </div>
        <div class="container">


<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h4>Registration for {{ $registration->activityName }} at {{ $registration->eventName }} </h4>
                
            </div>
            <div class="panel-body">
                {{ Form::open(array('action' => 'RegistrationController@updateRegistration', 'method' => 'POST', 'id' => 'createRoles', 'class' => 'form-horizontal', 'style' => 'margin-bottom:0px !important')) }}    
                    {{ Form::hidden('registrationID', $registration->registrationID) }}
                
                    <legend>Edit Registration </legend>
                                        
                    <div class="form-group">
                        <label class="col-md-3 text-right"><b>Event Name</b></label>
                        <label class="col-md-3">{{ $registration->eventName }}</label>
                    </div>
                                        
                    <div class="form-group">
                        <label class="col-md-3 text-right"><b>Activity Name</b></label>
                        <label  class="col-md-3">{{ $registration->activityName }}</label>
                    </div>
                                        
                    <div class="form-group">
                        <label class="col-md-3 text-right"><b>Activity Date</b></label>
                        <label  class="col-md-3">{{ date('d/m/Y', strtotime($registration->activityDate)) }}</label>
                    </div>
                                        
                    <div class="form-group">
                        <label class="col-md-3 text-right"><b>Activity Time</b></label>
                        <label  class="col-md-3">{{ date('h:i A', strtotime($registration->activityStartDateTime)) . ' - ' . date('h:i A', strtotime($registration->activityEndDateTime)) }} </label>
                    </div>

                    <hr />

                    <div class="form-group">
                        <label class="col-sm-3 control-label">What time are you available from?</label>
                        <div class="col-sm-6">
                            {{ Form::text('availableFrom', date('H:i', strtotime($registration->availableFrom)), array('class' => 'form-control', 'name' => 'availableFrom', 'id' => 'availableFrom', 'required' => 'required', 'type' => 'text')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">What time are you available to?</label>
                        <div class="col-sm-6">
                            {{ Form::text('availableTo', date('H:i', strtotime($registration->availableTo)), array('class' => 'form-control', 'name' => 'availableTo', 'id' => 'availableTo', 'required' => 'required', 'type' => 'text')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="role_description" class="col-md-3 control-label">Notes</label>
                        <div class="col-md-6">                                
                            {{ Form::textarea('notes', $registration->notes, array('id' => 'notes', 'class' => 'form-control autosize', 'name' => 'notes', 'required' => 'required', 'rows' => '2')) }}
                        </div>
                    </div>

            </div>
                
            <div class="panel-footer">
                <div class="pull-right">
                    <a class="btn btn-small btn-info" href="{{ $_SERVER['HTTP_REFERER'] }}">Cancel</a> &nbsp;
                    {{ Form::submit('Update', array('class'=>'finish btn-success btn pull-right')) }}
                    {{ Form::close() }}
                </div>
            </div>

            </form>
        </div>


    </div>
</div>


</div> <!-- container -->
</div> <!--wrap -->
</div> <!-- page-content -->

@include('SEM.footer')