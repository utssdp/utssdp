@include('SEM.header')
@include('SEM.header-nav')

<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <ol class="breadcrumb">
                <li class="active"><a href="index.php">Home</a></li>
                <li class="active">Help</li>
            </ol>

            <h1>Help Centre</h1>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3">
							<a href="<?PHP if(Auth::user()->userType() <= 2) { echo URL::asset('assets/Manual/User_Manual_Mgr_Admin.pdf'); } else { echo URL::asset('assets/Manual/User_Manual_Volunteer.pdf');  } ?>" class="shortcut-tiles tiles-info">
								<div class="tiles-body" style="text-align: center; padding: 20px 0; height: 200px">
									<i class="fa fa-file" style="font-size: 2.7em; margin: 25px"></i>
								</div>
								<div class="tiles-footer" style="font-size: 20px; text-align: center;">
									User Manual
								</div>
							</a>
						</div>
                        <div class="col-md-3">
                            <a href="https://www.youtube.com/channel/UCd0uFR0uD0LBMLexXgxmmOw" target="_blank" class="shortcut-tiles tiles-brown">
								<div class="tiles-body" style="text-align: center; padding: 20px 0; height: 200px">
									<i class="fa fa-play" style="font-size: 2.7em; margin: 25px"></i>
								</div>
								<div class="tiles-footer" style="font-size: 20px; text-align: center;">
									Video Guides
								</div>
							</a>
						</div>
                        <div class="col-md-3">
                            <a href="http://evoluts.freshdesk.com" target="_blank" class="shortcut-tiles tiles-orange">
								<div class="tiles-body" style="text-align: center; padding: 20px 0; height: 200px">
									<i class="fa fa-question-circle" style="font-size: 2.7em; margin: 25px"></i>
								</div>
								<div class="tiles-footer" style="font-size: 20px; text-align: center;">
									Knowledge Base
								</div>
							</a>
						</div>
                        <div class="col-md-3">
                            <a href="http://evoluts.freshdesk.com/support/tickets/new" target="_blank" class="shortcut-tiles tiles-sky">
								<div class="tiles-body" style="text-align: center; padding: 20px 0; height: 200px">
									<i class="fa fa-phone" style="font-size: 2.7em; margin: 25px"></i>
								</div>
								<div class="tiles-footer" style="font-size: 20px; text-align: center;">
									Submit a Support Request
								</div>
							</a>
						</div>
                    </div>
                </div>
            </div>
        </div>
        <!-- container -->
    </div>
    <!--wrap -->
</div>
<!-- page-content -->

@include('SEM.footer')