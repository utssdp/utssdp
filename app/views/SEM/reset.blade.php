@include('SEM.header-focused')

<div class="verticalcenter">
    
    <a href="#" class="brand"><h4>Small Event Management</h4></a>
    
    
    <div class="panel panel-primary">
        
        <div class="panel-body">
            
            <h4 class="text-center" style="margin-bottom: 25px;">Reset Your Password</a></h4>

            {{ Form::open(array('class' => 'form-horizontal', 'style' => 'margin-bottom:0px !important')) }}
                
                <!-- if there are login errors, show them here -->
               @if (Session::has('error'))
                  {{ trans(Session::get('reason')) }}
               @endif
            
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            {{ Form::text('email', Input::old('email'), array('placeholder' => 'awesome@awesome.com', 'class' => 'form-control')) }}
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) }}
                            
                        </div>
                    </div>
                </div>
        {{ Form::hidden('token', $token) }}
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            {{ Form::password('password_confirmation', array('class' => 'form-control', 'placeholder' => 'Confirm Your Password')) }}
                        </div>
                    </div>
                </div>

                

               
            

        </div>
        
        <div class="panel-footer">

            <div class="pull-right">
            {{ Form::submit('Reset!', array('class'=>'btn btn-primary')) }}
            {{ Form::close() }}    
            </div>
        </div>
    </div>
</div>
</body>
</html>