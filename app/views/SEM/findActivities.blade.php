@include('SEM.header')
@include('SEM.header-nav')

<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <ol class="breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li>Activities</li>
                <li class='active'>Find</li>
            </ol>

            <h1>Find Activities</h1>
            
            <?PHP $segment = Request::segment(2);  ?>
            
        </div>
        <div class="container">
            <div class="row">
              <div class="col-md-12">
                    <div class="panel panel-sky">
                        <div class="panel-heading">
                            <h4>Activities</h4>
                            <div class="pull-right"><a class="btn btn-small btn-success" href="{{ URL::to('addActivity/' . $segment) }}">Add Activity</a></div>
                        </div>
                        <div class="panel-body collapse in">
                            @if (Session::has('message'))
                            <div class="alert alert-success">{{ Session::get('message') }}</div>
                            @endif
                            @if (Session::has('archived'))
                            <div class="alert alert-danger">{{ Session::get('archived') }}</div>
                            @endif
                            
                            <div class="table-responsive">
                                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables" id="example">
                                    <thead>
                                        <tr>
                                            <th>Activity Name</th>
                                            <th>Activity Description</th>
                                            <th>Activity Date</th>
                                            <th>Activity Start Time</th>
                                            <th>Activity End Time </th>
                                            <th>Modify</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                          @foreach ($activities as $activity)
                                        <tr>
                                            <td>{{ $activity->activityName }}</td>
                                            <td>{{ $activity->activityDescription }}</td>
                                            <td>{{ $activity->activityDate }}</td>
                                            <td>{{ $activity->activityStartDateTime }}</td>
                                            <td>{{ $activity->activityEndDateTime }}</td>

                                            <td width="20%">
                                            {{ Form::open(array('url' => 'activity/' . $activity->activityID . '/' . $segment) ) }}
                                                {{ Form::hidden('_method', 'DELETE') }}
                                                <a class="btn btn-small btn-info" href="{{ URL::to('activity/' . $activity->activityID . '/showRoles') }}">Open Roles</a>
                                                <a class="btn btn-small btn-warning" href="{{ URL::to('activity/edit/' . $activity->activityID) }}">Edit Activity</a>
                                                {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                                            {{ Form::close() }}    
                                        
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div> <!-- container -->
    </div> <!--wrap -->
</div> <!-- page-content -->

@include('SEM.footer')