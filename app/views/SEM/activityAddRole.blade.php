@include('SEM.header')
@include('SEM.header-nav')

<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <ol class="breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li>Event</li>
                <li class="active">Create</li>
            </ol>

            <h1>Create An Event</h1>
            
        </div>
        <div class="container">


<div class="row">
    <div class="col-md-12">
<?PHP 

$segment = Request::segment(3); 
        
if(isset($_GET['activityID'])){
    $activityIDURL = $_GET['activityID'];   
}
        ?>
        <div class="panel panel-info">
            <div class="panel-heading">
                <h4>Create An Event</h4>
                
            </div>
            <div class="panel-body">
                @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @else
                {{ Form::open(array('url' => 'role/create', 'method' => 'GET', 'id' => 'createRoles', 'class' => 'form-horizontal', 'style' => 'margin-bottom:0px !important')) }}    

                    <fieldset title="Final Step">
                        <legend>Create Roles</legend>
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Activity Name</label>
                            <div class="col-sm-6">
                                {{ Form::select('role_activity_name', $activity, $activity, array('onChange' => 'onClick()', 'style' => 'width:100%', 'class' => 'populate', 'id' => 'role_activity_name')) }}
                            </div>
                        </div>
                        
                        <!--
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Reports To</label>
                            <div class="col-sm-6">
                                <?PHP
                                if(isset($activityIDURL)){
                                    
                                    $roles =  DB::table('ActivityRole')            
                                        ->join('Role', 'ActivityRole.RoleID', '=', 'Role.RoleID')
                                        ->select('*')
                                        ->where('ActivityRole.activityID', '=', $activityIDURL)                                        
                                        ->get();
                                    
                                    $activityRoles = array();
                                    foreach($roles as $key => $act)
                                    {
                                      $activityRoles[$act->roleID] = $act->roleName;
                                    }
                                    
                                    echo Form::select('role_reports_to', $activityRoles, null, array('style' => 'width:100%', 'class' => 'populate', 'id' => 'role_reports_to'));
                                    
                                }
                                else{
                                echo Form::select('role_reports_to', $activityRoles, null, array('style' => 'width:100%', 'class' => 'populate', 'id' => 'role_reports_to'));
                                }
                                ?>
                            </div>
                        </div>-->
                        
                        <div class="form-group">
                            <label for="role_name" class="col-md-3 control-label">Role Name</label>
                            <div class="col-md-6">    
                                {{ Form::text('role_name', '', array('class' => 'form-control', 'name' => 'role_name', 'placeholder' => 'Role Name', 'required' => 'required', 'minlength' => '5', 'type' => 'text')) }}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="role_name" class="col-md-3 control-label">Number of Positions</label>
                            <div class="col-md-6">    
                                {{ Form::text('role_number_positions', '', array('class' => 'form-control', 'name' => 'role_number_positions', 'placeholder' => '# of Positions', 'required' => 'required', 'type' => 'number')) }}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Pay Rate</label>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    {{ Form::text('role_pay_rate', '', array('class' => 'form-control', 'name' => 'role_pay_rate', 'placeholder' => '25.00', 'required' => 'required', 'type' => 'number')) }}                                   <input type="hidden"  value="<?PHP echo $segment?>" id="URL_Segment"/>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="role_description" class="col-md-3 control-label">Role Description</label>
                            <div class="col-md-6">                                
                                 {{ Form::textarea('role_description', '', array('id' => 'role_description', 'class' => 'form-control autosize', 'name' => 'role_description', 'required' => 'required', 'rows' => '2')) }}
                            </div>
                        </div>
                        
                    </fieldset>
                        <div class="col-md-3">  
                    
                        </div>
                <div class="col-md-6">  
                    {{ Form::submit('Create', array('class'=>'finish btn-success btn pull-right')) }}
                        </div>
                
                    {{ Form::close() }}
                    @endif
            </div>
        </div>


    </div>
</div>


</div> <!-- container -->
</div> <!--wrap -->
</div> <!-- page-content -->
<script>

    
    var select = document.getElementById("role_activity_name");
        select.onchange = function(){
            var selectedString = select.options[select.selectedIndex].value;
            var event_id = document.getElementById("URL_Segment").value;
                window.location =  event_id + "?activityID=" + selectedString;
        }
        //alert(event_id);
        //window.location = "role/create/" . event_id . ;

    
</script>

@include('SEM.footer')