@include('SEM.header')
@include('SEM.header-nav')

<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <ol class="breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li>Users</li>
                <li class='active'>Show</li>
            </ol>

            <h1>Show Users</h1>
        </div>
        <div class="container">
            <div class="row">
              <div class="col-md-12">
                    <div class="panel panel-sky">
                        <div class="panel-heading">
                            <h4>All Users</h4>
                        </div>
                        <div class="panel-body collapse in">
                            @if (Session::has('message'))
                            <div class="alert alert-success">{{ Session::get('message') }}</div>
                            @endif
                            @if (Session::has('archived'))
                            <div class="alert alert-danger">{{ Session::get('archived') }}</div>
                            @endif

                            <div class="table-responsive">
                                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables" id="example">
                                    <thead>
                                        <tr>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Student ID</th>
                                            <th>Email Address</th>
                                            <th>Phone Number</th>
                                            <th>User Type</th>
                                            <th>Profile</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         @foreach ($users as $user)
                                        <tr>
                                            <td>{{ $user->userFirstName }}</td>
                                            <td>{{ $user->userLastName }}</td>
                                            <td>{{ $user->utsID }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ $user->userPhone }}</td>
                                            <td>
                                                @if($user->userType == 1)System Admin 
                                                @elseif($user->userType == 2)Event Manager
                                                @elseif($user->userType == 3)Sprout
                                                @elseif($user->userType == 4)Unpaid
                                                @elseif($user->userType == 5)External
                                                @endif                                            
                                            </td>
                                            <td>
                                                <a class="btn btn-small btn-info" href="{{ URL::to('volunteer/profile/' . $user->id )}}">View Profile</a> 
                                            </td>
                                            <td>
                                                {{ Form::open(array('url' => 'volunteer/findRoles/A'.$activityID, 'method' => 'GET', 'id' => 'findRoles', 'class' => 'form-horizontal', 'style' => 'margin-bottom:0px !important')) }}                                                                  
                                                    {{ Form::hidden('volunteerID', $user->id) }}        
                                                    {{ Form::submit('Select', array('class'=>'finish btn-success btn')) }}                                                           
                                                {{ Form::close() }}                                        
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div> <!-- container -->
    </div> <!--wrap -->
</div> <!-- page-content -->

@include('SEM.footer')