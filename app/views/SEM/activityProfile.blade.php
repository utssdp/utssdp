@include('SEM.header')
@include('SEM.header-nav')

<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <ol class="breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li class='active'>Events</li>
                <li class='active'>Profile</li>
            </ol>

            <h1>Event Profile</h1>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-info">
                        <div class="panel-body">
                            @if (Session::has('find'))
                            <div class="alert alert-success">{{ Session::get('find') }}</div>
                            @endif
                            @if (Session::has('message'))
                            <div class="alert alert-success">{{ Session::get('message') }}</div>
                            @endif
                            @if (Session::has('warning'))
                            <div class="alert alert-warning">{{ Session::get('warning') }}</div>
                            @endif
                            @if (Session::has('error'))
                            <div class="alert alert-danger">{{ Session::get('error') }}</div>
                            @endif
                            
							<div class="row">
                                <div class="col-md-6">
                                    <div class="table-responsive">
                                        <h3><strong>{{ $event->eventName .': '. $activity->activityName }}</strong></h3>
                                        <table class="table table-condensed">

                                            <!-- <thead>
												<tr>
													<th width="50%"></th>
													<th width="50%"></th>
												</tr>
											</thead> -->
                                            <tbody>
                                                <tr>
                                                    <td>Name</td>
                                                    <td>{{$activity->activityName}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Location</td>
                                                    <td>We forgot to put this in</td>
                                                </tr>
                                                <tr>
                                                    <td>Event</td>
                                                    <td>@if(!isset($event->userID))
                                                            None Assigned
                                                        @else
                                                            {{ $event->userID }}
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Date</td>
                                                    <td>{{date('d/m/Y', strtotime($activity->activityDate))}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Time</td>
                                                    <td>{{date('g:i A', strtotime($activity->activityStartDateTime)) . " - " . date('g:i A', strtotime($activity->activityEndDateTime))}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h3>Description</h3>
                                    <p>{{ $activity->activityDescription }}</p>
                                </div>
                            </div>
                            <hr>

                            <?php $i = 0 ?>
                            @foreach($roles as $role)
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h4>Role: {{ $role->roleName }}</h4>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Role Name</th>
                                                <th>Activity Name</th>
                                                <th>Date</th>
                                                <th>Pay Rate</th>
                                                <th>Positions Filled</th>
                                                <th width="20%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $role->roleName }}</td>
                                                <td>{{ $role->activityName }}</td>
                                                <td>{{ date('d/m/Y', strtotime($role->activityDate)) }}</td>
                                                <td>{{ '$ ' . $role->activityRolePayRate }}</td>
                                                <td>
                                                    <strong>{{ (array_key_exists($role->activityRoleID,$positionsFilled)) ? $positionsFilled[$role->activityRoleID] : '0' }}  out of {{ $role->positionsAvailable }} ({{ (array_key_exists($role->activityRoleID,$pending)) ? $pending[$role->activityRoleID] : '0' }} Pending)</strong>
                                                    <div class="progress progress-striped" style="margin: 5px 0 0">
                                                        <div class="progress-bar progress-bar-info" style="width: {{ (array_key_exists($role->activityRoleID,$positionsFilled)) ? (($positionsFilled[$role->activityRoleID]/$role->positionsAvailable)*100). '%' : '0%'}}"></div>                                                        
                                                        <div class="progress-bar progress-bar-warning" style="width: {{ (array_key_exists($role->activityRoleID,$pending)) ? (($pending[$role->activityRoleID]/$role->positionsAvailable)*100). '%' : '0%'}}"></div>
                                                    </div>
                                                </td>
                                                <td>@if (!array_key_exists($role->activityRoleID,$rolesStatus))      
                                                {{ Form::open(array('url' => 'volunteer/apply', 'method' => 'POST', 'id' => 'volunteer', 'class' => 'form-horizontal', 'style' => 'margin-bottom:0px !important')) }}                                                                  
                                                    {{ Form::hidden('activityRoleID', $role->activityRoleID) }}                                                                  
                                                    @if(Auth::user()->userType <= 2)
                                                    <a class="btn btn-small btn-info" href="{{ URL::to('volunteer/find/' . $role->activityRoleID) }}">Find Volunteers</a>
                                                    @endif 
                                                    {{ Form::submit('Apply', array('class'=>'finish btn-success btn')) }}                                                           
                                                {{ Form::close() }}
                                                @else                                                                    
                                                    @if(Auth::user()->userType <= 2)
                                                    <a class="btn btn-small btn-info" href="{{ URL::to('volunteer/find/' . $role->activityRoleID) }}">Find Volunteers</a>
                                                    @endif 
                                                @endif
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    
                                    <div id="accordioninpanel{{$i}}" class="accordion-group">
                                        <div class="accordion-item">
                                            <a class="accordion-title collapsed" data-toggle="collapse" data-parent="#accordioninpanel{{$i}}" href="#collapseinOne{{$i}}">
                                                <h4>Volunteers <span class="fa fa-caret-down"></span></h4>
                                            </a>
                                            <div id="collapseinOne{{$i}}" class="collapse" style="height: 0px;">
                                               <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>First Name</th>
                                                            <th>Last Name</th>
                                                            <th>User Type</th>
                                                            <th>View Profile</th>
                                                            <th width="20%">Status</th>
                                                        </tr>
                                                    </thead>
                                                        
                                                    @foreach($volunteers as $volunteer)
                                                    @if($volunteer->activityRoleID == $role->activityRoleID)
                                                    <tbody>
                                                        <tr>
                                                            <td>{{ $volunteer->userFirstName }}</td>
                                                            <td>{{ $volunteer->userLastName }}</td>
                                                            <td>
                                                                @if($volunteer->userType == 1)System Admin 
                                                                @elseif($volunteer->userType == 2)Event Manager
                                                                @elseif($volunteer->userType == 3)Sprout
                                                                @elseif($volunteer->userType == 4)Unpaid
                                                                @elseif($volunteer->userType == 5)External
                                                                @endif        
                                                            </td>
                                                            <td>View</td>
                                                            <td>                                                               
                                                                @if (array_key_exists($volunteer->activityRoleID,$rolesStatus))
                                                                    <i style="color: green" class="fa fa-check-circle"></i>&nbsp<strong> Applied</strong><br />  
                                                                    @if ($rolesStatus[$volunteer->activityRoleID]['selected'] == 1)   
                                                                        <i style="color: green" class="fa fa-check-circle"></i>&nbsp<strong> Selected</strong><br />
                                                                    @else
                                                                        <i style="color: orange" class="fa fa-circle-o"></i>&nbsp<strong> Selected</strong><br />
                                                                    @endif
                                                                    @if ($rolesStatus[$volunteer->activityRoleID]['confirmed'] == 1)   
                                                                        <i style="color: green" class="fa fa-check-circle"></i>&nbsp<strong> Confirmed</strong><br />
                                                                    @else
                                                                        <i style="color: orange" class="fa fa-circle-o"></i>&nbsp<strong> Confirmed</strong><br />
                                                                    @endif
                                                                @else
                                                                    <i style="color: orange" class="fa fa-circle-o"></i>&nbsp<strong> Applied</strong><br />
                                                                    <i style="color: orange" class="fa fa-circle-o"></i>&nbsp<strong> Selected</strong><br />
                                                                    <i style="color: orange" class="fa fa-circle-o"></i>&nbsp<strong> Confirmed</strong><br />                                                                    
                                                                @endif  
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                    @endif
                                                    @endforeach
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $i++ ?>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- container -->
</div>
<!--wrap -->
</div>
<!-- page-content -->

@include('SEM.footer')