@include('SEM.header')
@include('SEM.header-nav')

<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <ol class="breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li>Volunteer</li>
                <li>Allocations</li>
                <li class='active'>View</li>
            </ol>

            @if(isset($volunteerID))
                <h1>Your Role Allocations</h1>
            @else           
                <h1>Volunteer Allocations</h1>
            @endif
            <?PHP $segment = Request::segment(2);  ?>
        </div>
        <div class="container">
            <div class="row">
              <div class="col-md-12">
                    <div class="panel panel-sky">
                        <div class="panel-heading">
                            <h4>Confirmations</h4>
                        </div>
                        <div class="panel-body collapse in">
                            @if (Session::has('message'))
                            <div class="alert alert-success">{{ Session::get('message') }}</div>
                            @endif
                            @if (Session::has('declined'))
                            <div class="alert alert-danger">{{ Session::get('declined') }}</div>
                            @endif
                            @if (Session::has('error'))
                            <div class="alert alert-danger">{{ Session::get('error') }}</div>
                            @endif
                            
                            <div class="table-responsive">
                                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables" id="example">
                                    <thead>
                                        <tr>
                                            <th>Event Name</th>
                                            <th>Activity Name</th>
                                            <th>Role Name</th>
                                            <th>Date</th>
                                            <th>Time</th>
                                            <th>Volunteer</th>
                                            <th>Confirmed</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                          @foreach ($allocations as $allocation)
                                        <tr>
                                            <td>{{ $allocation->eventName }}</td>
                                            <td>{{ $allocation->activityName }}</td>
                                            <td>{{ $allocation->roleName }}</td>
                                            <td>{{ date('d/m/Y', strtotime($allocation->activityDate)) }}</td> 
                                            <td>{{ date('h:i A', strtotime($allocation->activityStartDateTime)) . " - " . date('h:i A', strtotime($allocation->activityEndDateTime)) }}</td>                                   
                                            <td>{{ $allocation->userFirstName . ' ' . $allocation->userLastName }}</td>
                                            <td>{{ ($allocation->confirmed) ? '<b>Yes</b>' : '<b>No</b>' }}</td>

                                            <td class="text-center" width="15%">    
                                                @if(isset($volunteerID) && (Auth::user()->id == $volunteerID))
                                                    @if($allocation->confirmed == 0)                                        
                                                    {{ Form::open(array('url' => 'volunteer/select', 'method' => 'POST', 'id' => 'volunteer', 'class' => 'form-horizontal', 'style' => 'margin-bottom:0px !important')) }} 
                                                        {{ Form::hidden('registrationID', $allocation->registrationID) }}
                                                        {{ Form::submit('Confirm', array('name' => 'confirm', 'class'=>'finish btn-success btn')) }}  
                                                        {{ Form::submit('Decline', array('name' => 'decline', 'class'=>'finish btn-danger btn')) }}                                                         
                                                    {{ Form::close() }}
                                                    @else
                                                    {{ Form::open(array('url' => 'volunteer/select/withdraw', 'method' => 'POST', 'id' => 'volunteer', 'class' => 'form-horizontal', 'style' => 'margin-bottom:0px !important')) }} 
                                                        {{ Form::hidden('registrationID', $allocation->registrationID) }}
                                                        {{ Form::submit('Withdraw', array('name' => 'withdraw', 'class'=>'finish btn-warning btn')) }}                                                        
                                                    {{ Form::close() }}
                                                    @endif
                                                @endif
                                                @if(!isset($volunteerID) && (Auth::user()->userType <= 2))  
                                                    {{ Form::open(array('url' => 'volunteer/reject', 'method' => 'POST', 'id' => 'volunteer', 'class' => 'form-horizontal', 'style' => 'margin-bottom:0px !important')) }} 
                                                        {{ Form::hidden('registrationID', $allocation->registrationID) }}
                                                        {{ Form::submit('Deallocate Volunteer', array('name' => 'remove', 'class'=>'finish btn-danger btn')) }}     
                                                    {{ Form::close() }}
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
   
                        </div>
                    </div>
                </div>
            </div>

        </div> <!-- container -->
    </div> <!--wrap -->
</div> <!-- page-content -->

@include('SEM.footer')