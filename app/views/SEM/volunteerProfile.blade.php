@include('SEM.header')
@include('SEM.header-nav')

<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <ol class="breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li class='active'>Events</li>
                <li class='active'>Profile</li>
            </ol>

            <h1>Volunteer Profile</h1>

            <div class="options">
                <div class="btn-toolbar">
                    <div class="btn-group hidden-xs">
                        <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cloud-download"></i><span class="hidden-sm"> Export Report  </span><span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?PHP echo URL::to('volunteer/report/participation/' . Auth::user()->id )?>" target="_blank">Participation Report File (PDF)</a></li>
                            <li><a href="<?PHP echo URL::to('volunteer/report/registrations/' . Auth::user()->id )?>" target="_blank">Registration Report File (PDF)</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-info">
                        <div class="panel-body">                            
							<div class="row">
                                <div class="col-md-6">
                                    <div class="table-responsive">
                                        <h3><strong>Volunteer Profile</strong></h3>
                                        <table class="table table-condensed">
                                            <tbody>
                                                <tr>
                                                    <td>Name</td>
                                                    <td>{{$userRecords[0]->userFirstName . ' ' . $userRecords[0]->userLastName}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Gender</td>
                                                    <td>{{$userRecords[0]->userGender}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Email</td>
                                                    <td>{{$userRecords[0]->email}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Phone</td>
                                                    <td>{{$userRecords[0]->userPhone}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h3>Additional Info</h3>
                                    <table class="table table-condensed">
                                        <tbody>
                                            <tr>
                                                <td>Skills</td>
                                                <td>
                                                    @foreach($userSkills as $userSkill)
                                                        <label class="label label-primary">{{$userSkill->skillName}}</label> &nbsp
                                                    @endforeach
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Training</td>
                                                <td>
                                                    <ul class="list-unstyled">
                                                        @foreach($userTrainings as $userTraining)
                                                            <li><h4><label class="label label-info">{{$userTraining->trainingName}}</label></h4></li>
                                                        @endforeach
                                                    </ul>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <hr>
                            
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h4>Participation</h4>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Event Name</th>
                                                    <th>Activity Name</th>
                                                    <th>Description</th>
                                                    <th>Paid</th>
                                                    <th>Date</th>
                                                    <th>Availability</th>
                                                    <th>Registration Notes</th>
                                                </tr>
                                            </thead>
                                            <tbody>  
                                                @foreach ($userRecords as $userRecord)   
                                                @if($userRecord->confirmed == 1 && $userRecord->rejected == 0 && $userRecord->declined == 0)                                        
	              							    <tr>
                                                   <td>{{ $userRecord->eventName }} </td>
                                                   <td>{{ $userRecord->activityName }} </td>
                                                   <td>{{ $userRecord->activityDescription }} </td>
                                                   <td>{{ ($userRecord->isPaid) ? 'Yes' : 'No' }} </td>
                                                   <td>{{ date('d/m/Y', strtotime($userRecord->activityDate)) }} </td>
                                                   <td>{{ date('h:i A', strtotime($userRecord->availableFrom)) . ' - ' . date('h:i A', strtotime($userRecord->availableTo)) }} </td>
                                                   <td>{{ $userRecord->notes }} </td>
                                                </tr>
                                                @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h4>Registered</h4>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Event Name</th>
                                                    <th>Activity Name</th>
                                                    <th>Description</th>
                                                    <th>Paid</th>
                                                    <th>Date</th>
                                                    <th>Availability</th>
                                                    <th>Registration Notes</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>  
                                                @foreach ($userRecords as $userRecord)   
                                                @if(($userRecord->confirmed == 0 || $userRecord->rejected == 1 || $userRecord->declined == 1) && $userRecord->registrationID != null)                                        
	              							    <tr>
                                                   <td>{{ $userRecord->eventName }} </td>
                                                   <td>{{ $userRecord->activityName }} </td>
                                                   <td>{{ $userRecord->activityDescription }} </td>
                                                   <td>{{ ($userRecord->isPaid) ? 'Yes' : 'No' }} </td>
                                                   <td>{{ date('d/m/Y', strtotime($userRecord->activityDate)) }} </td>
                                                   <td>{{ date('h:i A', strtotime($userRecord->availableFrom)) . ' - ' . date('h:i A', strtotime($userRecord->availableTo)) }} </td>
                                                   <td>{{ $userRecord->notes }} </td>
                                                   <td>
                                                        @if ($userRecord->declined == 1) 
                                                            <i style="color: red" class="fa fa-times-circle"></i>&nbsp<strong> Declined</strong><br />  
                                                        @elseif($userRecord->rejected == 1)
                                                            <i style="color: red" class="fa fa-times-circle"></i>&nbsp<strong> Rejected</strong><br /> 
                                                        @elseif ($userRecord->allocated  == 1)   
                                                            <i style="color: green" class="fa fa-check-circle"></i>&nbsp<strong> Allocated</strong><br />
                                                        @else 
                                                            <i style="color: orange" class="fa fa-check-circle"></i>&nbsp<strong> Applied</strong><br />
                                                        @endif
                                                   </td>
                                                </tr>
                                                @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            

                        </div>
                    </div>
                </div>
            </div>          


        </div> <!-- container -->
    </div> <!--wrap -->
</div> <!-- page-content -->            
  
@include('SEM.footer')