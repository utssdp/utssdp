@include('SEM.header')
@include('SEM.header-nav')

<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <ol class="breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li>Volunteer</li>
                <li class='active'>Allocate</li>
            </ol>

            <h1>Find Roles</h1>
            <?PHP $segment = Request::segment(2);  ?>
        </div>
        <div class="container">
            <div class="row">
              <div class="col-md-12">
                    <div class="panel panel-sky">
                        <div class="panel-heading">
                            <h4>Roles</h4>
                            <div class="pull-right"><a class="btn btn-small btn-success" href="{{ URL::to('roleCreate/' . $segment) }}">Add Role</a></div>
                        </div>
                        <div class="panel-body collapse in">
                            @if (Session::has('message'))
                            <div class="alert alert-success">{{ Session::get('message') }}</div>
                            @endif
                            @if (Session::has('archived'))
                            <div class="alert alert-danger">{{ Session::get('archived') }}</div>
                            @endif
                            
                            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables" id="example">
                                <thead>
                                    <tr>
                                        <th>Role Name</th>
                                        <th>Role Description</th>
                                        <th>Positions Available</th>
                                        <th>Pay Rate</th>
                                        
                                        <th>Modify</th>
                                    </tr>
                                </thead>
                                <tbody>
                                      @foreach ($roles as $role)
                                    <tr>
                                        <td>{{ $role->roleName }}</td>
                                        <td>{{ $role->roleDescription }}</td>
                                        <td>{{ $role->positionsAvailable }}</td>
                                        <td>{{ $role->activityRolePayRate }}</td>
                                        

                                        <td width="15%">
                                        {{ Form::open(array('url' => 'role/' . $role->roleID . '/' . $segment)) }}
                                            {{ Form::hidden('_method', 'DELETE') }}
                                            <a class="btn btn-small btn-warning" href="{{ URL::to('role/edit/' . $role->activityRoleID) }}">Edit Role</a>
                                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                                        {{ Form::close() }}    
                                        
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
   
                        </div>
                    </div>
                </div>
            </div>

        </div> <!-- container -->
    </div> <!--wrap -->
</div> <!-- page-content -->

@include('SEM.footer')