<!-- app/views/login.blade.php -->
@include('SEM.header-focused')
<div class="verticalcenter">
    
    <a href="{{URL::to('/')}}" class="brand">
        <img src="{{ URL::asset('assets/img/Logo.svg'); }}" style="width:224px;height:77px" />
    </a>
    
    
    
    <div class="panel panel-primary">
        @if (Session::has('message') && Session::get('message') != "Email you have entered is incorrect.")
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
        @else
        <div class="alert alert-info">{{ Session::get('message') }}</div>
        <div class="panel-body">
            
            <h4 class="text-center" style="margin-bottom: 25px;"><h4>Reset Your Password!</h4> 
                {{ Form::open(array('class' => 'form-horizontal', 'style' => 'margin-bottom:0px !important')) }}
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            {{ Form::text('email', Input::old('email'), array('placeholder' => 'awesome@awesome.com', 'class' => 'form-control')) }}
                        </div>
                    </div>
                </div>
                
        </div>
        
        <div class="panel-footer">
            <div class="pull-right">
                
            {{ Form::submit('Reset!', array('class'=>'btn btn-primary')) }}
            {{ Form::close() }}  
            </div>
        </div>
        @endif
    </div>
</div>
</body>
</html>