@include('SEM.header')
@include('SEM.header-nav')

<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <ol class="breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li>Accounts</li>
                <li class='active'>Create</li>
            </ol>

            <h1>Create Event Manager</h1>
        </div>


        <div class="container">    
            
            
            <div class="row">
            <div class="col-md-12">
               <div class="panel panel-midnightblue">
                  <div class="panel-heading">
                     <h4>Event Manager Creation Form</h4>
                  </div>
                  <div class="panel-body collapse in">
                    @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @else  
                     {{ Form::open(array('url' => 'addEventManager', 'class' => 'form-horizontal', 'style' => 'margin-bottom:0px !important')) }}
                        <div class="form-group">
                           <label class="col-sm-3 control-label">UTS ID</label>
                           <div class="col-sm-6">
                               {{ Form::text('utsID', '', array('class' => 'form-control', 'placeholder' => 'UTS ID#', 'autofocus', 'maxlength' => '6')) }}
                           </div>
                            {{ $errors->first('utsID') }}
                        </div>
                        <div class="form-group">
                           <label class="col-sm-3 control-label">First Name</label>
                           <div class="col-sm-6">
                               {{ Form::text('userFirstName', '', array('class' => 'form-control', 'placeholder' => 'First Name')) }}
                           </div>
                                {{ $errors->first('userFirstName') }}
                        </div>
                        <div class="form-group">
                           <label class="col-sm-3 control-label">Last Name</label>
                           <div class="col-sm-6">
                               {{ Form::text('userLastName', '', array('class' => 'form-control', 'placeholder' => 'Last Name')) }}
                           </div>
                                {{ $errors->first('userLastName') }}
                        </div>
                      <div class="form-group">
                           <label class="col-sm-3 control-label">Gender</label>
                           <div class="col-sm-6">
                               {{ Form::select('userGender', array('Male' => 'Male', 'Female' => 'Female'), null, array('class' => 'form-control')) }}
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-3 control-label">Email</label>
                           <div class="col-sm-6">
                               {{ Form::text('email', Input::old('email'), array('placeholder' => 'Enter UTS Email', 'class' => 'form-control')) }}
                           </div>
                            {{ $errors->first('email') }}
                        </div>             
                        <div class="form-group">
                           <label class="col-sm-3 control-label">Phone Number</label>
                           <div class="col-sm-6">
                               {{ Form::text('userPhone', '', array('class' => 'form-control', 'placeholder' => 'Enter Phone Number (XX) XXXX XXXX or +61XXX XXX XXX')) }}
                           </div>
                                {{ $errors->first('userPhone') }}
                        </div>
                      
                      <div class="form-group">
                           <label class="col-sm-3 control-label">User Type</label>
                           <div class="col-sm-6">
                               {{ Form::select('userType', array('2' => 'Event Manager', '1' => 'Administrator'), null, array('class' => 'form-control')) }}
                           </div>
                        </div>   
                               
                    

                     <div class="panel-footer">
                        <div class="row">
                           <div class="col-sm-6 col-sm-offset-3">
                              <div class="btn-toolbar">
                                {{ Form::submit('Register', array('class'=>'btn btn-primary')) }}
                                {{ Form::close() }}
                                @endif  
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>

         </div>
      </div>
   </div>
            
            

        </div> <!-- container -->
    </div> <!--wrap -->
</div> <!-- page-content -->

@include('SEM.footer')