<!-- app/views/login.blade.php -->
@include('SEM.header-focused')
<div class="verticalcenter">
    
    <a href="{{URL::to('/')}}" class="brand">
        <img src="{{ URL::asset('assets/img/Logo.svg'); }}" style="width:224px;height:77px" />
    </a>
    
    
    <div class="panel panel-primary">
        
        <div class="panel-body">
            
            <h4 class="text-center" style="margin-bottom: 25px;">Log in to get started!</a></h4>
                @if (Session::has('LoginFailed'))
                <div class="alert alert-danger">{{ Session::get('LoginFailed') }}</div>
                @endif

            {{ Form::open(array('url' => 'login', 'class' => 'form-horizontal', 'style' => 'margin-bottom:0px !important')) }}
                
                <!-- if there are login errors, show them here -->
                <p>
                {{ $errors->first('email') }}
                {{ $errors->first('password') }}
                </p>
            
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            {{ Form::text('email', Input::old('email'), array('placeholder' => 'awesome@awesome.com', 'class' => 'form-control', 'autofocus')) }}
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) }}
                        </div>
                    </div>
                </div>

                

               
            

        </div>
        
        <div class="panel-footer">
            <a href="<?PHP echo URL::route('password'); ?>" class="pull-left btn btn-link" style="padding-left:0">Forgot password?</a>

            <div class="pull-right">
                
            <a href="#" class="btn btn-default">Reset</a>
            {{ Form::submit('Log In!', array('class'=>'btn btn-primary')) }}
            {{ Form::close() }}    
            </div>
        </div>
    </div>
</div>
</body>
</html>
