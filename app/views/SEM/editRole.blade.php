@include('SEM.header')
@include('SEM.header-nav')

<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <ol class="breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li>Roles</li>
                <li class="active">Edit</li>
            </ol>

            <h1>Edit a Role</h1>
            
        </div>
        <div class="container">


<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h4>{{ $activity->activityName . ": " . $role->roleName}}</h4>
                
            </div>
            <div class="panel-body">
                {{ Form::open(array('action' => 'RoleController@updateRole', 'method' => 'POST', 'id' => 'createRoles', 'class' => 'form-horizontal', 'style' => 'margin-bottom:0px !important')) }}    
                    {{ Form::hidden('activityRoleID', $role->activityRoleID) }}
                
                    <legend>Edit Role </legend>
                        
                    <!--
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Reports To</label>
                        <div class="col-sm-6">
                            <!-- this should be moved to controller" 
                            <?PHP                                   
                                
                                $roles =  DB::table('ActivityRole')            
                                    ->join('Role', 'ActivityRole.RoleID', '=', 'Role.RoleID')
                                    ->select('*')
                                    ->where('ActivityRole.activityID', '=', $activity->activityID)                                        
                                    ->get();
                                    
                                $activityRoles = array();
                                foreach($roles as $key => $act)
                                {
                                    $activityRoles[$act->roleID] = $act->roleName;
                                }
                                    
                                array_unshift($activityRoles,null);

                                echo Form::select('role_reports_to', $activityRoles, null, array('style' => 'width:100%', 'class' => 'populate', 'id' => 'role_reports_to'));

                            ?>
                        </div>
                    </div>-->
                        
                    <div class="form-group">
                        <label for="role_name" class="col-md-3 control-label">Role Name</label>
                        <div class="col-md-6">    
                            {{ Form::text('role_name', $role->roleName, array('class' => 'form-control', 'name' => 'role_name', 'placeholder' => 'Role Name', 'required' => 'required', 'minlength' => '5', 'type' => 'text')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="role_description" class="col-md-3 control-label">Role Description</label>
                        <div class="col-md-6">                                
                                {{ Form::textarea('role_description', $role->roleDescription, array('id' => 'role_description', 'class' => 'form-control autosize', 'name' => 'role_description', 'required' => 'required', 'rows' => '2')) }}
                        </div>
                    </div>

                
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Skills</label>
                        <div class="col-sm-1">
                            <div class="table-responsive">
                                <select id="skills" name="skills[]" multiple style="width:100%" class="populate">
                                    @foreach($skills as $skill)
                                        <option value="{{$skill->skillID}}" {{ (in_array($skill->skillID, $roleSkills)) ? "selected='selected'" : '' }}>{{$skill->skillName}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
                
            <div class="panel-footer">
                <div class="pull-right">
                    <a class="btn btn-small btn-info" href="{{ URL::to('activity/'. $activity->activityID .'/showRoles')}}">Cancel</a> &nbsp;
                    {{ Form::submit('Update', array('class'=>'finish btn-success btn pull-right')) }}
                    {{ Form::close() }}
                </div>
            </div>

            </form>
        </div>


    </div>
</div>


</div> <!-- container -->
</div> <!--wrap -->
</div> <!-- page-content -->
<!--
<script>

    
    var select = document.getElementById("role_activity_name");
        select.onchange = function(){
            var selectedString = select.options[select.selectedIndex].value;
            var event_id = document.getElementById("URL_Segment").value;
                window.location =  event_id + "?activityID=" + selectedString;
        }
        //alert(event_id);
        //window.location = "role/create/" . event_id . ;

    
</script>
-->

@include('SEM.footer')