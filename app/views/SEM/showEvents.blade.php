@include('SEM.header')
@include('SEM.header-nav')

<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <ol class="breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li>Events</li>
                <li class='active'>View</li>
            </ol>

            <h1>View Events</h1>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-sky">
                        <div class="panel-heading">
                            <h4>All Events</h4>
                            <div class="pull-right"><a class="btn btn-small btn-success" href="{{ URL::route('event.index') }}">Create Event</a></div>
                        </div>
                        <div class="panel-body collapse in">
                            @if (Session::has('message'))
                            <div class="alert alert-success">{{ Session::get('message') }}</div>
                            @endif
                            @if (Session::has('error'))
                            <div class="alert alert-danger">{{ Session::get('error') }}</div>
                            @endif
                            @if (Session::has('archived'))
                            <div class="alert alert-danger">{{ Session::get('archived') }}</div>
                            @endif
                            <div class="table-responsive">

                                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables" id="example">
                                    <thead>
                                        <tr>
                                            <th>Event Name</th>
                                            <th>Description</th>
                                            <th>Location</th>
                                            <th>Event Manager</th>
                                            <th>Start Date</th>
                                            <th>End Date </th>
                                            <th>Modify</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($events as $event)
                                        <tr>
                                            <td>{{ $event->eventName }}</td>
                                            <td><a href="{{ URL::to('event/profile/' . $event->eventID) }}" class="btn btn-small btn-info tooltips" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="{{ $event->eventDescription }}">View Profile</a></td>
                                            <td>{{ $event->eventLocation }}</td>
                                            <td>{{ (isset($event->eventManager)) ? $event->userFirstName . ' '. $event->userLastName : "<i class='fa fa-warning'></i> No Event Manager Assigned"}}</td>
                                            <td>{{ (isset($event->eventStartDate)) ? date('F j, Y', strtotime($event->eventStartDate)) : "<i class='fa fa-warning'></i> No Start Date Assigned"}}</td>
                                            <td>{{ (isset($event->eventStartDate)) ?  date('F j, Y', strtotime($event->eventEndDate)) : "<i class='fa fa-warning'></i> No End Date Assigned" }}</td>

                                            <td width="20%">
                                                <a class="btn btn-small btn-info" style="width: 100%" href="{{ URL::to('event/' . $event->eventID . '/showActivity') }}">Show Activities</a>
                                                <a class="btn btn-small btn-warning" style="width: 100%" href="{{ URL::to('event/edit/' . $event->eventID) }}">Edit Event</a>
                                                <a class="btn btn-small btn-danger" style="width: 100%" href="{{ URL::to('event/check/' . $event->eventID) }}">Delete Event</a>
                                        
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                
                            </div>

                            <hr />

                            <h3>Past Events</h3>
                            <div class="table-responsive">

                                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables" id="example">
                                    <thead>
                                        <tr>
                                            <th>Event Name</th>
                                            <th>Description</th>
                                            <th>Location</th>
                                            <th>Event Manager</th>
                                            <th>Start Date</th>
                                            <th>End Date </th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($pastEvents as $pastEvent)
                                    <tr>
                                        <td>{{ $pastEvent->eventName }}</td>
                                        <td><a href="{{ URL::to('event/profile/' . $pastEvent->eventID) }}" class="btn btn-small btn-info tooltips" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="{{ $pastEvent->eventDescription }}">View Profile</a></td>
                                        <td>{{ $pastEvent->eventLocation }}</td>
                                        <td>{{ (isset($pastEvent->eventManager)) ? $pastEvent->userFirstName . ' '. $pastEvent->userLastName : "<i class='fa fa-warning'></i> No Event Manager Assigned"}}</td>
                                        <td>{{ (isset($pastEvent->eventStartDate)) ? date('F j, Y', strtotime($pastEvent->eventStartDate)) : "<i class='fa fa-warning'></i> No Start Date Assigned"}}</td>
                                        <td>{{ (isset($pastEvent->eventStartDate)) ?  date('F j, Y', strtotime($pastEvent->eventEndDate)) : "<i class='fa fa-warning'></i> No End Date Assigned" }}</td>

                                        <td width="20%">
                                            <a class="btn btn-small btn-info" style="width: 100%" href="{{ URL::to('event/' . $event->eventID . '/showActivity') }}">Show Activities</a>                                        
                                        </td>
                                    </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- container -->
    </div>
    <!--wrap -->
@if (Session::has('activityExist'))    
<div class="modal" id='myModal'>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Activities Exists!</h4>
      </div>
      <div class="modal-body">
        <p>There are Activities attached to this event. Are you sure you want to delete?</p>
      </div>
      <div class="modal-footer">
        {{ Form::open(array('url' => 'event/' . Session::get('activityExist'))) }}
        {{ Form::hidden('_method', 'DELETE') }}
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            {{ Form::submit('Yes I\'m sure!', array('class' => 'btn btn-danger')) }}
        {{ Form::close() }}    
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endif
    
@if (Session::has('eventNotPassed'))     
<div class="modal" id='myModal'>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Event has not passed yet!</h4>
      </div>
      <div class="modal-body">
        <p>The event has not passed yet. Are you sure you want to delete?</p>
      </div>
      <div class="modal-footer">
        {{ Form::open(array('url' => 'event/' . Session::get('activityExist'))) }}
        {{ Form::hidden('_method', 'DELETE') }}
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            {{ Form::submit('Yes I\'m sure!', array('class' => 'btn btn-danger')) }}
        {{ Form::close() }}    
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endif
    
</div>
<!-- page-content -->

@include('SEM.footer')