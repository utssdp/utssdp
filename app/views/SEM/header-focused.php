<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Login - eVol</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="SEM">
    <meta name="author" content="G21">

    <link rel="icon" type="image/png" href='<?PHP echo URL::asset('assets/img/favicon.png'); ?>'>    
    <link rel="stylesheet/css" media="all" href="<?PHP echo URL::asset('assets/css/styles.css'); ?>">
    <link rel="stylesheet/less" media="all" href="<?PHP echo URL::asset('assets/less/styles.less'); ?>">
    <script type="text/javascript" src="<?PHP echo URL::asset('assets/js/less.js');?>"></script>
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600' rel='stylesheet' type='text/css'>   
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    
</head>
<body class="focusedform">