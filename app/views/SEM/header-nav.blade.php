
    <?PHP 
    
    //badge badge values    
    $viewBadge = 0;
    $pendingRegistrationsBadge = 0; 
    $allocationsBadge = 0;  
    $volunteerBadge = 0;
    $GLOBALS['availabilityBadge'] = false;   
    $GLOBALS['skillsBadge'] = false; 
    
    if(Auth::user()->userType <= 2)
    {
        $pendingRegistrationsQuery = DB::table('Registrations')
                                    ->where('allocated','=',0)
                                    ->where('rejected','=',0)
                                    ->where('confirmed','=',0)
                                    ->where('declined','=',0)
                                    ->get();
    }
    
    $allocationsQuery = DB::table('Registrations')
                            ->where('allocated','=',1)
                            ->where('rejected','=',0)
                            ->where('confirmed','=',0)
                            ->where('declined','=',0)
                            ->where('volunteerID','=',Auth::User()->id)
                            ->get();
    
    $availabilityQuery = DB::table('Users')
                            ->where('id','=', Auth::user()->id)
                            ->where('availabilityUpdated','NOT',null)
                            ->get();
    
    $userSkillsQuery = DB::table('Userskill')
                        ->where('userID','=',Auth::user()->id)
                        ->get();
    
    if(isset($pendingRegistrationsQuery))
    {
        $pendingRegistrationsBadge = count($pendingRegistrationsQuery);
        $viewBadge += $pendingRegistrationsBadge;
    }
    
    $allocationsBadge = count($allocationsQuery);
    $viewBadge += $allocationsBadge;
    
    if(count($availabilityQuery))
    {
        $GLOBALS['availabilityBadge'] = true;
    }    
    
    if(count($userSkillsQuery) == 0)
    {
        $GLOBALS['skillsBadge'] = true;
    }   
    
    function csstag($address) {echo "<link rel='stylesheet' type='text/css' href='$address' /> \n";}
    
    $pageName = basename($_SERVER['PHP_SELF']); 
    
    //fill in conditional assets here
    
    ?>

<body class="<?php if (isset($_COOKIE["admin_leftbar_collapse"])) echo ($_COOKIE['admin_leftbar_collapse'] . " "); // check collapse state with php
                   if (isset($_COOKIE["admin_rightbar_show"])) echo $_COOKIE['admin_rightbar_show'];
                   if (isset($_COOKIE["fixed-header"])) echo ' static-header';
             ?>">

    <header class="navbar navbar-inverse <?php if (isset($_COOKIE["fixed-header"])) {echo 'navbar-static-top';} else {echo 'navbar-fixed-top';} ?>" role="banner">
        <a id="leftmenu-trigger" class="tooltips" data-toggle="tooltip" data-placement="right" title="" data-original-title="Toggle Sidebar"></a>

        <div class="text-center">
            <a class="navbar-brand" href="#">Small Event Marketing</a>
        </div>

        <div class="right-menu-trigger">
            <ul class="nav pull-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle username" data-toggle="dropdown" style="color: #888888"><span class="hidden-xs">{{ Auth::user()->firstName() }} {{ Auth::user()->lastName() }} <i class="fa fa-caret-down"></i></span>
                        <i class="fa fa-user" style="width: 20px; float: left; font-size: 2em; color: #888888;"></i></a>
                    <ul class="dropdown-menu userinfo arrow">
                        <li class="username">
                            <a href="#">
                                <div class="pull-right">
                                    <h5>Howdy, {{ Auth::user()->firstName() }}</h5>
                                    <small>Logged in as <span>{{ Auth::user()->email }}</span></small>
                                </div>
                            </a>
                        </li>
                        <li class="userlinks">
                            <ul class="dropdown-menu">
                                <li><a href="{{ URL::to('users/' . Auth::user()->id . '/edit') }}">Edit Profile <i class="pull-right fa fa-pencil"></i></a></li>
                                <li><a href="{{ URL::to('http://evoluts.freshdesk.com') }}" target="_blank">Help <i class="pull-right fa fa-question"></i></a></li>
                                <li class="divider"></li>
                                <li><a href="{{ URL::to('logout') }}" class="text-right">Log Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </header>


    <div id="page-container">
        <!-- BEGIN SIDEBAR -->
        <nav id="page-leftbar" role="navigation">
            <!-- BEGIN SIDEBAR MENU -->
            <ul class="acc-menu" id="sidebar">
                <li><a href="<?PHP echo URL::route('home'); ?>"><i class="fa fa-home"></i><span>Home</span></a></li>
                <li><a href="<?PHP echo URL::to('users/' . Auth::user()->id . '/edit')?>"><i class="fa fa-user"></i><span>Update Profile</span>@if($GLOBALS['availabilityBadge'] || $GLOBALS['skillsBadge'])<span class="badge badge-warning badge-sub"><i class="fa fa-warning"></i></span>@endif</a></li>

                <li class="divider"></li>
                @if(Auth::user()->userType() <= 2)
                <li><a href="javascript:;"><i class="fa fa-plus-circle"></i><span>Create</span></a>
                    <ul class="acc-menu">
                        <li><a href="<?PHP echo URL::route('event.index'); ?>"><span>Create Event</span></a></li>
                        
                        <li class="divider"></li>
                
                        @if(Auth::user()->userType() == 1)
                        <li><a href="<?PHP echo URL::route('create-event-manager'); ?>"><span>Create Event Manager</span></a></li>
                        @endif

                        <li><a href="<?PHP echo URL::route('create-volunteer'); ?>"><span>Create Volunteer</span></a></li>
                    </ul>
                </li>
                @endif
                @if(Auth::user()->userType() <= 4)
                <li><a href="javascript:;"><i class="fa fa-search"></i><span>Find</span></a>
                    <ul class="acc-menu">
                        @if(Auth::user()->userType() <= 2)
                        <li><a href="<?PHP echo URL::route('findVolunteers'); ?>"><span>Find Volunteers</span></a></li>
                        @endif
                        <li><a href="<?PHP echo URL::route('findEvents'); ?>"><span>Find Events</span></a></li>
                    </ul>
                </li>
                @endif
    
                <li><a href="javascript:;"><i class="fa fa-desktop"></i><span>View</span>@if($viewBadge > 0)<span class="badge badge-info">{{$viewBadge}}</span>@endif </a>
                    <ul class="acc-menu">
                        @if(Auth::user()->userType() <= 2)
                        <li><a href="<?PHP echo URL::route('showEvents'); ?>"><span>View Events</span></a></li>
                        <li><a href="<?PHP echo URL::route('showUsers'); ?>"><span>View Users</span></a></li>
                        <li><a href="<?PHP echo URL::route('showRegistrations'); ?>"><span>Pending Registrations</span>@if($pendingRegistrationsBadge > 0)<span class="badge badge-warning badge-sub">{{$pendingRegistrationsBadge}}</span>@endif</a></li>
                        <li><a href="<?PHP echo URL::route('showAllocations'); ?>"><span>Allocated Volunteers</span></a></li>
                        
                        <li class="divider"></li>
                        @endif
                        @if(Auth::user()->userType() <= 4)
                        <li><a href="<?PHP echo URL::to('volunteer/registrations/' . Auth::user()->id )?>"><span>Your Registrations</span></a></li>
                        @endif
                        <li><a href="<?PHP echo URL::to('volunteer/roles/' . Auth::user()->id ) ?>"><span>Your Roles</span>@if($allocationsBadge > 0)<span class="badge badge-success badge-sub">{{$allocationsBadge}}</span>@endif</a></li>
                        <li><a href="<?PHP echo URL::to('volunteer/profile/' . Auth::user()->id )?>"><span>Your Profile</span></a></li>
                    </ul>
                </li>
                <li><a href="javascript:;"><i class="fa fa-line-chart"></i><span>Report</span></a>
                    <ul class="acc-menu">
                        <li class="has-child"><a href="javascript:;">Volunteering</a>
                            <ul class="acc-menu">
                                <li><a href="<?PHP echo URL::to('volunteer/report/registrations/' . Auth::user()->id )?>" target="_blank"><span>Registrations Report</span></a></li>
                                <li><a href="<?PHP echo URL::to('volunteer/report/participation/' . Auth::user()->id )?>" target="_blank"><span>Participation Report</span></a></li>
                            </ul>
                        </li>
                        @if(Auth::user()->userType() <= 2)
                        <li><a href="<?PHP echo URL::to('eventmanager/' . Auth::user()->id . '/report/') ?>" target="_blank"><span>Event Manager Report</span></a></li>
                        @endif
                    </ul>
                </li>                

                <li class="divider"></li>

                <li style="position: fixed; bottom: 0px;"><a href="<?PHP echo URL::route('help'); ?>"><i class="fa fa-question-circle"></i><span>Help and Documentation</span></a></li>
    
            </ul>
            <!-- END SIDEBAR MENU -->
        </nav>
