@include('SEM.header')
@include('SEM.header-nav')

<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <ol class="breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li>Events</li>
                <li class="active">Edit</li>
            </ol>

            <h1>Edit an Event</h1>
            
        </div>
        <div class="container">


<div class="row">
    <div class="col-md-12">

        <div class="panel panel-info">
            <div class="panel-heading">
                <h4>{{$event->eventName}}</h4>
            </div>
            <div class="panel-body">
                {{ Form::open(array('url' => 'event/update/'.$event->eventID, 'method' => 'GET', 'id' => 'editEvent', 'class' => 'form-horizontal')) }}    
                   
                    <legend>Edit Event</legend>
                    <div class="form-group">
                        <label for="event_name" class="col-md-3 control-label">Event Name</label>
                        <div class="col-md-6">
                            {{ Form::text('event_name', $event->eventName, array('class' => 'form-control', 'name' => 'event_name', 'placeholder' => 'Event Name', 'required' => 'required', 'minlength' => '5', 'type' => 'text')) }}
                        </div>
                        {{ $errors->first('event_name') }}
                    </div>
                    <div class="form-group">
                        <label for="event_location" class="col-md-3 control-label">Event Location</label>
                        <div class="col-md-6">
                        {{ Form::text('event_location', $event->eventLocation, array('class' => 'form-control', 'name' => 'event_location', 'placeholder' => 'Event Location', 'minlength' => '5', 'type' => 'text')) }}
                        </div>
                        {{ $errors->first('event_location') }}
                    </div>
                
                    <div class="form-group">
                        <label for="event_description" class="col-md-3 control-label">Event Description</label>
                        <div class="col-md-6">
                        {{ Form::textarea('event_description', $event->eventDescription, array('class' => 'form-control', 'name' => 'event_description', 'placeholder' => 'Event Description', 'minlength' => '5', 'type' => 'text')) }}
                        </div>
                        {{ $errors->first('event_description') }}
                    </div>


                    <div class="form-group">
                        <label class="col-sm-3 control-label">Event Manager</label>
                        <div class="col-sm-6">
                            {{ Form::select('event_manager', $eventManagers, $event->eventManager, array('style' => 'width:100%', 'class' => 'populate', 'id' => 'event_manager')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Event Start &amp; End Date/Time</label>
                        <div class="col-sm-6">
                            {{ Form::text('eventStartEndDate', $dateString, array('class' => 'form-control', 'name' => 'eventStartEndDate', 'id' => 'eventStartEndDate', 'type' => 'text')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Event Start Time</label>
                        <div class="col-sm-6">
                            {{ Form::text('event_start_time', date('H:i', strtotime($event->eventStartTime)), array('class' => 'form-control', 'name' => 'event_start_time', 'id' => 'event_start_time', 'type' => 'text')) }}
                        </div>
                    </div>
            </div>

            <div class="panel-footer">
                <div class="pull-right">
                    <a class="btn btn-small btn-info" href="{{ URL::route('showEvents')}}">Cancel</a>
                    {{ Form::submit('Update Event', array('class'=>'finish btn-success btn')) }}    
                    {{ Form::close() }}
                </div>
            </div>
                
        </div>


    </div>
</div>


</div> <!-- container -->
</div> <!--wrap -->
</div> <!-- page-content -->


@include('SEM.footer')