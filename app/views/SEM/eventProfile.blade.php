@include('SEM.header')
@include('SEM.header-nav')

<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <ol class="breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li class='active'>Events</li>
                <li class='active'>Profile</li>
            </ol>

            <h1>Event Profile</h1>

            <div class="options">
                <a href="<?PHP echo URL::to('eventmanager/' . Auth::user()->id . '/report/' . $event->eventID) ?>" target="_blank" class="btn btn-default dropdown-toggle" ><i class="fa fa-cloud-download"></i><span class="hidden-sm"> Export Event Report  </span></a>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-info">
                        <div class="panel-body">
                            @if (Session::has('find'))
                            <div class="alert alert-success">{{ Session::get('find') }}</div>
                            @endif
                            @if (Session::has('message'))
                            <div class="alert alert-success">{{ Session::get('message') }}</div>
                            @endif
                            @if (Session::has('messageFull'))
                            <div class="alert alert-warning">{{ Session::get('messageFull') }}</div>
                            @endif
                            @if (Session::has('warning'))
                            <div class="alert alert-warning">{{ Session::get('warning') }}</div>
                            @endif
                            @if (Session::has('error'))
                            <div class="alert alert-danger">{{ Session::get('error') }}</div>
                            @endif
                            
                           
                            <!--<div class="alert">{{ $errors->first('time') }} {{ $errors->first('startTime') }} {{ $errors->first('endTime') }}</div>-->
                           
                            
                            
							<div class="row">
                                <div class="col-md-6">
                                    <div class="table-responsive">
                                        <h3><strong>{{ $event->eventName }}</strong></h3>
                                        <table class="table table-condensed">

                                            <!-- <thead>
												<tr>
													<th width="50%"></th>
													<th width="50%"></th>
												</tr>
											</thead> -->
                                            <tbody>
                                                <tr>
                                                    <td>Name</td>
                                                    <td>{{$event->eventName}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Location</td>
                                                    <td>{{$event->eventLocation}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Manager</td>
                                                    <td>
                                                        @if(!isset($event->eventManager))
                                                            None Assigned
                                                        @else
                                                            {{ $event->userFirstName . ' ' . $event->userLastName }}
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Start Date</td>
                                                    <td>{{date('d/m/Y', strtotime($event->eventStartDate))}}</td>
                                                </tr>
                                                <tr>
                                                    <td>End Date</td>
                                                    <td>{{date('d/m/Y', strtotime($event->eventEndDate))}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Start Time</td>
                                                    <td>{{date('h:i A', strtotime($event->eventStartTime))}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h3>Description</h3>
                                    <p>{{ $event->eventDescription }}</p>
                                </div>
                            </div>
                            <hr>

                            @foreach ($activities as $activity)
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h4>Activity: {{ $activity->activityName }}</h4>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Activity Name</th>
                                                    <th>Description</th>
                                                    <th>Paid/Unpaid</th>
                                                    <th>Date</th>
                                                    <th>Time</th>
                                                    <th>Positions</th>
                                                    <th>Status</th>
                                                    @if(strtotime($event->eventEndDate) >= strtotime(date('Y-m-d')))
                                                    <th>Action</th>
                                                    @endif
                                                </tr>
                                            </thead>
                                            <tbody>                                                           
	              							    <tr>
                                                    <td>{{$activity->activityName}}</td>
                                                    <td><a href="#" data-toggle="popover" data-trigger="focus" data-content="{{$activity->activityDescription}}">View</a></td>
                                                    <td>{{ ($activity->isPaid) ? 'Yes' : 'No' }}</td>
                                                    <td>{{date('d/m/Y', strtotime($activity->activityDate))}}</td>
                                                    <td>{{date('h:i A', strtotime($activity->activityStartDateTime)) . " - " . date('h:i A', strtotime($activity->activityEndDateTime))}}</td>
                                                    <td>
                                                        <strong>{{ (array_key_exists($activity->activityID,$positionsFilled)) ? $positionsFilled[$activity->activityID] : '0' }}  out of {{  $activity->positionsAvailable }} ({{ (array_key_exists($activity->activityID,$pending)) ? $pending[$activity->activityID] : '0' }} Pending)</strong>
                                                        <div class="progress progress-striped" style="margin: 5px 0 0">
                                                           <div class="progress-bar progress-bar-info" style="width: {{ (array_key_exists($activity->activityID,$positionsFilled)) ? (($positionsFilled[$activity->activityID]/$activity->positionsAvailable)*100). '%' : '0%'}}"></div>
                                                           <div class="progress-bar progress-bar-warning" style="width: {{ (array_key_exists($activity->activityID,$pending)) ? (($pending[$activity->activityID]/$activity->positionsAvailable)*100). '%' : '0%'}}"></div>
                                                        </div>
                                                    </td>                                                     
                                                    <td>
                                                        @if (array_key_exists($activity->activityID,$registrationStatus))
                                                            @if ($registrationStatus[$activity->activityID]['declined'] == 1) 
                                                                <i style="color: red" class="fa fa-times-circle"></i>&nbsp<strong> Declined</strong><br />  
                                                            @elseif($registrationStatus[$activity->activityID]['rejected'] == 1)
                                                                <i style="color: red" class="fa fa-times-circle"></i>&nbsp<strong> Rejected</strong><br />  
                                                            @else
                                                                <i style="color: green" class="fa fa-check-circle"></i>&nbsp<strong> Applied</strong><br />  
                                                                @if ($registrationStatus[$activity->activityID]['allocated'] == 1)   
                                                                    <i style="color: green" class="fa fa-check-circle"></i>&nbsp<strong> Allocated</strong><br />
                                                                @else
                                                                    <i style="color: orange" class="fa fa-circle-o"></i>&nbsp<strong> Allocated</strong><br />
                                                                @endif
                                                                @if ($registrationStatus[$activity->activityID]['confirmed'] == 1)   
                                                                    <i style="color: green" class="fa fa-check-circle"></i>&nbsp<strong> Confirmed</strong><br />
                                                                @else
                                                                    <i style="color: orange" class="fa fa-circle-o"></i>&nbsp<strong> Confirmed</strong><br />
                                                                @endif
                                                            @endif
                                                        @else
                                                            <i style="color: orange" class="fa fa-circle-o"></i>&nbsp<strong> Applied</strong><br />
                                                            <i style="color: orange" class="fa fa-circle-o"></i>&nbsp<strong> Allocated</strong><br />
                                                            <i style="color: orange" class="fa fa-circle-o"></i>&nbsp<strong> Confirmed</strong><br />                                                                    
                                                        @endif  
                                                    </td>
                                                      
                                                    @if(strtotime($event->eventEndDate) >= strtotime(date('Y-m-d')))
                                                    <td>     
                                                        @if(!array_key_exists($activity->activityID,$registrationStatus))        
                                                            <a data-toggle="modal" href="#registration_modal_{{$activity->activityID}}" class="btn btn-primary">Register</a>
                                                        @endif
                                                        @if(Auth::user()->userType <= 2)
                                                            <a class="btn btn-small btn-info" href="{{ URL::to('volunteer/find/' . $activity->activityID) }}">Find Volunteers</a> 
                                                        @else
                                                            @if(array_key_exists($activity->activityID,$registrationStatus))   
                                                                @if($registrationStatus[$activity->activityID]['rejected'] == 1)
                                                                <div class="alert alert-dismissable alert-danger text-center"><strong>Rejected</strong></div>
                                                                @elseif($registrationStatus[$activity->activityID]['declined'] == 1)
                                                                <div class="alert alert-dismissable alert-warning text-center"><strong>Declined</strong></div>
                                                                @else                                                            
                                                                <div class="alert alert-dismissable alert-success text-center"><strong>Applied</strong></div>
                                                                @endif 
                                                            @endif
                                                        @endif       
                                                    </td>
                                                    @endif
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div id="accordioninpanel{{$activity->activityID}}" class="accordion-group">
                                        <div class="accordion-item">
                                            <a class="accordion-title collapsed" data-toggle="collapse" data-parent="#accordioninpanel{{$activity->activityID}}" href="#collapseinOne{{$activity->activityID}}">
                                                <h4>Roles <span class="fa fa-caret-down"></span></h4>
                                            </a>
                                            <div id="collapseinOne{{$activity->activityID}}" class="collapse" style="height: 0px;">
                                               <div class="table-responsive">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>Role Name</th>
                                                                <th>Role Description</th>
                                                                <th>Activity Name</th>
                                                                <th>Activity Date</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($roles as $role)
                                                            @if($role->activityID == $activity->activityID)
	              								            <tr>
                                                              <td>{{ $role->roleName }}</td>
                                                              <td>{{ $role->roleDescription }}</td>
                                                              <td>{{ $role->activityName }}</td>
                                                              <td>{{ date('d/m/Y', strtotime($role->activityDate)) }}</td>
                                                            </tr>
                                                            @endif
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>                                         
            
            <!-- Registration Modals -->
            @foreach ($activities as $activity)
            <div class="modal fade modals" id="registration_modal_{{$activity->activityID}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Register for an Activity</h4>
                        </div>
                            {{ Form::open(array('url' => 'volunteer/register', 'method' => 'POST', 'id' => 'volunteer'.$activity->activityID, 'class' => 'form-horizontal', 'style' => 'margin-bottom:0px !important')) }}                                                                  
                            {{ Form::hidden('activityID', $activity->activityID) }}     
                        <div class="modal-body">
                            <div class="form-group">
                                <h4 class="col-sm-offset-3">Activity is from {{ date('h:i A', strtotime($activity->activityStartDateTime)) }} to {{  date('h:i A', strtotime($activity->activityEndDateTime)) }}</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">What time are you available from?</label>
                                <div class="col-sm-6">
                                    {{ Form::text('availableFrom', date('H:i', strtotime($activity->activityStartDateTime)), array('class' => 'form-control', 'name' => 'availableFrom', 'id' => 'availableFrom', 'required' => 'required', 'type' => 'text')) }}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">What time are you available to?</label>
                                <div class="col-sm-6">
                                    {{ Form::text('availableTo', date('H:i', strtotime($activity->activityEndDateTime)), array('class' => 'form-control', 'name' => 'availableTo', 'id' => 'availableTo', 'required' => 'required', 'type' => 'text')) }}
                                </div>
                            </div>
                            <h4>Notes (Optional)</h4>
                            {{ Form::textarea('notes', '', array('id' => 'notes', 'class' => 'form-control autosize', 'name' => 'notes', 'rows' => '2')) }}
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            {{ Form::submit('Register', array('class'=>'finish btn-success btn')) }}  
                            {{ Form::close() }} 
                        </div>
                    </div>
                </div>
            </div>
            @endforeach


        </div> <!-- container -->
    </div> <!--wrap -->
</div> <!-- page-content -->            
  
@include('SEM.footer')