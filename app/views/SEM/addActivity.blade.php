@include('SEM.header')
@include('SEM.header-nav')

<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <ol class="breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li>Event</li>
                <li class="active">Create</li>
            </ol>

            <h1>Add Activity to An Event</h1>
            
        </div>
        <div class="container">


<div class="row">
    <div class="col-md-12">

        <div class="panel panel-info">
            <div class="panel-heading">
                <h4>Add Activity to An Event!</h4>
            </div>
            <div class="panel-body">
                {{ Form::open(array('url' => 'activity/create', 'method' => 'GET', 'id' => 'createEvent', 'class' => 'form-horizontal', 'style' => 'margin-bottom:0px !important')) }}    
                    <fieldset title="Step 1">
                        <legend>Add Activity</legend>
                        <div class="form-group">
                            <label for="activity_name" class="col-md-3 control-label">Activity Name</label>
                             <div class="col-sm-6">
                            {{ Form::text('activity_name', '', array('class' => 'form-control', 'name' => 'activity_name', 'placeholder' => 'Activity Name', 'required' => 'required', 'minlength' => '5', 'type' => 'text')) }}
                            </div>
                            
                            
                            {{ Form::hidden('eventID', $eventID) }}
                            {{ Form::hidden('eventStartDate', $eventDate) }}
                        </div>
                        
                        <div class="form-group">
                            <label for="activity_description" class="col-md-3 control-label">Activity Description</label>
                            <div class="col-md-6">
                                {{ Form::textarea('activity_description', '', array('id' => 'activity_description', 'class' => 'form-control autosize', 'name' => 'activity_description', 'required' => 'required', 'rows' => '2')) }}
                            </div>
                            
                        </div>
                        
                        <div class="form-group">
                            <label for="activity_location" class="col-md-3 control-label">Activity Location</label>
                             <div class="col-sm-6">
                            {{ Form::text('activity_location', '', array('class' => 'form-control', 'name' => 'activity_location', 'placeholder' => 'Activity Location', 'minlength' => '5', 'type' => 'text')) }}
                            </div>
                        </div>     

                        <div class="form-group">
                            <label for="activity_description" class="col-md-3 control-label">Paid/Unpaid</label>
                            <div class="col-md-6">
                                {{ Form::radio('activity_isPaid[]', True, False) }} 
                                {{ Form::label('paid','Paid') }} &nbsp&nbsp
                                {{ Form::radio('activity_isPaid[]', False, True) }}
                                {{ Form::label('unpaid','Unpaid') }}
                            </div>                            
                        </div>        
                                                                  
                        <div class="form-group">
                            <label for="activity_description" class="col-md-3 control-label">Number of Positions</label>
                            <div class="col-md-6">
                                {{ Form::text('activity_number_positions', '', array('id' => 'activity_number_positions', 'class' => 'form-control', 'name' => 'activity_number_positions')) }}
                            </div>                            
                        </div>    
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Activity Date</label>
                            <div class="col-sm-6">
                                {{ Form::text('activity_date', '', array('class' => 'form-control', 'name' => 'activity_date', 'id' => 'activity_date', 'type' => 'text')) }}
                            </div>
                            {{ $errors->first('endDate') }}
                            {{ $errors->first('startDate') }}
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Activity Start Time</label>
                            <div class="col-sm-6">
                                {{ Form::text('activity_start_time', '', array('class' => 'form-control', 'name' => 'activity_start_time', 'id' => 'activity_start_time', 'type' => 'text')) }}
                            </div>
                            {{ $errors->first('time') }}
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Activity End Time</label>
                            <div class="col-sm-6">
                                {{ Form::text('activity_end_time', '', array('class' => 'form-control', 'name' => 'activity_end_time', 'id' => 'activity_end_time', 'type' => 'text')) }}
                            </div>
                            {{ $errors->first('time') }}
                        </div>
                        
                    </fieldset>
                    {{ Form::submit('Create!', array('class'=>'finish btn-success btn')) }}
                    {{ Form::close() }}
                </form>
            </div>
        </div>


    </div>
</div>


</div> <!-- container -->
</div> <!--wrap -->
</div> <!-- page-content -->


@include('SEM.footer')