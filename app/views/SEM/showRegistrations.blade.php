@include('SEM.header')
@include('SEM.header-nav')

<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <ol class="breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li>Volunteer</li>
                <li>Registrations</li>
                <li class='active'>View</li>
            </ol>

            @if(isset($volunteerID))
                <h1>Activity Registrations</h1>
            @else           
                <h1>Volunteer Registrations</h1>
            @endif
            <?PHP $segment = Request::segment(2);  ?>
        </div>
        <div class="container">
            <div class="row">
              <div class="col-md-12">
                    <div class="panel panel-sky">
                        <div class="panel-heading">
                            <h4>Pending Registrations</h4>
                        </div>
                        <div class="panel-body collapse in">
                            @if (Session::has('message'))
                            <div class="alert alert-success">{{ Session::get('message') }}</div>
                            @endif
                            @if (Session::has('reject'))
                            <div class="alert alert-warning">{{ Session::get('reject') }}</div>
                            @endif
                            @if (Session::has('delete'))
                            <div class="alert alert-danger">{{ Session::get('delete') }}</div>
                            @endif
                            
                            <div class="table-responsive">
                                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables" id="example">
                                    <thead>
                                        <tr>
                                            <th>Event Name</th>
                                            <th>Activity Name</th>
                                            <th>Applicant Name</th>
                                            <th>Registration Date</th>
                                            <th>Profile</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($registrations as $registration)
                                        <tr>
                                            <td>{{ $registration->eventName }}</td>
                                            <td>{{ $registration->activityName }}</td>
                                            <td>{{ $registration->userFirstName . " " . $registration->userLastName }}</td>   
                                            <td>{{ date('F j, Y', strtotime($registration->updated_at)) }}</td>
                                            <td><a class="btn btn-info" href="{{ URL::to('volunteer/profile/' . $registration->volunteerID )}}">View Profile</a></td>                                   

                                            <td width="15%"> 
                                                @if(isset($volunteerID) && (Auth::user()->id == $volunteerID))
                                                    @if($registration->rejected == 1)
                                                        <div class="alert alert-dismissable alert-danger">
								                            Unfortunately your application was rejected by the event manager.
							                            </div>
                                                    @else        
                                                        <a class="btn btn-small btn-warning" href="{{ URL::to('volunteer/registration/edit/' . $registration->registrationID ) }}">Edit</a>
                                                        <a data-toggle="modal" href="#cancel_modal_{{$registration->registrationID}}" class="btn btn-small btn-danger">Cancel</a>                                                         
                                                    @endif
                                                @endif
                                                @if((Auth::user()->userType <= 2) && !isset($volunteerID))  
                                                    @if(!isset($registrationsCheck[$registration->activityID]))        
                                                    {{ Form::open(array('url' => 'volunteer/reject', 'method' => 'POST', 'id' => 'volunteer', 'class' => 'form-horizontal', 'style' => 'margin-bottom:0px !important')) }} 
                                                        {{ Form::hidden('registrationID', $registration->registrationID) }}
                                                        <a class="btn btn-small btn-success" href="{{ URL::to('volunteer/findRoles/R' . $registration->registrationID) }}">Allocate</a>    
                                                        {{ Form::submit('Reject', array('class'=>'finish btn-danger btn')) }}     
                                                    {{ Form::close() }}
                                                    @else
                                                    {{ Form::open(array('url' => 'volunteer/reject', 'method' => 'POST', 'id' => 'volunteer', 'class' => 'form-horizontal', 'style' => 'margin-bottom:0px !important')) }} 
                                                        {{ Form::hidden('registrationID', $registration->registrationID) }}
                                                        <a data-toggle="modal" class="btn btn-small btn-success" href="#allocate_modal_{{$registration->registrationID}}">Allocate</a>
                                                        {{ Form::submit('Reject', array('class'=>'finish btn-danger btn')) }}     
                                                    {{ Form::close() }}
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
   
                        </div>
                    </div>
                </div>
            </div>
                        
            <!-- Cancel Modals -->
            @if(isset($volunteerID) && (Auth::user()->id == $volunteerID))
            @foreach ($registrations as $registration)
            <div class="modal fade modals" id="cancel_modal_{{$registration->registrationID}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3 class="modal-title">Cancel Registration</h3>
                        </div>
                        <div class="modal-body">
                            <h4>Are you sure you want to cancel your registration?</h4>
                        </div>
                        <div class="modal-footer">
                            {{ Form::open(array('url' => 'volunteer/registration/delete', 'method' => 'POST', 'id' => 'cancel', 'class' => 'form-horizontal', 'style' => 'margin-bottom:0px !important')) }}                                                                  
                            {{ Form::hidden('registrationID', $registration->registrationID) }}  
                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                            {{ Form::submit('Yes', array('class'=>'finish btn-danger btn')) }}  
                            {{ Form::close() }}     
                        </div>
                    </div>
                </div>
            </div> 
            @endforeach
            @endif

            @if((Auth::user()->userType <= 2) && !isset($volunteerID))  
            @foreach ($registrations as $registration)
            @if(isset($registrationsCheck[$registration->activityID]))
            <div class="modal fade modals" id="allocate_modal_{{$registration->registrationID}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3 class="modal-title">Confirmation</h3>
                        </div>
                        @if($registrationsCheck[$registration->activityID] == 'A')
                            <div class="modal-body">
                                <h5>This activity is already full. Please either remove some volunteers from the activity or increase the required number of volunteers for the activity.</h5>
                            </div>
                            <div class="modal-footer">
                                 {{ Form::open(array('url' => 'volunteer/reject', 'method' => 'POST', 'id' => 'volunteer', 'class' => 'form-horizontal', 'style' => 'margin-bottom:0px !important')) }} 
                                    {{ Form::hidden('registrationID', $registration->registrationID) }}
                                    {{ Form::submit('Remove Registration', array('class'=>'finish btn-danger btn')) }}    
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button> 
                                {{ Form::close() }}
                            </div>   
                        @endif
                        @if($registrationsCheck[$registration->activityID] == 'R')
                            <div class="modal-body">
                                <h5>There are more people registerd for this event than positions available.<br /> Are you sure you want to allocate this volunteer?</h5>
                            </div>
                            <div class="modal-footer"> 
                                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                <a class="btn btn-small btn-success" href="{{ URL::to('volunteer/findRoles/R' . $registration->registrationID) }}">Allocate</a>    
                            </div>   
                        @endif
                    </div>
                </div>
            </div> 
            @endif
            @endforeach
            @endif

        </div> <!-- container -->
    </div> <!--wrap -->
</div> <!-- page-content -->

@include('SEM.footer')