@include('SEM.header')
@include('SEM.header-nav')

<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <ol class="breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li>Roles</li>
                <li class='active'>Show</li>
            </ol>

            @if($type=='R' || $type=='A')
                <h1>Allocate to Role</h1>
            @else           
                <h1>Show Roles</h1>
            @endif
            <?PHP $segment = Request::segment(2);  ?>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-sky">
                        <div class="panel-heading">
                            <h4>All Roles</h4>
                            
                            @if($type=='A' || is_numeric($type))
                            <div class="pull-right"><a class="btn btn-small btn-success" href="{{ URL::to('role/add/' . $segment) }}">Add Role</a></div>
                            @endif
                        </div>
                        <div class="panel-body collapse in">
                            @if (Session::has('message'))
                            <div class="alert alert-success">{{ Session::get('message') }}</div>
                            @endif
                            @if (Session::has('archived'))
                            <div class="alert alert-danger">{{ Session::get('archived') }}</div>
                            @endif
                            
                            <div class="table-responsive">
                                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables" id="example">
                                    <thead>
                                        <tr>
                                            <th>Role Name</th>
                                            <th>Description</th>

                                            <th>Modify</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($roles as $role)
                                    <tr>
                                        <td>{{ $role->roleName }}</td>
                                        <td>{{ $role->roleDescription }}</td>

                                        <td class="text-center" width="15%">   
                                            @if($type=='R')
                                            {{ Form::open(array('url' => 'volunteer/allocate')) }}
                                                <!-- Check security on this -->
                                                {{ Form::hidden('activityRoleID', $role->activityRoleID) }}
                                                {{ Form::hidden('activityID', $role->activityID) }}
                                                {{ Form::hidden('registrationID', $role->registrationID) }}
                                                {{ Form::submit('Allocate', array('class' => 'btn btn-success', 'style' => "width: 100%")) }}
                                            {{ Form::close() }}  
                                            @elseif($type=='A')
                                            {{ Form::open(array('url' => 'volunteer/register/allocate')) }}
                                                <!-- Check security on this -->
                                                {{ Form::hidden('activityRoleID', $role->activityRoleID) }}
                                                {{ Form::hidden('activityID', $role->activityID) }}
                                                {{ Form::hidden('volunteerID', $volunteerID) }}
                                                {{ Form::submit('Allocate', array('class' => 'btn btn-success', 'style' => "width: 100%")) }}
                                            {{ Form::close() }}  
                                            @else                                               
                                            {{ Form::open(array('url' => 'role/' . $role->roleID . '/' . $segment)) }}
                                                {{ Form::hidden('_method', 'DELETE') }}
                                                <a class="btn btn-small btn-warning" style="width: 100%" href="{{ URL::to('role/edit/' . $role->activityRoleID) }}">Edit Role</a>
                                                {{ Form::submit('Cancel', array('class' => 'btn btn-danger', 'style' => "width: 100%")) }}
                                            {{ Form::close() }}  
                                            @endif
                                        </td>
                                    </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        
        
@if (Session::has('noMorePositions'))    
<div class="modal" id='myModal'>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Max Number of Positions Already Reached!</h4>
      </div>
      <div class="modal-body">
        <p>There are positions left for this activity. Please allocate to another!</p>
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>  
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endif
        
        <!-- container -->
    </div>
    <!--wrap -->
</div>
<!-- page-content -->

@include('SEM.footer')