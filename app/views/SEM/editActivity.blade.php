@include('SEM.header')
@include('SEM.header-nav')

<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <ol class="breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li>Activities</li>
                <li class="active">Edit</li>
            </ol>

            <h1>Edit an Activity</h1>
            
        </div>
        <div class="container">


<div class="row">
    <div class="col-md-12">

        <div class="panel panel-info">
            <div class="panel-heading">
                <h4> {{ $eventName->eventName }} | {{ $activity->activityName }} </h4>
            </div>
            <div class="panel-body">
                {{ Form::open(array('url' => 'activity/update/'.$activity->activityID, 'method' => 'GET', 'id' => 'updateActivity', 'class' => 'form-horizontal', 'style' => 'margin-bottom:0px !important')) }}    
                   
                    <legend>Update Activity</legend>
                    <div class="form-group">
                        <label for="activity_name" class="col-md-3 control-label">Activity Name</label>
                         <div class="col-sm-6">
                        {{ Form::text('activity_name', $activity->activityName, array('class' => 'form-control', 'name' => 'activity_name', 'placeholder' => 'Activity Name', 'required' => 'required', 'minlength' => '5', 'type' => 'text')) }}
                        </div>
                        {{ $errors->first('activity_name') }}


                        {{ Form::hidden('eventID', '') }}
                    </div>

                    <div class="form-group">
                        <label for="activity_description" class="col-md-3 control-label">Activity Description</label>
                        <div class="col-md-6">
                            {{ Form::textarea('activity_description', $activity->activityDescription, array('id' => 'activity_description', 'class' => 'form-control', 'name' => 'activity_description', 'required' => 'required', 'rows' => '5')) }}
                        </div>
                        {{ $errors->first('activity_description') }}

                    </div>
                
                    <div class="form-group">
                        <label for="activity_location" class="col-md-3 control-label">Activity Location</label>
                            <div class="col-sm-6">
                        {{ Form::text('activity_location', $activity->activityLocation, array('class' => 'form-control', 'name' => 'activity_location', 'placeholder' => 'Activity Location', 'minlength' => '5', 'type' => 'text')) }}
                        </div>
                    {{ $errors->first('activity_location') }}
                    </div>                

                    <div class="form-group">
                        <label for="activity_description" class="col-md-3 control-label">Paid/Unpaid</label>
                        <div class="col-md-6">
                            {{ Form::radio('activity_isPaid[]', True, ($activity->isPaid == 1)) }} 
                            {{ Form::label('paid','Paid') }} &nbsp&nbsp
                            {{ Form::radio('activity_isPaid[]', False, ($activity->isPaid == 0)) }}
                            {{ Form::label('unpaid','Unpaid') }}
                        </div>                            
                    </div>        
                                                                  
                    <div class="form-group">
                        <label for="activity_description" class="col-md-3 control-label">Number of Positions</label>
                        <div class="col-md-6">
                            {{ Form::text('activity_number_positions', $activity->positionsAvailable, array('id' => 'activity_number_positions', 'class' => 'form-control', 'name' => 'activity_number_positions')) }}
                        </div> 
                        {{ $errors->first('activity_number_positions') }}
                    </div>                        

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Activity Date</label>
                        <div class="col-sm-6">
                            {{ Form::text('activity_date', date('d/m/Y',strtotime($activity->activityDate)), array('class' => 'form-control', 'name' => 'activity_date', 'id' => 'activity_date', 'type' => 'text')) }}
                        </div>
                        {{ $errors->first('endDate') }}
                        {{ $errors->first('startDate') }}
                        {{ $errors->first('activity_date') }}
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Activity Start Time</label>
                        <div class="col-sm-6">
                            {{ Form::text('activity_start_time', date('H:i', strtotime($activity->activityStartDateTime)), array('class' => 'form-control', 'name' => 'activity_start_time', 'id' => 'activity_start_time', 'type' => 'text')) }}
                        </div>
                        {{ $errors->first('time') }}
                        {{ $errors->first('activity_start_time') }}
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Activity End Time</label>
                        <div class="col-sm-6">
                            {{ Form::text('activity_end_time', date('H:i', strtotime($activity->activityEndDateTime)), array('class' => 'form-control', 'name' => 'activity_end_time', 'id' => 'activity_end_time', 'type' => 'text')) }}
                        </div>
                        {{ $errors->first('time') }}
                        {{ $errors->first('activity_end_time') }}
                    </div>
            </div>
        
            <div class="panel-footer">
                <div class="pull-right">
                    <a class="btn btn-small btn-info" href="{{ URL::to('event/'. $activity->eventID .'/showActivity') }}">Cancel</a>
                    {{ Form::submit('Update', array('class'=>'finish btn-success btn')) }}
                    {{ Form::close() }}
                </div>
            </div>
                
            </form>
        </div>


    </div>
</div>


</div> <!-- container -->
</div> <!--wrap -->
</div> <!-- page-content -->


@include('SEM.footer')