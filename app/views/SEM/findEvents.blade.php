@include('SEM.header')
@include('SEM.header-nav')

<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <ol class="breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li>Events</li>
                <li class='active'>Find</li>
            </ol>

            <h1>Find Events</h1>
        </div>
        <div class="container">
            <div class="row">
              <div class="col-md-12">
                    <div class="panel panel-sky">
                        <div class="panel-heading">
                            <h4>Events</h4>
                        </div>
                        <div class="panel-body collapse in">
                            @if (Session::has('find'))
                            <div class="alert alert-success">{{ Session::get('find') }}</div>
                            @endif
                            @if (Session::has('message'))
                            <div class="alert alert-success">{{ Session::get('message') }}</div>
                            @endif
                            @if (Session::has('error'))
                            <div class="alert alert-danger">{{ Session::get('error') }}</div>
                            @endif
                            @if (Session::has('archived'))
                            <div class="alert alert-danger">{{ Session::get('archived') }}</div>
                            @endif
                            
                            <div class="table-responsive">
                                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables" id="example">
                                    <thead>
                                        <tr>
                                            <th>Event Name</th>
                                            <th>Location</th>
                                            <th>Event Manager</th>
                                            <th>Start Date</th>
                                            <th>End Date </th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                          @foreach ($events as $event)
                                        <tr>
                                            <td>{{ $event->eventName }}</td>
                                            <td>{{ $event->eventLocation }}</td>

                                            <!--needs to be moved up into controller-->
                                            @if(!isset($event->userID))
                                            <td>None Assigned</td>
                                            @else
                                            <td>{{ $event->userID }}</td>
                                            @endif
                                            <td>{{ date('F j, Y', strtotime($event->eventStartDate)) }}</td>
                                            <td>{{ date('F j, Y', strtotime($event->eventEndDate)) }}</td>

                                            <td class="text-center" >
                                                <a class="btn btn-small btn-info" href="{{ URL::to('event/profile/' . $event->eventID) }}">View Event</a>                                    
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
   
                        </div>
                    </div>
                </div>
            </div>

        </div> <!-- container -->
    </div> <!--wrap -->
</div> <!-- page-content -->

@include('SEM.footer')