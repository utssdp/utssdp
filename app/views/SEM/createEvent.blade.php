@include('SEM.header')
@include('SEM.header-nav')

<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <ol class="breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li>Event</li>
                <li class="active">Create</li>
            </ol>

            <h1>Create An Event!</h1>
            
        </div>
        <div class="container">


<div class="row">
    <div class="col-md-12">

        <div class="panel panel-info">
            <div class="panel-heading">
                <h4>Create An Event</h4>
            </div>
            <div class="panel-body">
                {{ Form::open(array('url' => 'event/create', 'method' => 'GET', 'id' => 'createEvent', 'class' => 'form-horizontal', 'style' => 'margin-bottom:0px !important')) }}    
                    <fieldset title="Step 1">
                        <legend>Create Event</legend>
                        <div class="form-group">
                            <label for="event_name" class="col-md-3 control-label">Event Name</label>
                            <div class="col-md-6">
                                {{ Form::text('event_name', '', array('class' => 'form-control', 'name' => 'event_name', 'placeholder' => 'Event Name', 'required' => 'required', 'minlength' => '5', 'type' => 'text')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="event_location" class="col-md-3 control-label">Event Location</label>
                            <div class="col-md-6">
                            {{ Form::text('event_location', '', array('class' => 'form-control', 'name' => 'event_location', 'placeholder' => 'Event Location', 'minlength' => '5', 'type' => 'text')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="event_description" class="col-md-3 control-label">Event Description</label>
                            <div class="col-md-6">
                            {{ Form::textarea('event_description', '', array('class' => 'form-control', 'name' => 'event_description', 'placeholder' => 'Event Description', 'minlength' => '5', 'type' => 'text')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Event Manager</label>
                            <div class="col-sm-6">
                                {{ Form::select('event_manager', $eventManagers, null, array('style' => 'width:100%', 'class' => 'populate', 'id' => 'event_manager')) }}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Event Start &amp; End Date/Time</label>
                            <div class="col-sm-6">
                                {{ Form::text('eventStartEndDate', '', array('class' => 'form-control', 'name' => 'eventStartEndDate', 'id' => 'eventStartEndDate', 'type' => 'text')) }}
                            </div>
                        </div>               

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Event Start Time</label>
                            <div class="col-sm-6">
                                {{ Form::text('event_start_time', '', array('class' => 'form-control', 'name' => 'event_start_time', 'id' => 'event_start_time', 'type' => 'text')) }}
                            </div>
                        </div>

                    </fieldset>
                    <fieldset title="Step 2">
                        <legend>Create Activity</legend>
                        <div class="form-group">
                            <label for="activity_name" class="col-md-3 control-label">Activity Name</label>
                             <div class="col-sm-6">
                            {{ Form::text('activity_name', '', array('class' => 'form-control', 'name' => 'activity_name', 'placeholder' => 'Activity Name', 'required' => 'required', 'minlength' => '5', 'type' => 'text')) }}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="activity_description" class="col-md-3 control-label">Activity Description</label>
                            <div class="col-md-6">
                                {{ Form::textarea('activity_description', '', array('id' => 'activity_description', 'class' => 'form-control autosize', 'name' => 'activity_description', 'required' => 'required', 'rows' => '2')) }}
                            </div>                            
                        </div>
                                                                  
                        <div class="form-group">
                            <label for="activity_description" class="col-md-3 control-label">Activity Location</label>
                            <div class="col-md-6">
                                {{ Form::text('activity_location', '', array('id' => 'activity_location', 'class' => 'form-control', 'name' => 'activity_location')) }}
                            </div>                            
                        </div>         

                        <div class="form-group">
                            <label for="activity_description" class="col-md-3 control-label">Paid/Unpaid</label>
                            <div class="col-md-6">
                                {{ Form::radio('activity_isPaid[]', True, False) }} 
                                {{ Form::label('paid','Paid') }} &nbsp&nbsp
                                {{ Form::radio('activity_isPaid[]', False, True) }}
                                {{ Form::label('unpaid','Unpaid') }}
                            </div>                            
                        </div>        
                                                                  
                        <div class="form-group">
                            <label for="activity_description" class="col-md-3 control-label">Number of Positions</label>
                            <div class="col-md-6">
                                {{ Form::text('activity_number_positions', '', array('id' => 'activity_number_positions', 'class' => 'form-control', 'name' => 'activity_number_positions')) }}
                            </div>                            
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Activity Date</label>
                            <div class="col-sm-6">
                                {{ Form::text('activity_date', '', array('class' => 'form-control', 'name' => 'activity_date', 'id' => 'activity_date','type' => 'text')) }}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Activity Start Time</label>
                            <div class="col-sm-6">
                                {{ Form::text('activity_start_time', '', array('class' => 'form-control', 'name' => 'activity_start_time', 'id' => 'activity_start_time', 'type' => 'text')) }}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Activity End Time</label>
                            <div class="col-sm-6">
                                {{ Form::text('activity_end_time', '', array('class' => 'form-control', 'name' => 'activity_end_time', 'id' => 'activity_end_time', 'type' => 'text')) }}
                            </div>
                        </div>
                        
                    </fieldset>
                    {{ Form::submit('Create Event', array('class'=>'finish btn-success btn')) }}
                    {{ Form::close() }}        
            </div>
        </div>


    </div>
</div>


</div> <!-- container -->
</div> <!--wrap -->
</div> <!-- page-content -->


@include('SEM.footer')