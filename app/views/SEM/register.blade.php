@include('SEM.header-focused')

<div class="verticalcenter">
	<div class="panel panel-primary">
		<div class="panel-body">
			<h4 class="text-center" style="margin-bottom: 25px;">Sign Up</h4>
            
				{{ Form::open(array('url' => 'register', 'class' => 'form-horizontal', 'style' => 'margin-bottom:0px !important')) }}
						
                        <div class="form-group">
							<div class="col-sm-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
									{{ Form::text('email', Input::old('email'), array('placeholder' => 'awesome@awesome.com', 'class' => 'form-control')) }}
								</div>
                                {{ $errors->first('email') }}
							</div>
						</div>
                    
						<div class="form-group">
							<div class="col-sm-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock"></i></span>
									{{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) }}
								</div>
							</div>
						</div>
                    
						<div class="form-group">
							<div class="col-sm-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock"></i></span>
									{{ Form::password('password_confirmation', array('class' => 'form-control', 'placeholder' => 'Re-Type Password')) }}
								</div>
                                {{ $errors->first('password') }}
							</div>
						</div>
                    
                    
                        <div class="form-group">
							<div class="col-sm-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock"></i></span>
									{{ Form::text('firstName', '', array('class' => 'form-control', 'placeholder' => 'First Name')) }}
								</div>
							</div>
						</div>
                        
                        <div class="form-group">
							<div class="col-sm-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    {{ Form::text('lastName', '', array('class' => 'form-control', 'placeholder' => 'Last Name')) }}
								</div>
							</div>
						</div>
            
                        <div class="form-group">
							<div class="col-sm-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    {{ Form::text('utsID', '', array('class' => 'form-control', 'placeholder' => 'UTS ID#')) }}
								</div>
                                {{ $errors->first('utsID') }}
							</div>
						</div>
            
                        <div class="form-group">
							<div class="col-sm-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock"></i></span>
									{{ Form::text('phoneNumber', '', array('class' => 'form-control', 'placeholder' => 'Enter Phone Number (XX) XXXX XXXX or +61XXX XXX XXX')) }}
								</div>
							</div>
						</div>
                    
						<div class="block" style="text-align:center">
							<input type="checkbox" style="margin-right:5px">
							<span>
								I accept the <a href="#">User Agreement</a>
							</span>
						</div>
						
					  
					
		</div>
		<div class="panel-footer">
			<div class="pull-left">
				<a href="extras-login.php" class="btn btn-default">Cancel</a>
			</div>
			<div class="pull-right">
				{{ Form::submit('Register!', array('class'=>'btn btn-primary')) }}
                {{ Form::close() }}  
			</div>
		</div>
	</div>
 </div>
      
</body>
</html>