<?php include "header.php" ?>

<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <ol class="breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li>Advanced Forms</li>
                <li class="active">Wizards</li>
            </ol>

            <h1>Wizards</h1>
            <div class="options">
                <div class="btn-toolbar">
                    <div class="btn-group hidden-xs">
                        <a href='#' class="btn btn-default dropdown-toggle" data-toggle='dropdown'><i class="fa fa-cloud-download"></i><span class="hidden-sm"> Export as  </span><span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Text File (*.txt)</a></li>
                            <li><a href="#">Excel File (*.xlsx)</a></li>
                            <li><a href="#">PDF File (*.pdf)</a></li>
                        </ul>
                    </div>
                    <a href="#" class="btn btn-default"><i class="fa fa-cog"></i></a>
                </div>
            </div>
        </div>
        <div class="container">



<div class="panel panel-info">
	<div class="panel-heading">
		<h4>Event Wizard</h4>
	</div>
	<div class="panel-body">
		<p>Please input all the required dat</p>
		<form action="#" id="wizard" class="form-horizontal">
			<fieldset title="Step 1">
				<legend>Event Description</legend>
				<div class="form-group">
					<label for="fieldname" class="col-md-3 control-label">Event Name</label>
					<div class="col-md-6">
						<input id="eventName" class="form-control" name="eventName" minlength="4" placeholder="Enter Event Name" type="text" required>
					</div>
				</div>
				<div class="form-group">
					<label for="fieldname" class="col-md-3 control-label">Description</label>
					<div class="col-md-6">
						<input id="eventDescription" class="form-control" name="eventDescription" minlength="4" type="text" required>
					</div>
				</div>   
				<div class="form-group">
                    <label class="col-sm-3 control-label">Date Range</label>
                    <div class="col-sm-6">
                    <div class="input-daterange input-group" id="datepicker3">
                        <input type="text" class="input-small form-control" id="eventStartDate" name="eventStartDate" required/>
                        <span class="input-group-addon">to</span>
                        <input type="text" class="input-small form-control" id="eventEndDate" name="eventEndDate" required/>
                    </div>
                    </div>
                </div>               
				<div class="form-group">
					<label for="fieldname" class="col-md-3 control-label">Location</label>
					<div class="col-md-6"><input id="eventLocation" class="form-control" placeholder="Please enter the full address including the city, state and postcode"type="text" name="eventLocation" required></div>
				</div>
			</fieldset>
			<fieldset title="Step 2">
				<legend>Event Activities</legend>
					<div class="panel-group panel-info" id="accordion">
						<div class="panel panel-default">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
								<div class="panel-heading"><h4>Activity 1</h4></div>
							</a>
							<div id="collapse1" class="panel-collapse collapse in">
								<div class="panel-body">
                                <div class="form-group">
					               <label for="fieldname" class="col-md-3 control-label">Activity Name</label>
					               <div class="col-md-6">
						                  <input id="activityName" class="form-control" name="activityName" minlength="4" placeholder="Enter Activity Name" type="text" required>
					               </div>
				                </div>
                                <div class="form-group">
					               <label for="fieldname" class="col-md-3 control-label">Description</label>
					               <div class="col-md-6">
						                  <input id="activityDescription" class="form-control" name="activityDescription" placeholder="Enter activity description" minlength="4" type="text" required>
					               </div>
				                </div>   
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Date</label>
                                    <div class="col-sm-6">
                                            <input type="text" class="form-control" placeholder="Enter date" id="datepicker">
                                    </div>
                                </div>
								</div>
							</div>
						</div>
					</div>
                <button id="addActivity" class="btn btn-success btn-label"><i class="fa fa-plus"></i> Add Activity</button>
			</fieldset>
			<fieldset title="Step 3">
				<legend>Event Roles</legend>
                    <div class="panel-group panel-info" id="accordion">
						<div class="panel panel-default">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
								<div class="panel-heading"><h4>Activity 1</h4></div>
							</a>
							<div id="collapse1" class="panel-collapse collapse in">
								<div class="panel-body">
                                <div class="form-group">
					               <label for="fieldname" class="col-md-3 control-label">Role Name</label>
					               <div class="col-md-6">
						                  <input id="roleName" class="form-control" name="roleName" minlength="4" placeholder="Enter Activity Name" type="text" required>
					               </div>
				                </div>
                                <div class="form-group">
					               <label for="fieldname" class="col-md-3 control-label">Description</label>
					               <div class="col-md-6">
						                  <input id="roleDescription" class="form-control" name="roleDescription" placeholder="Enter activity description" minlength="4" type="text" required>
					               </div>
				                </div>  
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Which activity does this role take place?</label>
                                    <div class="col-sm-6">
                                        <select class="form-control" id="source">
                                            <option value="NSW">Activity</option>
                                            <option value="QLD">Options</option>
                                            <option value="SA">Here</option>
                                        </select>
                           </div>
                        </div>  
								</div>
							</div>
						</div>
					</div>
                <button id="addRole" class="btn btn-success btn-label"><i class="fa fa-plus"></i> Add Role</button>				
			</fieldset>
			<input type="submit" class="finish btn-success btn" value="Submit" />
		</form>
	</div>
</div>


    </div>
</div>

</div> <!-- container -->
</div> <!--wrap -->
</div> <!-- page-content -->

<?php include "footer.php" ?>