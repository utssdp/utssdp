@include('SEM.header')
@include('SEM.header-nav')

<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <ol class="breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li>Roles</li>
                <li class="active">Apply for roles</li>
            </ol>

            <h1>Apply for Roles</h1>

        <div class="container">
            <div class="row">
              <div class="col-md-12">
                    <div class="panel panel-sky">
                        <div class="panel-heading">
                            <h4>Roles Available</h4>
                        </div>
                        <div class="panel-body collapse in">
                            @if (Session::has('message'))
                            <div class="alert alert-success">{{ Session::get('message') }}</div>
                            @endif
                            @if (Session::has('archived'))
                            <div class="alert alert-danger">{{ Session::get('archived') }}</div>
                            @endif
                            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables" id="example">
                                <thead>
                                    <tr>
                                        <th>Role Name</th>
                                        <th>Role Description</th> <!-- Popup for this attribute as description will be long for most roles -->
                                        <th>Positions Available</th>
                                        <th>Pay Rate</th>
                                        <th>Activity Name</th> 
                                        <th>Event Name</th>
                                        <th>Duration</th> <!-- Put the times the activity will take place or could this be changed to timing but timing doesn't sound right -->
                                        <th>Apply</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <!--
                                    @foreach ($roles as role)
                                    <tr>
                                        <td>{{ $role->roleName }}</td>
                                        <td>{{ $role->roleDescription }}</td>
                                        <td>{{ $role->positionsAvailable }}</td>
                                        <td>{{ $role->activityRolePayRate }}</td>
                                    </tr>
                                -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div> <!-- container -->
    </div> <!--wrap -->
</div> <!-- page-content -->

@include('SEM.footer')