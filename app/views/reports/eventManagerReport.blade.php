<div id="page-content">
    <div id='wrap'>
        @if(count($records)==0)
        <h1>You have no events that you have managed!</h1>
        @endif
        @foreach($records as $record)
        <div id="page-heading">
            <h1>Event Report - {{ $record['event']->eventName }}</h1>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-info">
                        <div class="panel-body">
							<div class="row">
                                <div class="col-md-3">
                                    <div class="table-responsive">
                                        <table class="table table-condensed">
                                            <tbody>
                                                <tr>
                                                    <td>Name</td>
                                                    <td>{{$record['event']->eventName}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Description</td>
                                                    <td>{{ $record['event']->eventDescription }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Location</td>
                                                    <td>{{$record['event']->eventLocation}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Manager</td>
                                                    <td>
                                                        @if(!isset($record['event']->eventManager))
                                                            None Assigned
                                                        @else
                                                            {{ $record['event']->userFirstName . ' ' . $record['event']->userLastName }}
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Start Date</td>
                                                    <td>{{date('d/m/Y', strtotime($record['event']->eventStartDate))}}</td>
                                                </tr>
                                                <tr>
                                                    <td>End Date</td>
                                                    <td>{{date('d/m/Y', strtotime($record['event']->eventEndDate))}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Start Time</td>
                                                    <td>{{date('h:i A', strtotime($record['event']->eventStartTime))}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <hr>

                            @foreach ($record['activities'] as $activity)
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3>Activity - {{ $activity->activityName }}</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-condensed">
                                            <tbody>
                                                <tr>
                                                    <td>Description</td>
                                                    <td>{{$activity->activityDescription}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Paid</td>
                                                    <td>{{ ($activity->isPaid) ? 'Yes' : 'No' }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Date</td>
                                                    <td>{{date('d/m/Y', strtotime($activity->activityDate))}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Time</td>
                                                    <td>{{date('h:i A', strtotime($activity->activityStartDateTime)) . " - " . date('h:i A', strtotime($activity->activityEndDateTime))}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Positions</td>
                                                    <td>{{ (array_key_exists($activity->activityID,$record['positionsFilled'])) ? $record['positionsFilled'][$activity->activityID] : '0' }}  out of {{  $activity->positionsAvailable }} ({{ (array_key_exists($activity->activityID,$record['pending'])) ? $record['pending'][$activity->activityID] : '0' }} Pending)</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Volunteer</th>
                                                    <th>Role</th>
                                                    <th>Role Description</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($record['volunteers'] as $volunteer)
                                                @if($volunteer->activityID == $activity->activityID)
	              								<tr>
                                                    <td>{{ $volunteer->userFirstName . ' ' . $volunteer->userLastName }}</td>
                                                    <td>{{ isset($volunteer->roleName) ? $volunteer->roleName : 'N/A' }}</td>
                                                    <td>{{ isset($volunteer->roleDescription) ? $volunteer->roleDescription : 'N/A' }}</td>
                                                    <td>
                                                        @if ($volunteer->allocated == 1)   
                                                            <strong>Allocated</strong>
                                                        @elseif ($volunteer->confirmed == 1)  
                                                            <strong>Confirmed</strong>  
                                                        @else
                                                            <strong>Applied</strong> 
                                                        @endif     
                                                    </td>
                                                </tr>
                                                @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <br />
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>      

        </div> <!-- container -->
        @endforeach
    </div> <!--wrap -->
</div> <!-- page-content -->      