<style>
body{
    font-family: 'Source Sans Pro', 'Segoe UI', 'Droid Sans', Tahoma, Arial, sans-serif;
}

#page-heading h1 {
    margin: 0;
    padding: 20px 20px 20px 20px;
    float: left;
    line-height: 30px;
    font-weight: 300;
    font-size: 42px;
}

.table{
    width: 100%;
}

th{
    text-align: left;
}

.panel-info {
    
}

.list-unstyled {
    padding-left: 0;
    list-style: none;
}

.label {
    display: inline;
    padding: .2em .6em .3em;
    font-size: 75%;
    line-height: 1;
    color: #ffffff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: .25em;
    padding: .09em .6em .15em;
    font-family: 'Source Sans Pro', 'Segoe UI', 'Droid Sans', Tahoma, Arial, sans-serif;
    font-weight: 600;
    border-radius: 1px;
}

.label-primary {
    background-color: #4f8edc;
}

.label-info {
    background-color: #2bbce0;
}

</style>
<body>
    <div id="page-heading">
        <h1>Volunteering Report - Participation</h1>
    </div>
    <br />
    <div class="container">                          
	    <div class="row">
            <div class="col-md-6">
                <table class="table table-condensed">
                    <tbody>
                        <tr>
                            <td>Name</td>
                            <td>{{$userRecords[0]->userFirstName . ' ' . $userRecords[0]->userLastName}}</td>
                        </tr>
                        <tr>
                            <td>Gender</td>
                            <td>{{$userRecords[0]->userGender}}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>{{$userRecords[0]->email}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-6">
                <h3>Additional Info</h3>
                <table class="table table-condensed">
                    <tbody>
                        <tr>
                            <td>Skills</td>
                            <td>
                                @foreach($userSkills as $userSkill)
                                    <label class="label label-primary">{{$userSkill->skillName}}</label> &nbsp;
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <td>Training</td>
                            <td>
                                @foreach($userTrainings as $userTraining)
                                    <label class="label label-info">{{$userTraining->trainingName}}</label> &nbsp;
                                @endforeach
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <hr>
                            
        <div class="panel-heading">
            <h2>Participation</h2>
        </div>
        <div class="panel-body">
            @if($participations > 0)
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Event Name</th>
                            <th>Activity Name</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Availability</th>
                            <th>Paid</th>
                            <th>Notes</th>
                        </tr>
                    </thead>
                    <tbody>  
                        @foreach ($userRecords as $userRecord)   
                        @if($userRecord->confirmed == 1 && $userRecord->rejected == 0 && $userRecord->declined == 0)                                        
	              	    <tr>
                            <td>{{ $userRecord->eventName }} </td>
                            <td>{{ $userRecord->activityName }} </td>
                            <td>{{ date('d/m/Y', strtotime($userRecord->activityDate)) }} </td>
                            <td>{{ date('h:i A', strtotime($userRecord->activityStartDateTime)) . ' - ' . date('h:i A', strtotime($userRecord->activityEndDateTime)) }} </td>
                            <td>{{ date('h:i A', strtotime($userRecord->availableFrom)) . ' - ' . date('h:i A', strtotime($userRecord->availableTo)) }} </td>
                            <td>{{ ($userRecord->isPaid) ? 'Yes' : 'No' }} </td>
                            <td>{{ $userRecord->notes }} </td>
                        </tr>
                        @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
            @else
            <h4>You have not participated in any events or activities!</h4>
            @endif
        </div>


                        
    </div> <!-- container -->
</body>