<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
//Redirect to Login Page
Route::get('/',  array('as' => 'home', 'before'=>'auth', function()
{
	return View::make('SEM.index');
    
}));




//Route::post('/', array('uses'=>'RemindersController@postRemind'));
//Route::get('testme', array('uses'=>'UsersController@test'));

//Show Volunteer Form
Route::get('addVolunteer', array('as' => 'create-volunteer', 'before'=>'auth', function()
{
    return View::make('SEM.createVolunteer');
}));

//Register Volunteer
Route::post('addVolunteer', array('uses'=>'UsersController@doRegister', 'before'=>'auth'));


//Show Event Manager Form
Route::get('addEventManager', array('as' => 'create-event-manager', 'before'=>'auth', function()
{
    return View::make('SEM.createEventManager');
}));

//Show Users
Route::get('showUsers', array('as' => 'showUsers', 'uses'=>'UsersController@showAllUsers', 'before'=>'auth'));

//Register EventManager
Route::post('addEventManager', array('uses'=>'UsersController@doRegister', 'before'=>'auth'));

//Home Redirect
Route::get('home', array('before'=>'auth', function()
{
	return View::make('SEM.index');
}));

//Login Page
Route::get('login', array('uses'=>'UsersController@showLogin'));
//Register Page
//Route::get('register', array('uses'=>'UsersController@showRegister'));

//Login Action
Route::post('login', array('uses'=>'UsersController@doLogin'));

//Log a user out.
Route::get('logout', array('uses' => 'UsersController@doLogout'));


//Password Reset
//Route::controller('password', 'RemindersController');
//Route::resource('password', 'RemindersController', array(
//    'only' => array('index', 'store')
//)

Route::put('users/{id}/edit', array('uses'=>'UsersController@doUpdate', 'before'=>'auth'));
 
Route::get('password', array('uses'=>'RemindersController@index', 'as' => 'password'));
Route::post('password', array('uses'=>'RemindersController@store'));
Route::get('password/reset/{token}', array('uses'=>'RemindersController@show'));
Route::post('password/reset/{token}', array('uses'=>'RemindersController@update', 'as' => 'password.update'));
//Route::get('password/reset/{token}', 'RemindersController@show');
//Route::post('password/reset/{token}', 'RemindersController@update');

Route::resource('users', 'UsersController',
                array('only' => array('destroy', 'edit', 'before'=>'auth')));

Route::resource('event', 'EventController');
Route::resource('role', 'RoleController');
//Route::resource('activity', 'ActivityController');

##Event Routing
Route::get('showEvents', array('uses'=>'EventController@showEvents', 'as' => 'showEvents', 'before'=>'auth'));
Route::get('event/edit/{eventID}', array('uses'=>'EventController@editEvent', 'as' => 'editEvent', 'before'=>'auth'));
Route::get('event/update/{eventID}', array('uses'=>'EventController@update', 'before'=>'auth'));
Route::get('event/profile/{eventID}', array('uses'=>'EventController@getEvent', 'as' => 'eventProfile', 'before'=>'auth'));
Route::get('event/check/{eventID}', array('uses'=>'EventController@checkEvent', 'as' => 'eventProfile', 'before'=>'auth'));

##Activity Routing
Route::DELETE('activity/{id}/{eventID}', array('uses'=>'ActivityController@destroy', 'as' => 'activity.destroy', 'before'=>'auth'));

Route::get('activity/check/{id}/{eventID}', array('uses'=>'ActivityController@checkActivity', 'before'=>'auth'));

Route::get('activity/add/{eventID}', array('uses'=>'ActivityController@addActivity', 'before'=>'auth'));

Route::get('activity/create/', array('uses'=>'ActivityController@create', 'before'=>'auth'));
Route::get('event/{eventID}/showActivity', array('uses'=>'ActivityController@showActivity', 'before'=>'auth'));
Route::get('activity/edit/{activityID}', array('uses'=>'ActivityController@editActivity', 'before'=>'auth'));
Route::get('activity/update/{activityID}', array('uses'=>'ActivityController@update', 'before'=>'auth'));
Route::get('activity/{activityID}/showRoles', array('uses'=>'RoleController@showRoles', 'before'=>'auth'));
Route::get('activity/profile/{activityID}', array('uses'=>'ActivityController@getActivity', 'as' => 'activityProfile', 'before'=>'auth'));


#Role Routing
Route::DELETE('role/{id}/{eventID}', array('uses'=>'RoleController@destroyRole', 'before'=>'auth'));
//Route::POST('activity/{id}/{eventID}', array('uses'=>'ActivityController@test', 'as' => 'activity.destroy'));

Route::get('role/add/{activityID}', array('uses'=>'RoleController@index', 'before'=>'auth'));
Route::POST('role/create/', array('uses'=>'RoleController@createActivityRole', 'before'=>'auth'));

Route::get('role/edit/{roleID}', array('uses'=>'RoleController@editRole', 'before'=>'auth'));
Route::POST('role/update', array('uses'=>'RoleController@updateRole', 'before'=>'auth'));

#Volunteer Routing
Route::get('volunteer/findEvents', array('uses' => 'EventController@showVolunteerEvents', 'as' => 'findEvents', 'before'=>'auth'));
Route::get('volunteer/findActivities', array('uses' => 'ActivityController@showVolunteerActivities', 'as' => 'findActivities', 'before'=>'auth'));
Route::get('volunteer/findRoles/{searchID}', array('uses'=>'RoleController@showRoles', 'as' => '', 'before'=>'auth'));

Route::POST('volunteer/register', array('uses'=>'RegistrationController@register', 'before'=>'auth'));
Route::get('volunteer/registrations/{volunteerID?}', array('uses'=>'RegistrationController@showRegistrations', 'as' => 'showRegistrations', 'before'=>'auth'));
Route::POST('volunteer/allocate', array('uses'=>'RegistrationController@allocate', 'before'=>'auth'));
Route::POST('volunteer/reject', array('uses'=>'RegistrationController@reject', 'before'=>'auth'));
Route::get('volunteer/allocations/{activityID?}', array('uses'=>'RegistrationController@showAllocations', 'as' => 'showAllocations', 'before'=>'auth'));
Route::get('volunteer/roles/{volunteerID}', array('uses'=>'RegistrationController@showAllocations', 'before'=>'auth'));
Route::POST('volunteer/select/', array('uses'=>'RegistrationController@select', 'before'=>'auth'));
Route::POST('volunteer/select/withdraw', array('uses'=>'RegistrationController@select', 'before'=>'auth'));

Route::get('volunteer/registration/edit/{registrationID}', array('uses'=>'RegistrationController@editRegistration', 'before'=>'auth'));
Route::POST('volunteer/registration/update', array('uses'=>'RegistrationController@updateRegistration', 'before'=>'auth'));
Route::POST('volunteer/registration/delete', array('uses'=>'RegistrationController@deleteRegistration', 'before'=>'auth'));

Route::get('volunteer/profile/{volunteerID}', array('uses'=>'UsersController@getUser', 'before'=>'auth'));

#Find Volunteer Routing
Route::get('volunteer/find', array('uses' => 'EventController@showVolunteerEvents', 'as' => 'findVolunteers', 'before'=>'auth'));
Route::get('volunteer/find/{activityRoleID}', array('uses' => 'UsersController@findVolunteers', 'before'=>'auth'));
Route::post('volunteer/register/allocate', array('uses' => 'RegistrationController@registerAllocate', 'before'=>'auth'));

#Reporting
Route::get('eventmanager/{eventManagerID}/report/{eventID?}', array('uses' => 'UsersController@displayEventManagerReport', 'before'=>'auth'));
Route::get('volunteer/report/registrations/{volunteerID}', array('uses' => 'UsersController@getUser', 'before'=>'auth'));
Route::get('volunteer/report/participation/{volunteerID}', array('uses' => 'UsersController@getUser', 'before'=>'auth'));


Route::get('help', array('uses' => 'HomeController@showHelp', 'as' => 'help', 'before'=>'auth'));


