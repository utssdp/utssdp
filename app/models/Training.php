<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

/**
 * Training model
 *
 * @package models
 */
class Training extends Eloquent {
    /**
     * The primary key used by the Training Model
     *
     * @var string
     */
    protected $primaryKey = 'trainingID'; 
	/**
     * The database table used by the model.
     *
     * @var string
     */
	protected $table = 'Training';
    /**
    * Returns trainingID - The Primary Key
    */
    public function getTrainingID()
    {
        return $this->trainingID;
    }
    
    
}