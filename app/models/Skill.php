<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

/**
 * Skill model
 *
 * @package models
 */
class Skill extends Eloquent {
    /**
	 * The primary key used by Skill Model
	 *
	 * @var string
	 */
    protected $primaryKey = 'skillID'; 
	/**
     * The database table used by the model.
     *
     * @var string
     */
	protected $table = 'Skill';
    /**
    * Returns skillID - The Primary Key
    */
    public function getskillID()
    {
        return $this->skillID;
    }
    
    
}