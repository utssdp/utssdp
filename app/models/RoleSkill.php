<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

/**
 * Role Skill model
 *
 * @package models
 */
class RoleSkill extends Eloquent {
	/**
     * The database table used by the model.
     *
     * @var string
     */
	protected $table = 'Roleskill';
    
    /**
     * Returns skillID - Skill ID - Foreign Key
     */
    
    public function getSkillID()
    {
        return $this->skillID;
    }
    
    /**
     * Returns RoleID - Role ID - Foreign Key
     */
    
    public function getRoleID()
    {
        return $this->roleID;
    }
    
    
}