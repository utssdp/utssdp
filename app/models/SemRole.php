<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

/**
 * Role model
 *
 * @package models
 */
class SemRole extends Eloquent {
    /**
	 * The primary key used by Role Model
	 *
	 * @var string
	 */
    protected $primaryKey = 'roleID'; 
 
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'Role';
    /**
    * Returns roleID - The Primary Key
    */
    public function getRoleID()
    {
      return $this->roleID;
    }
    /**
    * Returns roleName - The Role Name
    */
    public function getRoleName()
    {
      return $this->roleName;
    }
    /**
    * Returns roleDescription - The Role Description
    */
    public function getRoleDescription()
    {
      return $this->roleDescription;
    }
    
}