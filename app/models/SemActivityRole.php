<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

/**
 * Activity Role model
 *
 * @package models
 */
class SemActivityRole extends Eloquent {
    /**
	 * The Primary Key for Activity Role Model
	 *
	 * @var string
	 */
    protected $primaryKey = 'activityRoleID'; 
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'ActivityRole';
    
    /**
    * Returns activityID - the Activity ID - Foreign Key
    */
    public function getActivityID()
    {
      return $this->activityID;
    }
    /**
    * Returns roleID - The Role ID - Foreign Key
    */
    public function getRoleID()
    {
      return $this->roleID;
    }
    /**
    * Returns reportingRoleID the Reporting Role ID - Foreign Key
    */
    public function getReportingRoleID()
    {
      return $this->reportingRoleID;
    }
    /**
    * Returns positionsAvailiable - The Positions Avaliable
    */
    public function getPositionsAvailiable()
    {
      return $this->positionsAvailiable;
    }
    /**
    * Returns activityRolePayRate - The Activity Role PayRate
    */
    public function getActivityRolePayRate()
    {
      return $this->activityRolePayRate;
    }    
    
}