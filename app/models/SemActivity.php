<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

/**
 * Activity model
 *
 * @package models
 */
class SemActivity extends Eloquent {
    /**
	 * Primary Key for Activity Model
	 *
	 * @var string
	 */
    protected $primaryKey = 'activityID';
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'Activity';
    
    /**
    * Returns activityID - Primary Key
    */
    
    public function getActivityID()
    {
      return $this->activityID;
    }
    
    /**
    * Returns eventID - The Event ID - Foreign Key
    */
    
    public function getEventID()
    {
      return $this->eventID;
    }
    /**
    * Returns activityName - The Activity Name
    */
    public function getActivityName()
    {
      return $this->activityName;
    }
    /**
    * Returns activityDescription - The Activity Description
    */
    public function getAcitivyDescription()
    {
      return $this->activityDescription;
    }
    /**
    * Returns ActivityDate - The Date of the Activity
    */
    public function getActivityDate()
    {
      return $this->activityDate;
    }
    /**
    * Returns ActivityStartDateTime - The Start Time of the Activity
    */
    public function getActivityStartDateTime()
    {
      return $this->activityStartDateTime;
    }
    /**
    * Returns ActivityEndDateTime - The End Time of the Activity
    */
    public function getActivityEndDateTime()
    {
      return $this->activityEndDateTime;
    }
    
}