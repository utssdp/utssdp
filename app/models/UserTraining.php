<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

/**
 * User Training model
 *
 * @package models
 */
class UserTraining extends Eloquent {
    /**
	 * The primary key used by UserTraining Model
	 *
	 * @var string
	 */
    protected $primaryKey = 'userTrainingID'; 
	/**
     * The database table used by the model.
     *
     * @var string
     */
	protected $table = 'UserTraining';
    
    public function getUserTrainingID()
    {
        return $this->userTrainingID;
    }
    
    public function getTrainingID()
    {
        return $this->trainingID;
    }
    
    public function getUserID()
    {
        return $this->userID;
    }
    
    
}