<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

/**
 * User model
 *
 * @package models
 */
class User extends Eloquent implements UserInterface, RemindableInterface {
 
	/**
	 * The primary key used by Users
	 *
	 * @var string
	 */
	protected $table = 'Users';
 
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');
 
	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}
 
	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}
    /**
    * Returns id - The Primary Key
    */
    public function getId()
    {
      return $this->id;
    }
    /**
    * Returns userFirstName - The Users First Name
    */
    public function firstName()
    {
      return $this->userFirstName;
    }
    /**
    * Returns userLastName - The Users Last Name
    */
    public function lastName()
    {
      return $this->userLastName;
    }
    /**
    * Returns userType - User Type such as System Admin, Event Manager, etc.
    */
     public function userType()
    {
      return $this->userType;
    }

    
 
	/**
	 * Get the token value for the "remember me" session.
	 *
	 * @return string
	 */
	public function getRememberToken()
	{
		return $this->remember_token;
	}
 
	/**
	 * Set the token value for the "remember me" session.
	 *
	 * @param  string  $value
	 * @return void
	 */
	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}
 
	/**
	 * Get the column name for the "remember me" token.
	 *
	 * @return string
	 */
	public function getRememberTokenName()
	{
		return 'remember_token';
	}
 
	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}
 
}
