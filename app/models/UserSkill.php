<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

/**
 * User Skill model
 *
 * @package models
 */
class UserSkill extends Eloquent {
    /**
	 * The primary key used by UserSkill Model
	 *
	 * @var string
	 */
    protected $primaryKey = 'userskillID'; 
	/**
     * The database table used by the model.
     *
     * @var string
     */
	protected $table = 'Userskill';
    /**
    * Returns userskillID - The primary Key
    */
    public function getuserskillID()
    {
        return $this->userskillID;
    }
    /**
    * Returns skillID - The Skill ID
    */
    public function getSkillID()
    {
        return $this->skillID;
    }
    /**
    * Returns userID - The User ID
    */
    public function getUserID()
    {
        return $this->userID;
    }
    
    
}