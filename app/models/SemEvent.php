<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

/**
 * Event model
 *
 * @package models
 */
class SemEvent extends Eloquent {
    /**
	 * The primary key for Events Model
	 *
	 * @var string
	 */
    protected $primaryKey = 'eventID'; 
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'Event';
    /**
    * Returns eventID - Event ID - Primary Key
    */
    public function getEventID()
    {
      return $this->eventID;
    }
    /**
    * Returns userID - User ID - Foreign Key
    */
    public function getUserID()
    {
      return $this->userID;
    }
    /**
    * Returns eventName - The Event Name
    */
    public function getEventName()
    {
      return $this->eventName;
    }
    /**
    * Returns eventStartDate - The Event Start Date
    */ 
    public function getEventStartDate()
    {
      return $this->eventStartDate;
    }
    /**
    * Returns eventEndDate - The Event End DAte
    */
    public function getEventEndDate()
    {
      return $this->eventEndDate;
    }
    /**
    * Returns eventLocation - The Event Location
    */
    public function getEventLocation()
    {
      return $this->eventLocation;
    }
    /**
    * Returns isArchived - If the Record has been deleted(archived)
    */
    public function getIsArchived()
    {
      return $this->isArchived;
    }
    /**
    * Returns created_by - Who is the event created by
    */
    public function getCreated_by()
    {
      return $this->created_by;
    }
    
}