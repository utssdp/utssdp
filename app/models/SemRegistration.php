<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

/**
 * Registration model
 *
 * @package models
 */
class SemRegistration extends Eloquent {
    /**
	 * The primary key used by Registrations Model
	 *
	 * @var string
	 */
    protected $primaryKey = 'registrationID'; 
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'Registrations';
    /**
    * Returns activityID - The Activity ID - Foreign  Key
    */
    public function getActivityID()
    {
      return $this->activityID;
    }
    /**
    * Returns roleID - The Role ID - Foreign  Key
    */
    public function getRoleID()
    {
      return $this->roleID;
    }
    /**
    * Returns volunteerID - The Volunteer ID - Foreign Key
    */
    public function getVolunteerID()
    {
        return $this->volunteerID;
    }
    /**
    * Returns reportingRoleID - The Reporting Role ID - Foreign Key
    */
    public function getReportingRoleID()
    {
      return $this->reportingRoleID;
    }
    /**
    * Returns positionsAvailiable - Number of Positions availiable for an activity
    */
    public function getPositionsAvailiable()
    {
      return $this->positionsAvailiable;
    }
    /**
    * Returns activityRolePayRate - Activity Role Pay Rate
    */
    public function getActivityRolePayRate()
    {
      return $this->activityRolePayRate;
    }    
    
}